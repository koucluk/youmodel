package com.forest_indo.youmodel.Utils.Services;

import com.forest_indo.youmodel.Model.Video;

import io.reactivex.Observable;

/**
 * Created by Koucluck on 12/14/2016.
 */

public interface UploaderInterface {

    int getQueueCount();

    void addToQueue(Observable<Video> videos);

    void deleteQueue();

    void checkQueue();
}
