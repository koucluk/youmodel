package com.forest_indo.youmodel.Utils.Services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by FIN_MULTIMEDIA on 05/10/2016.
 */

public class FirebaseInstanceIdRefresh extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {

        String token = FirebaseInstanceId.getInstance().getToken();

        Log.d("TOKEN FCM", token);
    }
}
