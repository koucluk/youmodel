package com.forest_indo.youmodel.Utils.Dependencies;

import android.app.Application;

import com.forest_indo.youmodel.Utils.Dependencies.components.AppComponent;
import com.forest_indo.youmodel.Utils.Dependencies.components.DaggerAppComponent;
import com.forest_indo.youmodel.Utils.Dependencies.modules.AppModule;

import io.realm.Realm;
import io.realm.RealmConfiguration;

//import com.squareup.leakcanary.LeakCanary;

/**
 * Created by Koucluck on 8/24/2016.
 */
public class AppController extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        /*if(LeakCanary.isInAnalyzerProcess(this)){
            return;
        }

        LeakCanary.install(this);*/

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .schemaVersion(3)
                .deleteRealmIfMigrationNeeded()
                .build();


        Realm.setDefaultConfiguration(config);

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
