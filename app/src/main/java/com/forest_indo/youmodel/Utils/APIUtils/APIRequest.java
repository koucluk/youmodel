package com.forest_indo.youmodel.Utils.APIUtils;

import com.forest_indo.youmodel.Model.BrowseAgencyResult;
import com.forest_indo.youmodel.Model.BrowseModelResult;
import com.forest_indo.youmodel.Model.BrowseRequest;
import com.forest_indo.youmodel.Model.Field;
import com.forest_indo.youmodel.Model.HistoryMessage;
import com.forest_indo.youmodel.Model.Message;
import com.forest_indo.youmodel.Model.MessageThread;
import com.forest_indo.youmodel.Model.Photo;
import com.forest_indo.youmodel.Model.Profile;
import com.forest_indo.youmodel.Model.ProfileAgency;
import com.forest_indo.youmodel.Model.ProfileModel;
import com.forest_indo.youmodel.Model.ProfileSeeker;
import com.forest_indo.youmodel.Model.Register;
import com.forest_indo.youmodel.Model.Request;
import com.forest_indo.youmodel.Model.User;
import com.forest_indo.youmodel.Model.UserField;
import com.forest_indo.youmodel.Model.Video;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * Created by Koucluck on 9/7/2016.
 */
public interface APIRequest {

    @Headers("Authorization: Basic admin")
    @POST("RegisterUser")
    Observable<List<String>> registerUser(@Body Register register);

    @Headers("Authorization: Basic admin")
    @POST("Login")
    Observable<List<String>> loginUser(@Body User.UserLogin param);

    @Headers("Authorization: Basic admin")
    @POST("RetrieveAvailableField")
    Observable<List<Field>> getFields();

    @Headers("Authorization: Basic admin")
    @POST("GetProfileUser")
    Observable<List<Profile>> getProfileUser(@Body Profile.ProfileRequest params);

    @Headers("Authorization: Basic admin")
    @POST("GetProfileUser")
    Observable<List<ProfileModel>> getProfileModel(@Body Profile.ProfileRequest params);

    @Headers("Authorization: Basic admin")
    @POST("GetProfileUser")
    Observable<List<ProfileAgency>> getProfileAgency(@Body Profile.ProfileRequest params);

    @Headers("Authorization: Basic admin")
    @POST("DeleteUserField")
    Observable<List<String>> deleteUserField(@Body User.UserIdRequest params);

    @Headers("Authorization: Basic admin")
    @POST("GetProfileUser")
    Observable<List<ProfileSeeker>> getProfileSeeker(@Body Profile.ProfileRequest params);

    @Headers("Authorization: Basic admin")
    @POST("GetDataUser")
    Observable<List<User>> getDataUser(@Body User.UserRequest params);

    @Headers("Authorization: Basic admin")
    @POST("InitiateUserModel")
    Observable<List<String>> initiateModel(@Body User.InitiateModel param);

    @Headers("Authorization: Basic admin")
    @POST("InitiateUserSeeker")
    Observable<List<String>> initiateSeeker(@Body User.InitiateSeeker param);

    @Headers("Authorization: Basic admin")
    @POST("InitiateUserAgency")
    Observable<List<String>> initiateAgency(@Body User.InitiateAgency param);

    @Headers("Authorization: Basic admin")
    @POST("SendUserField")
    Observable<List<String>> sendUserField(@Body Field.UserFieldRequest param);

    @Headers("Authorization: Basic admin")
    @POST("GetUserField ")
    Observable<List<UserField>> getUserField(@Body User.UserRequest param);

    @Headers("Authorization: Basic admin")
    @POST("SeeUserDetail")
    Observable<List<BrowseModelResult>> getModelData(@Body Request.UserRequest param);

    @Headers("Authorization: Basic admin")
    @POST("SeeUserDetail")
    Observable<List<BrowseAgencyResult>> getAgencyData(@Body Request.UserRequest param);

    @Headers("Authorization: Basic admin")
    @POST("GiveRate")
    Observable<List<String>> giveRates(@Body Request.GiveRate param);

    @Headers("Authorization: Basic admin")
    @POST("UpdateProfileInformation")
    Observable<List<String>> updateProfile(@Body Profile.ProfileData data);

    @Headers("Authorization: Basic admin")
    @POST("CheckEmailAvailability")
    Observable<List<String>> checkEmailAvailable();

    @Headers("Authorization: Basic admin")
    @POST("BrowseAgency")
    Observable<List<BrowseAgencyResult>> browseAgency(@Body BrowseRequest.BrowseAgency filter);

    @Headers("Authorization: Basic admin")
    @POST("BrowseModel")
    Observable<List<BrowseModelResult>> browseModel(@Body ProfileModel.RequestFilter filter);

    @Headers("Authorization: Basic admin")
    @POST("Hire")
    Observable<List<String>> hireModel(@Body Request.HireModel param);

    @Headers("Authorization: Basic admin")
    @POST("UpdateLimitLeft")
    Observable<List<String>> updateLimit(@Body Request.UpdateLimit param);

    @Headers("Authorization: Basic admin")
    @POST("SeeMessageByUser")
    Observable<List<HistoryMessage>> getMessage(@Body Request.RequestMessage param);

    @Headers("Authorization: Basic admin")
    @POST("PurchasePremium")
    Observable<List<String>> purchasePremium();

    @Headers("Authorization: Basic admin")
    @POST("DeliverMessage")
    Observable<List<String>> sendMessage(@Body Request.RequestSendMessage param);

    @Headers("Authorization: Basic admin")
    @POST("RetreiveMessageList")
    Observable<List<Message>> getMessageList(@Body Request.GetRates param);

    @Headers("Authorization: Basic admin")
    @POST("SeeMessageDetail")
    Observable<List<MessageThread>> getMessageList(@Body Request.RequestMessageList param);

    @Headers("Authorization: Basic admin")
    @POST("GetRate")
    Observable<List<String>> getRates(@Body Request.GetRates param);

    @Headers("Authorization: Basic admin")
    @POST("SeeUserPhoto")
    Observable<List<Photo>> getModelPhoto(@Body Request.GetRates param);

    @Headers("Authorization: Basic admin")
    @POST("SeeUserVideo")
    Observable<List<Video>> getModelVideo(@Body Request.GetRates param);

    @Multipart
    @Headers("Authorization: Basic admin")
    @POST("ChangeProfilePicture")
    Observable<List<String>> changePicture(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part file);

    @Multipart
    @Headers("Authorization: Basic admin")
    @POST("UploadPhoto")
    Observable<List<Photo>> uploadPhoto(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part file);

    @Multipart
    @Headers("Authorization: Basic admin")
    @POST("UploadVideo")
    Observable<List<Video>> uploadVideo(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part file,
                                        @Part MultipartBody.Part file2);
}
