package com.forest_indo.youmodel.Utils.View;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.forest_indo.youmodel.Utils.Constant;

/**
 * Created by Koucluk on 15/03/2016.
 */
public class CustomButton extends Button implements Constant {
    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        Typeface typeface = Typeface.createFromAsset(context.getAssets(), FontPath);
        this.setTypeface(typeface);
    }
}
