/*
package com.forest_indo.youmodel.Utils.View;

*/
/**
 * Created by FIN_MULTIMEDIA on 06/10/2016.
 *//*


import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.forest_indo.youmodel.R;
import com.github.rtoshiro.view.video.FullscreenVideoView;

*/
/**
 * Created by Koucluck on 7/16/2016.
 *//*

public class CustomVideoView extends FullscreenVideoView implements View.OnClickListener, SeekBar.OnSeekBarChangeListener, MediaPlayer.OnPreparedListener, View.OnTouchListener {

    protected static final Handler TIME_THREAD = new Handler();
    protected boolean IS_FULLSCREEN = false;
    protected View videoControl;
    protected SeekBar seekBar;
    protected ImageButton imgPlay;
    protected ImageButton imgFullscreen;
    protected TextView txtTotal;

    protected OnTouchListener touchListener;
    protected AudioManager am;
    protected AudioManager.OnAudioFocusChangeListener audioListener;

    protected Runnable updateTimeRunnable = new Runnable() {
        @Override
        public void run() {
            updateCounter();
            TIME_THREAD.postDelayed(updateTimeRunnable, 1000);
        }
    };

    public CustomVideoView(Context context) {
        super(context);
    }

    public CustomVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init() {
        super.init();
        if (this.isInEditMode()) {
            return;
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.videoControl = inflater.inflate(R.layout.childview_video_control, this, false);
        RelativeLayout.LayoutParams params =
                new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        params.addRule(ALIGN_PARENT_BOTTOM);
        addView(videoControl, params);

        this.seekBar = (SeekBar) this.videoControl.findViewById(R.id.seekbar_video);
        this.imgFullscreen = (ImageButton) this.videoControl.findViewById(R.id.btn_fullscreen);
        this.imgPlay = (ImageButton) this.videoControl.findViewById(R.id.btn_player_action);
        */
/*this.textTotal = (TextView) this.videoControlsView.findViewById(R.id.vcv_txt_total);
        this.textElapsed = (TextView) this.videoControlsView.findViewById(R.id.vcv_txt_elapsed);*//*


        super.setOnTouchListener(this);

        this.imgPlay.setOnClickListener(this);
        this.imgFullscreen.setOnClickListener(this);
        this.seekBar.setOnSeekBarChangeListener(this);

        am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        this.audioListener = new AudioManager.OnAudioFocusChangeListener() {
            @Override
            public void onAudioFocusChange(int focusChange) {
                if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
                    pause();
                } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                    start();
                } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                    am.abandonAudioFocus(audioListener);
                    stop();
                }
            }
        };

        this.videoControl.setVisibility(INVISIBLE);
    }

    public void updateFullscreen() {
        imgFullscreen.performClick();
    }

    protected void startCounter() {
        TIME_THREAD.postDelayed(updateTimeRunnable, 1000);
    }

    protected void stopCounter() {
        TIME_THREAD.removeCallbacks(updateTimeRunnable);
    }

    protected void updateCounter() {
        int elapsed = getCurrentPosition() / 1000;
        int max = getDuration() / 1000;

        Log.e("VIDEO POSITION", "" + getCurrentPosition());

//        super.getCurrentPosition();
        // getCurrentPosition is a little bit buggy :(
        seekBar.setMax(max);
        seekBar.setProgress(elapsed);

        if (elapsed > 0 && elapsed < getDuration()) {

            elapsed = Math.round(elapsed / 1000.f);
            long s = elapsed % 60;
            long m = (elapsed / 60) % 60;
            long h = (elapsed / (60 * 60)) % 24;

            */
/*if (h > 0)
                textElapsed.setText(String.format(Locale.US, "%d:%02d:%02d", h, m, s));
            else
                textElapsed.setText(String.format(Locale.US, "%02d:%02d", m, s));*//*

        }
    }

    @Override
    public void setOnTouchListener(OnTouchListener l) {
        touchListener = l;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_player_action) {
            if (isPlaying()) {
                ((ImageButton) v).setImageResource(R.drawable.player_icon_player_play);
                pause();
            } else {
                ((ImageButton) v).setImageResource(R.drawable.player_icon_player_pause);
                start();
            }
        } else {
            if (IS_FULLSCREEN == false) {
                DisplayMetrics metrics = new DisplayMetrics();
                ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
                ((ImageButton) v).setImageResource(R.drawable.ic_fullscreen_exit_white);
                action.getVideoLayout(this, true);
                ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);

                FrameLayout frame = (FrameLayout) getParent();
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,
                        LayoutParams.FILL_PARENT);
                frame.setLayoutParams(params);
                IS_FULLSCREEN = true;
//                setFullscreen(!isFullscreen());
            } else {
                DisplayMetrics metrics = new DisplayMetrics();
                ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
//                setActivity((Activity) context);
                ((ImageButton) v).setImageResource(R.drawable.ic_fullscreen_white);
                action.getVideoLayout(this, false);
                ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

                FrameLayout frame = (FrameLayout) getParent();
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
                        (int) (240f * metrics.density));
                frame.setLayoutParams(params);
//                setFullscreen(!isFullscreen());
                IS_FULLSCREEN = false;
            }
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
//        og.d(TAG, "onCompletion");

        super.onCompletion(mp);
        am.abandonAudioFocus(audioListener);
        stopCounter();
        updateControls();
        if (currentState != State.ERROR)
            updateCounter();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        boolean result = super.onError(mp, what, extra);
        stopCounter();
        updateControls();
        return result;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (getCurrentState() == State.END) {
            stopCounter();
        }
    }

    @Override
    protected void tryToPrepare() {
        super.tryToPrepare();

        if (getCurrentState() == State.PREPARED || getCurrentState() == State.STARTED) {
            int total = getDuration() / 1000;
            if (total > 0) {
                seekBar.setMax(total);
                seekBar.setProgress(0);

                total = total / 1000;
                long s = total % 60;
                long m = (total / 60) % 60;
                long h = (total / (60 * 60)) % 24;
                if (h > 0) {
                    */
/*textElapsed.setText("00:00:00");
                    textTotal.setText(String.format(Locale.US, "%d:%02d:%02d", h, m, s));*//*

                } else {
                    */
/*textElapsed.setText("00:00");
                    textTotal.setText(String.format(Locale.US, "%02d:%02d", m, s));*//*

                }
            }

            videoControl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void start() throws IllegalStateException {
//        super.start();
        if (!isPlaying()) {
//            super.start();

            int result = am.requestAudioFocus(null, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
            if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                super.start();
                startCounter();
                updateControls();
            }
        }
    }

    @Override
    public void pause() throws IllegalStateException {
//        super.pause();
        if (isPlaying()) {
            stopCounter();
            super.pause();
            am.abandonAudioFocus(audioListener);
            updateControls();
        }
    }

    @Override
    public void reset() {
        super.reset();

        seekBar.setProgress(0);
        */
/*textElapsed.setText("00:00");
        textTotal.setText("00:00");*//*

        am.abandonAudioFocus(audioListener);
        stopCounter();
        updateControls();
    }

    @Override
    public void stop() throws IllegalStateException {
        super.stop();
        am.abandonAudioFocus(audioListener);
        stopCounter();
        updateControls();
    }

    protected void updateControls() {
        Drawable icon;
        if (getCurrentState() == State.STARTED) {
            icon = context.getResources().getDrawable(R.drawable.player_icon_player_pause);
        } else {
            icon = context.getResources().getDrawable(R.drawable.player_icon_player_play);
        }
        imgPlay.setImageDrawable(icon);
    }

    public void hideControls() {
        if (videoControl != null) {
            videoControl.setVisibility(View.INVISIBLE);
        }
    }

    public void showControls() {
//        Log.d(TAG, "showControls");
        if (videoControl != null) {
            videoControl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        stopCounter();
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        int progress = seekBar.getProgress();
        seekTo(progress * 1000);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (videoControl != null) {
                if (videoControl.getVisibility() == View.VISIBLE)
                    hideControls();
                else
                    showControls();
            }
        }

        if (touchListener != null) {
            return touchListener.onTouch(CustomVideoView.this, event);
        }

        return false;
    }
}*/
