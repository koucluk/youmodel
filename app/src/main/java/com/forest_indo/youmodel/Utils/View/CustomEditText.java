package com.forest_indo.youmodel.Utils.View;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

import com.forest_indo.youmodel.Utils.Constant;

/**
 * Created by Koucluck on 7/25/2016.
 */
public class CustomEditText extends EditText implements Constant, TextWatcher {

    Context context;

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), FontPath);
        this.setTypeface(typeface);
    }

    public boolean setCustomFontEdittext(Context ctx, String asset) {
        Typeface tf;
        try {
            tf = Typeface.createFromAsset(ctx.getAssets(), asset);
        } catch (Exception e) {
            return false;
        }
        setTypeface(tf);
        return true;
    }

    public void setCustomFont(Context context) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), FontPath);
        this.setTypeface(typeface);
    }

    public void setNormalFont(Context context) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), FontPharPath);
        this.setTypeface(typeface);
    }

    @Override
    public void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);

        if (text.length() < 0) {
            setCustomFont(getContext());
        } else {
            setNormalFont(getContext());
        }

        if (TextUtils.isEmpty(text)) {
            setCustomFont(getContext());
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
