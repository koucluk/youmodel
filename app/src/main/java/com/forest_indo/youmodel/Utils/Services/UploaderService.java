package com.forest_indo.youmodel.Utils.Services;

import android.app.Service;
import android.content.Intent;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

import com.forest_indo.youmodel.Model.Video;
import com.forest_indo.youmodel.Utils.APIUtils.APIRequest;
import com.forest_indo.youmodel.Utils.Dependencies.AppController;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class UploaderService extends Service implements UploaderInterface {

    IBinder binder = new UploaderBinder();

    List<Observable<Video>> Videos;

    @Inject
    APIRequest request;

    public UploaderService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        ((AppController) getApplication()).getAppComponent().inject(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return binder;
    }

    @Override
    public int getQueueCount() {
        return Videos.size();
    }

    @Override
    public void addToQueue(Observable<Video> videos) {
        if (Videos == null) {
            Videos = new ArrayList<>();
        }
        Videos.add(videos);
    }

    @Override
    public void deleteQueue() {
        if (Videos != null && Videos.size() > 0) {
            Videos.remove(0);
        }
    }

    @Override
    public void checkQueue() {
        if (getQueueCount() > 0) {
//            proceedQueue();
        } else {
            stopSelf();
        }
    }

    /*private void proceedQueue() {
        Handler handler = new Handler();

        Observable.just(compressVideo())
                .concatWith(request.uploadVideo()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribeWith(new DisposableObserver<List<Video>>() {
                            @Override
                            public void onNext(List<Video> value) {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {

                            }
                        })
                ).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<File>() {
                    @Override
                    public void onNext(File value) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

        handler.post(() -> {

        });
    }*/

    /*private File compressVideo() {
        Handler handler = new Handler();
        final File[] file = new File[1];

        handler.post(() -> {
            file[0] = proceed();
        });

        return file[0];
    }*/

   /* private File proceed() {
        MediaCodec codec;
        MediaFormat format;
    }*/

    public class UploaderBinder extends Binder {
        UploaderService getService() {
            return UploaderService.this;
        }
    }
}
