package com.forest_indo.youmodel.Utils.Dependencies.components;

import com.forest_indo.youmodel.Utils.Dependencies.UserScope;
import com.forest_indo.youmodel.Utils.Dependencies.modules.UserModule;
import com.forest_indo.youmodel.View.LoginActivity;
import com.forest_indo.youmodel.View.MainActivity;

import dagger.Component;

/**
 * Created by Koucluck on 10/12/2016.
 */

/*@UserScope
@Component(modules = {UserModule.class})*/
public interface UserComponent {

    void inject(LoginActivity activity);

    void inject(MainActivity activity);
}
