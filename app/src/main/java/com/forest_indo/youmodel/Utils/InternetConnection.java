package com.forest_indo.youmodel.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by Koucluck on 8/9/2016.
 */
public class InternetConnection {
    private final int maxRetryLimit = 5;
    private Context context;
    private BroadcastReceiver broadcastReceiver;
    private PublishSubject<Boolean> internetStatusObservable;
    private int delayBetweenRetry = 100;
    private int currentRepeatCount = 1;

    public InternetConnection(Context context) {
        this.context = context;
    }

    public Observable<Boolean> isInternetOnObservable() {
        return Observable.just(true, false);
    }
}
