package com.forest_indo.youmodel.Utils.View;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.forest_indo.youmodel.Utils.Constant;

/**
 * Created by Koucluck on 9/5/2016.
 */
public class CustomTextViewPhar extends TextView implements Constant {
    public CustomTextViewPhar(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), FontPharPath);
        this.setTypeface(typeface);

        isInEditMode();
    }
}
