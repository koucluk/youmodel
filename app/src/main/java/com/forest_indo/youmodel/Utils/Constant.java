package com.forest_indo.youmodel.Utils;

/**
 * Created by User on 21/1/2016.
 */
public interface Constant {

    String PROFILEPICTURE_SERVER_URL = "http://103.253.113.205/YouModelAPI/uploads/profilepicture/";
    String PROFILEVIDEO_SERVER_URL = "http://103.253.113.205/YouModelAPI/uploads/profilevideo/";
    String GALERY_PHOTO_SERVER_URL = "http://103.253.113.205/YouModelAPI/uploads/galeries/photos/";
    String GALERY_VIDEO_SERVER_URL = "http://103.253.113.205/YouModelAPI/uploads/galeries/videos/";
    String GALERY_THUMBS_SERVER_URL = "http://103.253.113.205/YouModelAPI/uploads/galeries/videos/thumbs/";

    //REQUEST CODE
    int REQUEST_CAMERA_PHOTO = 0x123;
    int REQUEST_CAMERA_VIDEO = 0x123a;
    int REQUEST_GALERY_PHOTO = 0x124;
    int REQUEST_GALERY_VIDEO = 0x124a;

    String PROFILEPICTURE_LOCAL_PATH = "/profile/photos/";

    String FontPath = "fonts/futurist-bold.ttf";
    String FontPharPath = "fonts/Lato-Light.ttf";

    //preferences
    String KEY_PREFERENCES = "youmodel";
    String KEY_LOGGEDIN = "isLoggedIn";
    String KEY_USER_TYPE = "userType";
    String KEY_USER_ID = "userId";
    String KEY_SKIP_INITIATE = "skipInitiate";
    String KEY_CHECK_DONE = "check_done";

    //error message
    String ERROR_EMAIL_NOT_REG = "YOUR EMAIL HAVE NOT BEEN REGISTERED";
    String ERROR_WRONG_PASSWORD = "INCORRECT PASSWORD";

    String[] MONTH = new String[]{
            "JANUARI",
            "FEBRUARI",
            "MARET",
            "APRIL",
            "MEI",
            "JUNI",
            "JULI",
            "AGUSTUS",
            "SEPTEMBER",
            "OKTOBER",
            "NOPEMBER",
            "DESEMBER",
    };

    String[] DAY = new String[]{
            "Sen",
            "Sel",
            "Rab",
            "Kam",
            "Jum",
            "Sab",
            "Min"
    };

    String[] MONTH_SHORT = new String[]{
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "Mei",
            "Jun",
            "Jul",
            "Agu",
            "Sep",
            "Okt",
            "Nop",
            "Des",
    };

}
