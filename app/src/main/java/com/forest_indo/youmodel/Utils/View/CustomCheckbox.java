package com.forest_indo.youmodel.Utils.View;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.CheckBox;

import com.forest_indo.youmodel.Utils.Constant;

/**
 * Created by Koucluk on 16/03/2016.
 */
public class CustomCheckbox extends CheckBox implements Constant {
    public CustomCheckbox(Context context, AttributeSet attrs) {
        super(context, attrs);

        Typeface typeface = Typeface.createFromAsset(context.getAssets(), FontPath);
        this.setTypeface(typeface);
    }
}
