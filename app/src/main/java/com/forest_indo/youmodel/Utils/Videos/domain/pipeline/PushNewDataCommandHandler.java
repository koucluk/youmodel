/*
 * Copyright 2014-2016 Media for Mobile
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.forest_indo.youmodel.Utils.Videos.domain.pipeline;

import com.forest_indo.youmodel.Utils.Videos.domain.Frame;
import com.forest_indo.youmodel.Utils.Videos.domain.ICommandHandler;
import com.forest_indo.youmodel.Utils.Videos.domain.IOutput;
import com.forest_indo.youmodel.Utils.Videos.domain.Render;

import java.nio.ByteBuffer;


class PushNewDataCommandHandler implements ICommandHandler {
    private IOutput output;
    private Render render;

    public PushNewDataCommandHandler(IOutput output, Render render) {
        super();
        this.output = output;
        this.render = render;
    }

    @Override
    public void handle() {
        Frame frame = new Frame(ByteBuffer.allocate(1024 * 1024), 1024 * 1024, 0, 0, 0, 0);
        output.pull(frame);
        render.push(frame);
    }
}
