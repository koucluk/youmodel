package com.forest_indo.youmodel.Utils.Dependencies.components;

import com.forest_indo.youmodel.Presenter.LoginPresenter;
import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.Presenter.RegisterPresenter;
import com.forest_indo.youmodel.Utils.Dependencies.modules.AppModule;
import com.forest_indo.youmodel.Utils.Services.UploaderService;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by FIN_MULTIMEDIA on 27/09/2016.
 */

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    void inject(LoginPresenter presenter);

    void inject(MainPresenter presenter);

    void inject(RegisterPresenter presenter);

    void inject(UploaderService service);
}
