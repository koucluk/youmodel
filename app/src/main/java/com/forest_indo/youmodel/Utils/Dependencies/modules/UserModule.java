package com.forest_indo.youmodel.Utils.Dependencies.modules;

import com.forest_indo.youmodel.Model.User;
import com.forest_indo.youmodel.Utils.Constant;
import com.forest_indo.youmodel.Utils.Dependencies.UserScope;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Koucluck on 10/12/2016.
 */

//@Module
public class UserModule implements Constant {

    User user;

   /* @Provides
    @UserScope*/
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
