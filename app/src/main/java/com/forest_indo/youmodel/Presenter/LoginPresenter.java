package com.forest_indo.youmodel.Presenter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.widget.EditText;

import com.forest_indo.youmodel.Model.User;
import com.forest_indo.youmodel.Utils.APIUtils.APIRequest;
import com.forest_indo.youmodel.Utils.Constant;
import com.forest_indo.youmodel.Utils.Dependencies.AppController;
import com.forest_indo.youmodel.View.LoginActivity;
import com.forest_indo.youmodel.View.MainActivity;
import com.forest_indo.youmodel.View.RegisterActivity;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Koucluck on 9/1/2016.
 */
public class LoginPresenter implements Constant {

    LoginInterface login;
    LoginActivity context;

    CompositeDisposable disposable = new CompositeDisposable();

    @Inject
    APIRequest request;

    @Inject
    SharedPreferences sharedPreferences;

    public LoginPresenter(LoginActivity context, LoginInterface login) {
        this.context = context;
        this.login = login;

        ((AppController) this.context.getApplication()).getAppComponent().inject(this);
    }

    public void doLogin(String email, String passwd) {
        User.UserLogin loginReq = new User.UserLogin(email, passwd);
        context.showProgress(true);

        if (email.isEmpty() || passwd.isEmpty()) {
            context.showProgress(false);
            login.showError("Harap lengkapi semua isian!");
        } else {
            disposable.add(
                    request.loginUser(loginReq)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(new DisposableObserver<List<String>>() {
                                @Override
                                public void onNext(List<String> value) {

                                    if (value.get(0).equalsIgnoreCase("Success")) {

                                        sharedPreferences.edit().putBoolean(KEY_LOGGEDIN, true).apply();

                                        Intent login = new Intent(context, MainActivity.class);
                                        login.putExtra("USER_ID", value.get(1));

                                        sharedPreferences.edit().putInt(KEY_USER_ID, Integer.parseInt(value.get(1))).apply();
                                        
                                        context.startActivity(login);
                                        context.finish();
                                    } else if (value.get(1).equalsIgnoreCase(ERROR_EMAIL_NOT_REG)) {
                                        login.showError("Email tidak terdaftar");
                                    } else if (value.get(1).equalsIgnoreCase(ERROR_WRONG_PASSWORD)) {
                                        login.showError("Password tidak sesuai");
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
//                                Log.d("LOGIN VALUE", e.getMessage());
                                    e.printStackTrace();
                                }

                                @Override
                                public void onComplete() {
                                    Log.d("LOGIN VALUE", "COMPLETE");
                                    context.showProgress(false);
                                }
                            })
            );
        }
    }

    public void showPass(EditText etPasswd, boolean status) {
        if (status == false) {
            etPasswd.setTransformationMethod(HideReturnsTransformationMethod
                    .getInstance());

            login.setStatusPassword(true);
        } else {
            etPasswd.setTransformationMethod(PasswordTransformationMethod
                    .getInstance());

            login.setStatusPassword(false);
        }
    }

    public void doRegister() {
        Intent register = new Intent(context, RegisterActivity.class);
        context.startActivity(register);
    }

    public boolean isLoggedIn() {
        if (sharedPreferences.getBoolean(KEY_LOGGEDIN, false) == true) {
            Intent login = new Intent(context, MainActivity.class);
            context.startActivity(login);
            context.finish();
        }
        return sharedPreferences.getBoolean(KEY_LOGGEDIN, false);
    }

    public void onDestroy() {
        disposable.clear();
        context = null;
    }

    public interface LoginInterface {

        void setStatusPassword(boolean status);

        void showError(String msg);
    }
}
