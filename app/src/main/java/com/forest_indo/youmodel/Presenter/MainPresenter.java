package com.forest_indo.youmodel.Presenter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.forest_indo.youmodel.Adapter.ChatAdapter;
import com.forest_indo.youmodel.Adapter.PreviewVideoAdapter;
import com.forest_indo.youmodel.Adapter.SmallPhotoAgencyAdapter;
import com.forest_indo.youmodel.Adapter.UserEntryAdapter;
import com.forest_indo.youmodel.Adapter.UserKategoriAdapter;
import com.forest_indo.youmodel.Model.BrowseAgencyResult;
import com.forest_indo.youmodel.Model.BrowseModelResult;
import com.forest_indo.youmodel.Model.BrowseRequest;
import com.forest_indo.youmodel.Model.Field;
import com.forest_indo.youmodel.Model.HistoryMessage;
import com.forest_indo.youmodel.Model.Message;
import com.forest_indo.youmodel.Model.MessageThread;
import com.forest_indo.youmodel.Model.Photo;
import com.forest_indo.youmodel.Model.Profile;
import com.forest_indo.youmodel.Model.ProfileAgency;
import com.forest_indo.youmodel.Model.ProfileModel;
import com.forest_indo.youmodel.Model.ProfileSeeker;
import com.forest_indo.youmodel.Model.Request;
import com.forest_indo.youmodel.Model.User;
import com.forest_indo.youmodel.Model.UserField;
import com.forest_indo.youmodel.Model.Video;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.APIUtils.APIRequest;
import com.forest_indo.youmodel.Utils.Constant;
import com.forest_indo.youmodel.Utils.Dependencies.AppController;
import com.forest_indo.youmodel.Utils.View.CircleTransformation;
import com.forest_indo.youmodel.Utils.View.CustomTextView;
import com.forest_indo.youmodel.View.Fragment.AgencyDetailFragment;
import com.forest_indo.youmodel.View.Fragment.ChatFragment;
import com.forest_indo.youmodel.View.Fragment.InitiateFragment;
import com.forest_indo.youmodel.View.Fragment.MainEntryFragment;
import com.forest_indo.youmodel.View.Fragment.MainFragment;
import com.forest_indo.youmodel.View.Fragment.ModelDetailFragment;
import com.forest_indo.youmodel.View.Fragment.PacketFragment;
import com.forest_indo.youmodel.View.LoginActivity;
import com.forest_indo.youmodel.View.MainActivity;
import com.github.ornolfr.ratingview.RatingView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Koucluck on 8/9/2016.
 */
public class MainPresenter implements Constant {

    MainActivity view;
    MainInterface mainInterface;

    CompositeDisposable disposable;
    DisposableObserver<List<MessageThread>> chatObserver;

    User user;

    @Inject
    APIRequest request;

    @Inject
    SharedPreferences preferences;

    Realm realm;

    AlertDialog dialog = null;

    private int ID_USER = -1;
    private int MESSAGE_ID = -1;
    private List<BrowseModelResult> ModelResult;
    private List<BrowseAgencyResult> AgencyResult;

    public MainPresenter(MainActivity activity, Realm defaultInstance) {
        view = activity;
        mainInterface = activity;

        realm = defaultInstance;

        ((AppController) view.getApplication()).getAppComponent().inject(this);
        disposable = new CompositeDisposable();
    }

    public void setupRecModel(RecyclerView recModel) {
        recModel.setLayoutManager(new GridLayoutManager(view, 2));
        recModel.setAdapter(new UserEntryAdapter(2));
    }

    private boolean checkIsNew(final String user_id) {
        view.showProgress(true);

        User.UserRequest userRequest = new User.UserRequest(Integer.parseInt(user_id));

        final Profile.ProfileRequest[] profileRequest = new Profile.ProfileRequest[1];
        final User[] us = new User[1];
        disposable.add(
                request.getDataUser(userRequest).doOnNext(u -> {
                    profileRequest[0] = new Profile.ProfileRequest(Integer.parseInt(user_id),
                            u.get(0).getUser_Type());

                    preferences.edit().putInt(KEY_USER_TYPE, u.get(0).getUser_Type()).apply();
                    us[0] = u.get(0);

                    realm = Realm.getDefaultInstance();

                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(u.get(0));
                    realm.commitTransaction();

                }).flatMap(u -> request.getProfileUser(profileRequest[0])).doOnError(e -> e.printStackTrace())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<List<Profile>>() {
                            List<Profile> profiles = new ArrayList<>();

                            @Override
                            public void onNext(List<Profile> value) {
                                profiles = value;
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {
                                realm = Realm.getDefaultInstance();
                                user = realm.where(User.class).findFirst();

                                Log.d("GET DATA PROFILE RES", "Size : " + profiles.size());

                                preferences.edit().putBoolean(KEY_CHECK_DONE, true).apply();

                                if (profiles.size() > 0) {
                                    Log.d("JUMLAH PROFILE", "" + profiles.size());
                                    getProfile(Integer.parseInt(getUser().getUSER_ID()),
                                            getUser().getUser_Type(), "main");
                                } else {
                                    Log.d("USER EMAIL", user.getEmail());

                                    ID_USER = Integer.parseInt(getUser().getUSER_ID());

                                    Fragment fragment = InitiateFragment.newInstance(user.getUser_Type(),
                                            user.getEmail(), user.getPassword());
                                    view.ChangeFragment(fragment, new String[]{"initiate"}, false);
                                    view.showProgress(false);
                                }
                            }
                        })
        );


        return false;
    }

    private void retreiveUserData(User.UserRequest userRequest) {

        Log.d("RETREIVE USER", "RUN");

        disposable.add(
                request.getDataUser(userRequest)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<List<User>>() {

                            @Override
                            public void onNext(List<User> value) {
                                realm = Realm.getDefaultInstance();

                                realm.executeTransaction(realm1 -> realm1.copyToRealmOrUpdate(value.get(0)));
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {
                                view.ChangeFragment(MainFragment.newInstance(null, null), new String[]{"main"}, false);
                                view.showProgress(false);
                                view.setupViewWithUser(getUser());

                                if (getUser().getUser_Type() == 3) {
                                    mainInterface.setupDrawer(true);
                                } else {
                                    mainInterface.setupDrawer(false);
                                }
                            }
                        })
        );
    }

    public User getUser() {
        realm = Realm.getDefaultInstance();
        return realm.where(User.class).findFirst();
    }

    public ProfileModel getModel() {
        realm = Realm.getDefaultInstance();
        return realm.where(ProfileModel.class).findFirst() == null
                ? null
                : realm.where(ProfileModel.class).findFirst();
    }

    public ProfileAgency getAgency() {
        realm = Realm.getDefaultInstance();
        return realm.where(ProfileAgency.class).findFirst() == null
                ? null
                : realm.where(ProfileAgency.class).findFirst();
    }

    public ProfileSeeker getSeeker() {
        realm = Realm.getDefaultInstance();
        return realm.where(ProfileSeeker.class).findFirst() == null
                ? null
                : realm.where(ProfileSeeker.class).findFirst();
    }

    public void getProfile(int user_id, int type, String gotos) {
        switch (type) {
            case 1:
                disposable.add(
                        request.getProfileModel(new Profile.ProfileRequest(user_id
                                , type))
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableObserver<List<ProfileModel>>() {
                                    @Override
                                    public void onNext(List<ProfileModel> value) {
                                        realm = Realm.getDefaultInstance();

                                        realm.beginTransaction();
                                        realm.copyToRealmOrUpdate(value);
                                        realm.commitTransaction();

                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        e.printStackTrace();
                                    }

                                    @Override
                                    public void onComplete() {

                                        Log.d("NAME MODEL", "" + getModel().getName());

                                        view.setupViewWithProfile(getModel(), 1);

                                        if (gotos.equals("main")) {
                                            view.ChangeFragment(MainFragment.newInstance(null, null), new String[]{"main"}, false);
                                            if (type == 3) {
                                                mainInterface.setupDrawer(true);
                                            } else {
                                                mainInterface.setupDrawer(false);
                                            }
                                        } else {
                                            view.ChangeFragment(ModelDetailFragment.newInstance(true, 0), new String[]{"myprofile"}, false);
                                        }

                                        view.showProgress(false);
                                    }
                                })
                );
                break;
            case 2:
                disposable.add(
                        request.getProfileAgency(new Profile.ProfileRequest(user_id
                                , type))
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableObserver<List<ProfileAgency>>() {
                                    @Override
                                    public void onNext(List<ProfileAgency> value) {
                                        realm = Realm.getDefaultInstance();

                                        realm.beginTransaction();
                                        realm.copyToRealmOrUpdate(value.get(0));
                                        realm.commitTransaction();
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        e.printStackTrace();
                                    }

                                    @Override
                                    public void onComplete() {
                                        Log.d("NAME AGENCY", "" + getAgency().getName());

                                        view.setupViewWithProfile(getAgency(), 2);
                                        if (gotos.equals("main")) {
                                            view.ChangeFragment(MainFragment.newInstance(null, null), new String[]{"main"}, false);
                                            if (type == 3) {
                                                mainInterface.setupDrawer(true);
                                            } else {
                                                mainInterface.setupDrawer(false);
                                            }
                                        } else {
                                            view.ChangeFragment(AgencyDetailFragment.newInstance(true, 0), new String[]{"myprofile"}, false);
                                        }

                                        view.showProgress(false);
                                    }
                                })
                );
                break;
            case 3:
                disposable.add(
                        request.getProfileSeeker(new Profile.ProfileRequest(user_id
                                , type))
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableObserver<List<ProfileSeeker>>() {
                                    @Override
                                    public void onNext(List<ProfileSeeker> value) {
                                        realm = Realm.getDefaultInstance();

                                        realm.beginTransaction();
                                        realm.copyToRealmOrUpdate(value.get(0));
                                        realm.commitTransaction();
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        e.printStackTrace();
                                    }

                                    @Override
                                    public void onComplete() {
                                        view.showProgress(false);
                                    }
                                })
                );
                break;
        }
    }

    private void deleteUserField(RecyclerView recKategori) {
        disposable.add(
                request.deleteUserField(new User.UserIdRequest(Integer.parseInt(getUser().getUSER_ID())))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<List<String>>() {
                            @Override
                            public void onNext(List<String> value) {
                                Log.d("DELETE VALUE", value.get(0));

                                if (value.get(0).equalsIgnoreCase("Success")) {

                                    realm = Realm.getDefaultInstance();
                                    realm.beginTransaction();
                                    RealmResults<UserField> rows = realm.where(UserField.class)
                                            .equalTo("User_Id", Integer.parseInt(getUser().getUSER_ID()))
                                            .findAll();

                                    rows.deleteAllFromRealm();
                                    realm.commitTransaction();
                                }

                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {
                                sendUserField(recKategori, "profile", Integer.parseInt(getUser().getUSER_ID()), getUser().getUser_Type());
                            }
                        })
        );
    }

    public void uploadPhoto(File file, String userId, RecyclerView recView, CustomTextView txtEmptyPhoto) {
        view.showProgress(true);
        Map<String, RequestBody> map = new HashMap<>();
        RequestBody userid = RequestBody.create(MediaType.parse("multipart/form-data"), getUser().getUSER_ID());

        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        map.put("userid", userid);

        disposable.add(
                request.uploadPhoto(map, MultipartBody.Part.createFormData("image", file.getName(), requestFile))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<List<Photo>>() {

                            List<Photo> photos = new ArrayList<Photo>();

                            @Override
                            public void onNext(List<Photo> value) {
                                photos = value;
                                realm = Realm.getDefaultInstance();
                                realm.beginTransaction();
                                realm.copyToRealmOrUpdate(value.get(0));
                                realm.commitTransaction();
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {
                                if (recView.getAdapter().getItemCount() == 0) {
                                    if (photos.size() > 0) {
                                        txtEmptyPhoto.setVisibility(View.GONE);
                                        recView.setVisibility(View.VISIBLE);
                                        ((SmallPhotoAgencyAdapter) recView.getAdapter()).addNewItem(photos);
                                    }
                                } else {
                                    if (photos.size() > 0) {
                                        ((SmallPhotoAgencyAdapter) recView.getAdapter()).updateItem(photos.get(0));
                                    }
                                }

                                view.showProgress(false);
                            }
                        })
        );

    }

    public void uploadVideo(File[] file, String user_id, RecyclerView recVideo) {

        Log.d("VIDEO UPLOAD CALLED", "YES");

        Map<String, RequestBody> map = new HashMap<>();
        RequestBody userid = RequestBody.create(MediaType.parse("multipart/form-data"), getUser().getUSER_ID());

        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file[0]);
        RequestBody requestFile1 =
                RequestBody.create(MediaType.parse("multipart/form-data"), file[1]);

        map.put("userid", userid);

        disposable.add(
                request.uploadVideo(map, MultipartBody.Part.createFormData("video", file[0].getName(), requestFile),
                        MultipartBody.Part.createFormData("thumb", file[1].getName(), requestFile1))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<List<Video>>() {

                            Video video;

                            @Override
                            public void onNext(List<Video> value) {
                                Log.d("VIDEO UPLOAD RESULT", value.size() + "");
                                video = value.get(0);

                                video.setSuccess(true);
                                video.setUploading(false);

                                realm = Realm.getDefaultInstance();
                                realm.beginTransaction();
                                realm.copyToRealmOrUpdate(value);
                                realm.commitTransaction();

                                ((PreviewVideoAdapter) recVideo.getAdapter()).updateStatusUpload(0, video);
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {
                                Toast.makeText(view, "UPLOAD VIDEO COMPLETE", Toast.LENGTH_SHORT).show();
                            }
                        })
        );
    }

    public void getModelPhotos(int UserId, RecyclerView recPhoto, CustomTextView txtEmpty) {
        txtEmpty.setVisibility(View.VISIBLE);
        recPhoto.setVisibility(View.GONE);

        List<Photo> photos = new ArrayList<>();

        realm = Realm.getDefaultInstance();
        photos = realm.copyFromRealm(realm.where(Photo.class).equalTo("User_Id", UserId).findAllSorted("Photo_Id", Sort.DESCENDING));
        if (photos.size() > 0) {
            Log.d("DB PHOTO EXISTS", "YES - SIZE " + photos.size());
            txtEmpty.setVisibility(View.GONE);
            recPhoto.setVisibility(View.VISIBLE);
            recPhoto.setAdapter(new SmallPhotoAgencyAdapter(1, photos));
        } else {
            disposable.add(
                    request.getModelPhoto(new Request.GetRates(UserId))
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribeWith(new DisposableObserver<List<Photo>>() {

                                List<Photo> photos = new ArrayList<>();

                                @Override
                                public void onNext(List<Photo> value) {
                                    realm = Realm.getDefaultInstance();
                                    realm.beginTransaction();
                                    photos = value;
                                    realm.copyToRealmOrUpdate(photos);
                                    realm.commitTransaction();
                                }

                                @Override
                                public void onError(Throwable e) {
                                    e.printStackTrace();
                                }

                                @Override
                                public void onComplete() {
                                    if (photos.size() > 0) {
                                        Log.d("SERVER RETURN LIST", "YES");
                                        txtEmpty.setVisibility(View.GONE);
                                        recPhoto.setVisibility(View.VISIBLE);
                                        recPhoto.setAdapter(new SmallPhotoAgencyAdapter(1, (ArrayList<Photo>) photos));
                                    } else {
                                        Log.d("SERVER RETURN LIST", "NO");
                                        recPhoto.setVisibility(View.GONE);
                                        txtEmpty.setVisibility(View.VISIBLE);
                                        recPhoto.setAdapter(new SmallPhotoAgencyAdapter(1, photos));
                                    }
                                }
                            })
            );
        }
    }

    public void getModelVideos(int UserId, RecyclerView recVideo, CustomTextView txtEmpty) {
        txtEmpty.setVisibility(View.VISIBLE);
        recVideo.setVisibility(View.GONE);

        List<Video> videos = new ArrayList<>();

        realm = Realm.getDefaultInstance();
        videos = realm.copyFromRealm(realm.where(Video.class).equalTo("User_Id", UserId).findAllSorted("Video_Id", Sort.DESCENDING));
        if (videos.size() > 0) {
            Log.d("DB VIDEO EXISTS", "YES - SIZE " + videos.size());
            txtEmpty.setVisibility(View.GONE);
            recVideo.setVisibility(View.VISIBLE);
            recVideo.setAdapter(new PreviewVideoAdapter(videos, view));
        } else {
            disposable.add(
                    request.getModelVideo(new Request.GetRates(UserId))
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribeWith(new DisposableObserver<List<Video>>() {

                                List<Video> videos = new ArrayList<>();

                                @Override
                                public void onNext(List<Video> value) {
                                    realm = Realm.getDefaultInstance();
                                    realm.beginTransaction();
                                    videos = value;
                                    realm.copyToRealmOrUpdate(videos);
                                    realm.commitTransaction();
                                }

                                @Override
                                public void onError(Throwable e) {
                                    e.printStackTrace();
                                }

                                @Override
                                public void onComplete() {
                                    if (videos.size() > 0) {
                                        Log.d("SERVER RETURN LIST", "YES");
                                        txtEmpty.setVisibility(View.GONE);
                                        recVideo.setVisibility(View.VISIBLE);
                                        recVideo.setAdapter(new PreviewVideoAdapter(videos, view));
                                    } else {
                                        Log.d("SERVER RETURN LIST", "NO");
                                        recVideo.setVisibility(View.GONE);
                                        txtEmpty.setVisibility(View.VISIBLE);
                                        recVideo.setAdapter(new PreviewVideoAdapter(videos, view));
                                    }
                                }
                            })
            );
        }
    }

    /*public void compressVideo(String path, String workFolder) {
        view.showProgress(true);
        Observable str = Observable.just(runCompress(path, workFolder));

        str.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver() {
                    @Override
                    public void onNext(Object value) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        view.showProgress(false);
                    }
                });

    }*/

    /*private Object runCompress(String path, String workFolder) {
        GeneralUtils.copyLicenseFromAssetsToSDIfNeeded(view, workFolder);

        LoadJNI jni = new LoadJNI();
        path = path.replace(" ", "&#032");
        String commandStr2 = "ffmpeg -y -i " + path + " -strict experimental -s 160x120 -r 30 -aspect 3:4 -ab 48000 -ac 2 -ar 22050 -vcodec mpeg4 -b 2097152 /storage/emulated/0/Video/out2.mp4";
        try {
            jni.run(GeneralUtils.utilConvertToComplex(commandStr2), workFolder, view);
        } catch (CommandValidationException e) {
            e.printStackTrace();
        }

        return 1;
    }*/

    public void ChangePicture(File destination, ImageView imgProfil) {
        view.showProgress(true);
        Map<String, RequestBody> map = new HashMap<>();
        RequestBody userid = RequestBody.create(MediaType.parse("multipart/form-data"), getUser().getUSER_ID());
        RequestBody usertype = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(getUser().getUser_Type()));

        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), destination);

        map.put("userid", userid);
        map.put("user_type", usertype);

        disposable.add(
                request.changePicture(map, MultipartBody.Part.createFormData("image", destination.getName(), requestFile))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<List<String>>() {
                            boolean status = false;

                            @Override
                            public void onNext(List<String> value) {
                                if (value.get(1).equalsIgnoreCase("SUCCESSFULY UPDATE")) {
                                    status = true;
                                } else {
                                    status = false;
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {
                                view.showProgress(false);
                                if (status) {
                                    Toast.makeText(view, "Sukses", Toast.LENGTH_SHORT).show();

                                    realm = Realm.getDefaultInstance();

                                    realm.executeTransaction(realm1 -> {
                                        ProfileModel model = realm1.where(ProfileModel.class)
                                                .equalTo("User_Id", Integer.parseInt(getUser().getUSER_ID()))
                                                .findFirst();

                                        model.setPhoto_Url(destination.getName());
                                    });

                                    Picasso.with(view.getApplicationContext())
                                            .load(destination)
                                            .transform(new CircleTransformation())
                                            .into(imgProfil);

//                                    view.updateDrawer(destination.getName());

                                } else {
                                    Toast.makeText(view, "Gagal", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
        );
    }

    public void doRecruit(int userId) {
        if (!getUser().isPremium()) {
            if (getUser().getLimit_Left() > 0) {
                view.showTwoButtonDialog(view.getResources().getString(R.string.confirm_recruit_notice), "BATAL", "LANJUT");

                view.getDialog().findViewById(R.id.btn_action1_dialog).setOnClickListener(v -> {
                    view.getDialog().dismiss();
                });

                view.getDialog().findViewById(R.id.btn_action2_dialog).setOnClickListener(v -> {
                    view.getDialog().dismiss();
                    recruitModel(userId, true);
                });
            } else {
                view.showOneButtonDialog(view.getResources().getString(R.string.fail_recruit_notice), "VIP");

                view.getDialog().findViewById(R.id.btn_action1_dialog).setOnClickListener(v -> {
                    Fragment fragment = PacketFragment.newInstance("", "");
                    mainInterface.ChangeFragment(fragment, new String[]{"packet_frame"}, true);
                    view.getDialog().dismiss();
                });
            }
        } else {
            view.showTwoButtonDialog(view.getResources().getString(R.string.confirm_recruit), "BATAL", "LANJUT");
            view.getDialog().findViewById(R.id.btn_action1_dialog).setOnClickListener(v -> {
                view.getDialog().dismiss();
            });

            view.getDialog().findViewById(R.id.btn_action2_dialog).setOnClickListener(v -> {
                view.getDialog().dismiss();
                recruitModel(userId, false);
            });
        }
    }

    private void recruitModel(int userId, boolean limited) {
        view.showProgress(true);
        disposable.add(
                request.hireModel(new Request.HireModel(Integer.parseInt(getUser().getUSER_ID()), userId))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<List<String>>() {
                            boolean isSuccess = false;

                            @Override
                            public void onNext(List<String> value) {
                                if (value.get(0).equalsIgnoreCase("Success")) {
                                    isSuccess = true;
                                } else {
                                    isSuccess = false;
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {
                                if (isSuccess && limited) {
                                    updateLimitLeft();
                                } else if (isSuccess && !limited) {
                                    view.showOneButtonDialog("Permintaan telah dikirim", "TUTUP");
                                    view.getDialog().findViewById(R.id.btn_action1_dialog).setOnClickListener(v -> {
                                        view.getDialog().dismiss();
                                    });
                                } else {
                                    view.showOneButtonDialog("Terjadi kesalahan, silahkan ulangi kembali", "TUTUP");
                                    view.getDialog().findViewById(R.id.btn_action1_dialog).setOnClickListener(v -> {
                                        view.getDialog().dismiss();
                                    });
                                }
                            }
                        })
        );
    }

    private void updateLimitLeft() {
        disposable.add(
                request.updateLimit(new Request.UpdateLimit(Integer.parseInt(getUser().getUSER_ID()),
                        (getUser().getLimit_Left() - 1)))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<List<String>>() {
                            boolean isSuccess = false;

                            @Override
                            public void onNext(List<String> value) {
                                if (value.get(0).equalsIgnoreCase("Success")) {
                                    isSuccess = true;
                                } else {
                                    isSuccess = false;
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {
                                view.showProgress(false);
                                if (isSuccess) {
                                    realm = Realm.getDefaultInstance();
                                    User user = new User();
                                    user.setUSER_ID(getUser().getUSER_ID());
                                    user.setLimit_Left(getUser().getLimit_Left() - 1);
                                    realm.beginTransaction();
                                    realm.copyToRealmOrUpdate(user);
                                    realm.commitTransaction();
                                } else {

                                }
                            }
                        })
        );
    }

    public List<UserField> getUserField() {
        realm = Realm.getDefaultInstance();
        List<UserField> fields = realm.where(UserField.class).findAll();

        if (fields == null || fields.size() == 0) {
            return request.getUserField(new User.UserRequest(Integer.parseInt(getUser().getUSER_ID())))
                    .subscribeOn(Schedulers.newThread())
                    .doOnNext(fields1 -> {
                        realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        realm.copyToRealm(fields1);
                        realm.commitTransaction();
                    })
                    .blockingLast();
        } else {
            return realm.where(UserField.class).findAll();
        }
    }


    public List<UserField> getUserField(int userid) {
        return request.getUserField(new User.UserRequest(userid))
                .subscribeOn(Schedulers.newThread())
                .blockingLast();
    }

    public void setupKategori(final RecyclerView kategori) {
        kategori.invalidate();
        kategori.setAdapter(new UserKategoriAdapter(getField()));
    }

    public List<Field> getField() {
        realm = Realm.getDefaultInstance();
        List<Field> fields = realm.where(Field.class).findAll();

        if (fields == null || fields.size() == 0) {
            return request.getFields()
                    .subscribeOn(Schedulers.newThread())
                    .doOnNext(fields1 -> {
                        realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        realm.copyToRealm(fields1);
                        realm.commitTransaction();
                    })
                    .blockingLast();
        } else {
            return realm.where(Field.class).findAll();
        }
    }

    public void setupKategoriUpdatable(RecyclerView kategori) {

        if (getUserField() != null || getUserField().size() > 0) {
            kategori.setAdapter(new UserKategoriAdapter(getField(), getUserField()));
        } else {
            kategori.setAdapter(new UserKategoriAdapter(getField()));
        }
    }

    public void gotoMessage() {
        //load message here

        Fragment fragment = ChatFragment.newInstance("", "");
        view.ChangeFragment(fragment, new String[]{"chat_list"}, true);
    }

    public void checkUser(boolean new_user, int user_Type, String name, String passwd, String User_id) {
        if (new_user) {
            Fragment fragment = InitiateFragment.newInstance(user_Type, name, passwd);
            view.ChangeFragment(fragment, new String[]{"initiate"}, false);
            view.showProgress(true);
        } else if (preferences.getBoolean(KEY_CHECK_DONE, false) == true) {
            Log.d("SUDAH DICEK", "OKE");
            view.ChangeFragment(MainFragment.newInstance(null, null), new String[]{"main"}, false);
            if (getUser().getUser_Type() == 3) {
                mainInterface.setupDrawer(true);
            } else {
                mainInterface.setupDrawer(false);
            }
        } else if (preferences.getBoolean(KEY_SKIP_INITIATE, false) == true) {
            User.UserRequest userRequest = new User.UserRequest(Integer.parseInt(getUser().getUSER_ID()));
            retreiveUserData(userRequest);
        } else {
            checkIsNew(User_id);
        }
    }

    public void submitData(int type, String s, int gender, RecyclerView recKategori) {
        Observable<List<String>> req = null;

        Log.d("ID USER @ INITIATE", ID_USER + "");
        view.showProgress(true);
        switch (type) {
            case 1:
                User.InitiateModel model = new User.InitiateModel(ID_USER, gender, s);
                req = request.initiateModel(model);
                break;
            case 2:
                User.InitiateAgency agency = new User.InitiateAgency(ID_USER, s);
                req = request.initiateAgency(agency);
                break;
            case 3:
                User.InitiateSeeker seeker = new User.InitiateSeeker(ID_USER, s);
                req = request.initiateSeeker(seeker);
                break;
        }

        disposable.add(
                req.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<List<String>>() {
                            @Override
                            public void onNext(List<String> value) {
                                Log.d("VALUE INIT", value.get(0));
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {
                                sendUserField(recKategori, "main", ID_USER, type);
                            }
                        })
        );
    }

    private void sendUserField(RecyclerView recKategori, String gotos, int User_Id, int User_Type) {
        UserKategoriAdapter adapter = (UserKategoriAdapter) recKategori.getAdapter();
        HashMap<Integer, String> values = adapter.getSelectedUserField();
        Set<?> set = values.entrySet();
        Iterator<?> iterator = set.iterator();

        while (iterator.hasNext()) {
            Map.Entry map = (Map.Entry) iterator.next();

            Field.UserFieldRequest param = new Field.UserFieldRequest(map.getValue().toString(),
                    Integer.parseInt(map.getKey().toString()),
                    User_Id);


            request.sendUserField(param)
                    .subscribeOn(Schedulers.io())
                    .doOnNext(value -> {
                        Log.d("VALUE FIELD", value.get(0));

                        if (value.get(0).equalsIgnoreCase("Success")) {

                            realm = Realm.getDefaultInstance();
                            realm.beginTransaction();
                            RealmResults<UserField> rows = realm.where(UserField.class)
                                    .equalTo("User_Id", User_Id)
                                    .findAll();

                            rows.deleteAllFromRealm();
                            realm.commitTransaction();
                        }
                    })
                    .blockingLast();
        }

        view.showProgress(false);
        preferences.edit().putBoolean(KEY_LOGGEDIN, true).apply();
        preferences.edit().putBoolean(KEY_CHECK_DONE, true).apply();
        getProfile(User_Id, User_Type, gotos);
    }

    public void skipInit() {
        preferences.edit().putBoolean(KEY_SKIP_INITIATE, true).apply();
        view.ChangeFragment(MainFragment.newInstance(null, null), new String[]{"main"}, false);

        if (preferences.getInt(KEY_USER_TYPE, -1) == 3) {
            mainInterface.setupDrawer(true);
        } else {
            mainInterface.setupDrawer(false);
        }
    }

    public void setupInitiation(String email, String passwd) {
        User.UserLogin loginReq = new User.UserLogin(email, passwd);

        disposable.add(
                request.loginUser(loginReq)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<List<String>>() {

                            @Override
                            public void onNext(List<String> value) {
                                Log.d("VALUE initiate", value.toString());

                                if (value.get(0).equalsIgnoreCase("success"))
                                    ID_USER = Integer.parseInt(value.get(1));
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {
                                getDataUser(ID_USER);
                            }
                        })
        );
    }

    public void getDataUser(int user_id) {

        User.UserRequest userRequest = new User.UserRequest(user_id);

        disposable.add(
                request.getDataUser(userRequest)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<List<User>>() {
                            @Override
                            public void onNext(List<User> value) {
                                realm = Realm.getDefaultInstance();

                                realm.beginTransaction();
                                realm.copyToRealmOrUpdate(value.get(0));
                                realm.commitTransaction();
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {
                                view.showProgress(false);
                            }
                        }));
    }

    public void submitProfileData(String name, String lokasi, String phone, int gender,
                                  String dob, int age, int height, int weight, String logoUrl, String about,
                                  int since, String videoUrl, String website, RecyclerView recKategori) {

        view.showProgress(true);

        Profile.ProfileData data = new Profile.ProfileData(getUser().getUser_Type(), Integer.parseInt(getUser().getUSER_ID()),
                name, lokasi, phone, gender, dob, age, height, weight, logoUrl, about, since, videoUrl, website);

        disposable.add(
                request.updateProfile(data)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<List<String>>() {
                            @Override
                            public void onNext(List<String> value) {
                                Toast.makeText(view, "UPDATE RESULT " + value.get(0), Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {
                                if (recKategori != null) {
                                    deleteUserField(recKategori);
                                }
                            }
                        })
        );
    }

    public void applyModelFilter(String gender, int usia1, int usia2, int height1,
                                 int height2, int weight1, int weight2, String lokasi, RecyclerView recKategori) {

        view.showProgress(true);

        UserKategoriAdapter adapter = (UserKategoriAdapter) recKategori.getAdapter();
        HashMap<Integer, String> values = adapter.getSelectedUserField();
        Set<?> set = values.entrySet();
        Iterator<?> iterator = set.iterator();

        String kategori = "(";

        if (values.size() > 0) {
            Log.d("VALUE FILTER", values.size() + "");

            while (iterator.hasNext()) {
                Map.Entry map = (Map.Entry) iterator.next();

                kategori += map.getKey() + "";

                if (iterator.hasNext()) {
                    kategori += ",";
                }
            }
            kategori += ")";
        } else {
            Log.d("LIST FILTER", getField().size() + "");
            Iterator<Field> iter = getField().iterator();

            while (iter.hasNext()) {

                kategori += iter.next().getField_Id() + "";

                if (iter.hasNext()) {
                    kategori += ",";
                }
            }

            kategori += ")";
        }


        Log.d("Kategori Filter", kategori);

        ProfileModel.RequestFilter requestFilter = new ProfileModel.RequestFilter(getUser().getUSER_ID(), gender, usia1,
                usia2, height1, height2, weight1, weight2, lokasi, kategori);

        disposable.add(
                request.browseModel(requestFilter)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<List<BrowseModelResult>>() {
                            @Override
                            public void onNext(List<BrowseModelResult> value) {
                                ModelResult = value;
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {
                                view.showProgress(false);
                                view.ChangeFragment(MainEntryFragment.newInstance("model", true), new String[]{"model_entry"}, false);
                            }
                        })
        );
    }

    public ArrayList<BrowseModelResult> getCurrentModelResult() {
        return (ArrayList<BrowseModelResult>) ModelResult;
    }

    public ArrayList<BrowseAgencyResult> getCurrentAgencyResult() {
        return (ArrayList<BrowseAgencyResult>) AgencyResult;
    }

    public void applyModelFilterDefault() {
        view.showProgress(true);
        String kategori = "(";

        Iterator<Field> iterator = getField().iterator();

        while (iterator.hasNext()) {

            kategori += iterator.next().getField_Id() + "";

            if (iterator.hasNext()) {
                kategori += ",";
            }
        }

        kategori += ")";

        Log.d("Kategori Filter", kategori);

        ProfileModel.RequestFilter requestFilter = new ProfileModel.RequestFilter(getUser().getUSER_ID(), "(1,2)", 18,
                40, 150, 200, 40, 100, "", kategori);

        disposable.add(
                request.browseModel(requestFilter)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<List<BrowseModelResult>>() {
                            @Override
                            public void onNext(List<BrowseModelResult> value) {
                                ModelResult = value;
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {
                                view.showProgress(false);
                                Fragment fragment = MainEntryFragment.newInstance("model", true);
                                mainInterface.ChangeFragment(fragment, new String[]{"model_frame"}, true);
                                mainInterface.setTitle("MODEL");
                            }
                        })
        );
    }

    public void applyAgencyFilterDefault() {
        view.showProgress(true);
        String kategori = "(";

        Iterator<Field> iterator = getField().iterator();

        while (iterator.hasNext()) {

            kategori += iterator.next().getField_Id() + "";

            if (iterator.hasNext()) {
                kategori += ",";
            }
        }

        kategori += ")";

        Log.d("Kategori Filter", kategori);

        BrowseRequest.BrowseAgency requestFilter = new BrowseRequest.BrowseAgency(getUser().getUSER_ID(), "", kategori);

        disposable.add(
                request.browseAgency(requestFilter)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<List<BrowseAgencyResult>>() {
                            @Override
                            public void onNext(List<BrowseAgencyResult> value) {
                                AgencyResult = value;
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {
                                view.showProgress(false);
                                Fragment fragment = MainEntryFragment.newInstance("agency", true);
                                mainInterface.ChangeFragment(fragment, new String[]{"agency_frame"}, true);
                                mainInterface.setTitle("AGENSI");
                            }
                        })
        );
    }

    public void applyAgencyFilter(String lokasi, RecyclerView recKategori) {
        view.showProgress(true);

        UserKategoriAdapter adapter = (UserKategoriAdapter) recKategori.getAdapter();
        HashMap<Integer, String> values = adapter.getSelectedUserField();
        Set<?> set = values.entrySet();
        Iterator<?> iterator = set.iterator();

        String kategori = "(";

        if (values.size() > 0) {
            Log.d("VALUE FILTER", values.size() + "");

            while (iterator.hasNext()) {
                Map.Entry map = (Map.Entry) iterator.next();

                kategori += map.getKey() + "";

                if (iterator.hasNext()) {
                    kategori += ",";
                }
            }
            kategori += ")";
        } else {
            Log.d("LIST FILTER", getField().size() + "");
            Iterator<Field> iter = getField().iterator();

            while (iter.hasNext()) {

                kategori += iter.next().getField_Id() + "";

                if (iter.hasNext()) {
                    kategori += ",";
                }
            }

            kategori += ")";
        }


        Log.d("Kategori Filter", kategori);

        BrowseRequest.BrowseAgency requestFilter = new BrowseRequest.BrowseAgency(getUser().getUSER_ID(), lokasi, kategori);

        disposable.add(
                request.browseAgency(requestFilter)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<List<BrowseAgencyResult>>() {
                            @Override
                            public void onNext(List<BrowseAgencyResult> value) {
                                AgencyResult = value;
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {
                                view.showProgress(false);
                                view.ChangeFragment(MainEntryFragment.newInstance("agency", true), new String[]{"agency_entry"}, false);
                            }
                        })
        );
    }

    public BrowseModelResult getResultModel(int val_user_id) {
        view.showProgress(true);
        Request.UserRequest modelRequest = new Request.UserRequest(val_user_id, 1);

        return request.getModelData(modelRequest)
                .subscribeOn(Schedulers.io())
                .blockingLast().get(0);
    }

    public BrowseAgencyResult getResultAgency(int val_user_id) {
        view.showProgress(true);
        Request.UserRequest modelRequest = new Request.UserRequest(val_user_id, 2);

        return request.getAgencyData(modelRequest)
                .subscribeOn(Schedulers.io())
                .blockingLast().get(0);
    }

    public void giveRates(int userId, float rating, RatingView ratingModel) {
        view.showProgress(true);
        disposable.add(
                request.giveRates(new Request.GiveRate(userId, (int) rating))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<List<String>>() {
                            String res;

                            @Override
                            public void onNext(List<String> value) {
                                res = value.get(0);
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {
                                if (res.equalsIgnoreCase("Success")) {
                                    Toast.makeText(view, "Sukses!", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(view, "Gagal!", Toast.LENGTH_SHORT).show();
                                }
                                ratingModel.setRating(getRates(String.valueOf(userId)));
                            }
                        })
        );
    }

    public Float getRates(String user_id) {
        List<String> result = request.getRates(new Request.GetRates(Integer.parseInt(user_id)))
                .subscribeOn(Schedulers.io())
                .blockingLast();

        String dec = result.get(1);
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.CEILING);
        Number number = null;

        try {
            number = Float.valueOf(dec);
        } catch (NullPointerException e) {
            number = 0;
        }

        return Float.valueOf(df.format(number.doubleValue()));
    }

    public int getActiveChat() {
        return MESSAGE_ID;
    }

    public List<MessageThread> getChat(int val_dest, int val_from) {
        List<HistoryMessage> messages;

        realm = Realm.getDefaultInstance();
        messages = realm.where(HistoryMessage.class)
                .equalTo("User_One", val_dest).or().equalTo("User_Two", val_dest)
                .equalTo("User_One", val_from).or().equalTo("User_Two", val_from)
                .findAll();

        if (messages.size() == 0) {
            messages = request.getMessage(new Request.RequestMessage(val_from, val_dest))
                    .subscribeOn(Schedulers.io())
                    .doOnNext(message -> {
                        realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(message);
                        realm.commitTransaction();
                    })
                    .blockingLast();
        }

        if (messages != null && messages.size() > 0) {
            MESSAGE_ID = messages.get(0).getMessage_Id();
            realm = Realm.getDefaultInstance();
            List<MessageThread> messageThreads = realm.copyFromRealm(realm.where(MessageThread.class).equalTo("Message_Id", MESSAGE_ID).findAll());
            /*if (messageThreads.size() > 0) {
                return messageThreads;
            } else {*/
            return request.getMessageList(new Request.RequestMessageList(
                    messages.get(0).getMessage_Id()
            ))
                    .subscribeOn(Schedulers.io())
                    .doOnNext(thread -> {
                        realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(thread);
                        realm.commitTransaction();
                    })
                    .blockingLast();
//            }
        } else {
            return new ArrayList<>();
        }
    }

    public List<MessageThread> getChat(int MessageId, RecyclerView recyclerView) {
        realm = Realm.getDefaultInstance();
        List<MessageThread> messageThreads = realm.copyFromRealm(realm.where(MessageThread.class).equalTo("Message_Id", MessageId).findAll());

        chatObserver = new DisposableObserver<List<MessageThread>>() {

            @Override
            public void onNext(List<MessageThread> value) {
                if (value.size() > recyclerView.getAdapter().getItemCount()) {
                    ((ChatAdapter) recyclerView.getAdapter()).updateAdapter(value);
                    recyclerView.smoothScrollToPosition(recyclerView.getAdapter().getItemCount());

                    realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(value);
                    realm.commitTransaction();

                    Log.d("MORE VALUE", "YEAH");
                }
                Log.d("ON REPEAT", "YEAH");
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                Log.d("ON COMPLETE", "YEAH");
            }
        };

        if (messageThreads.size() > 0) {
            Log.d("GET CHAT FROM DB", "RUN AND SIZED " + messageThreads.size());

            request.getMessageList(new Request.RequestMessageList(
                    MessageId
            ))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .retry()
                    .repeat()
                    .subscribeWith(chatObserver);

            return messageThreads;
        } else {
            Log.d("GET CHAT FROM SERVER", "RUN");
            return request.getMessageList(new Request.RequestMessageList(
                    MessageId
            ))
                    .subscribeOn(Schedulers.io())
                    .doOnNext(thread -> {
                        realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(thread);
                        realm.commitTransaction();
                    })
                    .blockingLast();
//        }
        }
    }

    public void sendChat(int activeChat, int val_dest, String body, String timeStamp, RecyclerView recChat) {
        request.sendMessage(new Request.RequestSendMessage(activeChat, val_dest, body, timeStamp))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retryWhen(attempt -> attempt.zipWith(Observable.range(1, 30), (n, i) -> i).flatMap(i -> {
                    Log.d("ON RETRY", "delay retry by " + i + " second(s)");
                    MessageThread thread = new MessageThread(0, activeChat,
                            val_dest, timeStamp, body, false, false, false, true);
                    ((ChatAdapter) recChat.getAdapter()).addToList(thread, recChat.getAdapter().getItemCount() - 1, 1);
                    return Observable.timer(i, TimeUnit.SECONDS);
                }).subscribeOn(AndroidSchedulers.mainThread()))
                .subscribeWith(new DisposableObserver<List<String>>() {
                    String status;

                    @Override
                    public void onNext(List<String> value) {
                        status = value.get(0);

                        Log.d("VALUE", status + " " + value.get(1));

                        if (status.equalsIgnoreCase("Success")) {
                            realm = Realm.getDefaultInstance();
                            MessageThread thread = new MessageThread(Integer.parseInt(value.get(1)), activeChat,
                                    val_dest, timeStamp, body, false, false, false, false);
                            realm.beginTransaction();
                            realm.copyToRealmOrUpdate(thread);
                            realm.commitTransaction();

                            ((ChatAdapter) recChat.getAdapter()).addToList(thread, recChat.getAdapter().getItemCount() - 1);
                        } else {
                            MessageThread thread = new MessageThread(Integer.parseInt(value.get(1)), activeChat,
                                    val_dest, timeStamp, body, false, false, false, true);
                            ((ChatAdapter) recChat.getAdapter()).addToList(thread, recChat.getAdapter().getItemCount() - 1);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        MessageThread thread = new MessageThread(0, activeChat,
                                val_dest, timeStamp, body, false, false, false, true);
                        ((ChatAdapter) recChat.getAdapter()).addToList(thread, recChat.getAdapter().getItemCount() - 1);
                    }

                    @Override
                    public void onComplete() {
                        if (status.equalsIgnoreCase("Success")) {
                            realm = Realm.getDefaultInstance();
                            MessageThread thread = new MessageThread();
                        } else {

                        }
                        dispose();
                    }
                });

    }

    public List<Message> getChatList(RecyclerView recChat) {
        realm = Realm.getDefaultInstance();

        if (realm.where(Message.class).findAll().size() > 0) {
            request.getMessageList(new Request.GetRates(Integer.parseInt(getUser().getUSER_ID())))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<List<Message>>() {
                        @Override
                        public void onNext(List<Message> value) {
                            realm = Realm.getDefaultInstance();
                            realm.beginTransaction();
                            realm.copyToRealmOrUpdate(value);
                            realm.commitTransaction();
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                            dispose();
                        }
                    });

            return realm.where(Message.class).findAll();
        } else {
            return request.getMessageList(new Request.GetRates(Integer.parseInt(getUser().getUSER_ID())))
                    .subscribeOn(Schedulers.io())
                    .doOnNext(messages -> {
                        realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(messages);
                        realm.commitTransaction();
                    })
                    .blockingLast();
        }
    }

    public void doLogout() {
        //remove user data
        realm.executeTransaction(realm1 -> {
            realm.delete(User.class);
            realm.delete(ProfileModel.class);
            realm.delete(ProfileAgency.class);
            realm.delete(ProfileSeeker.class);
            realm.delete(UserField.class);
            realm.delete(Photo.class);
            realm.delete(Video.class);
        });

        preferences.edit().putBoolean(KEY_CHECK_DONE, false).apply();
        preferences.edit().putBoolean(KEY_SKIP_INITIATE, false).apply();
        preferences.edit().putBoolean(KEY_LOGGEDIN, false).apply();
        Intent intent = new Intent(view, LoginActivity.class);
        view.startActivity(intent);
        view.finish();
    }

    public void Destroy() {
        Log.d("DESTROY CALLED", "true");
        disposable.clear();
        if (chatObserver != null && !chatObserver.isDisposed())
            chatObserver.dispose();
    }

    public interface MainInterface {
        MainPresenter getPresenter();

        void ChangeFragment(Fragment fragment, String[] args, boolean poppedBackStack);

        void setupDrawer(boolean isGuest);

//        void updateDrawer(String photoPath);

        void setTitle(String title);

        void turnBehavior(boolean flag);

        void refreshUser();

        void setUserData(User user);
    }
}
