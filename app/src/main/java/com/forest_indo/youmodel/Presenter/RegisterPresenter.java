package com.forest_indo.youmodel.Presenter;

import android.content.SharedPreferences;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.forest_indo.youmodel.Model.Register;
import com.forest_indo.youmodel.Utils.APIUtils.APIRequest;
import com.forest_indo.youmodel.Utils.Dependencies.AppController;
import com.forest_indo.youmodel.View.RegisterActivity;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Koucluck on 9/1/2016.
 */
public class RegisterPresenter {

    RegisterInterface registerInterface;
    @Inject
    APIRequest request;
    @Inject
    SharedPreferences sharedPreferences;
    private RegisterActivity view;
    private int UserType;
    private CompositeDisposable disposable;

    public RegisterPresenter(RegisterActivity context, RegisterInterface registerInterface) {
        this.registerInterface = registerInterface;

        view = context;

        disposable = new CompositeDisposable();
        ((AppController) context.getApplication()).getAppComponent().inject(this);
    }

    public void showPass(EditText etPasswd, boolean status) {
        if (status == false) {
            etPasswd.setTransformationMethod(HideReturnsTransformationMethod
                    .getInstance());

            registerInterface.setStatusPassword(true);
        } else {
            etPasswd.setTransformationMethod(PasswordTransformationMethod
                    .getInstance());

            registerInterface.setStatusPassword(false);
        }
    }

    public void registerUser(String email, final String name, final String passwd) {

        String token = FirebaseInstanceId.getInstance().getToken();

        if (email.isEmpty() || name.isEmpty() || passwd.isEmpty()) {
            registerInterface.showError("Harap lengkapi semua isian!");
        } else {
            view.showProgress(true);
            Register register = new Register(email, passwd, name, token, UserType);
            disposable.add(
                    request.registerUser(register)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(new DisposableObserver<List<String>>() {
                                @Override
                                public void onNext(List<String> value) {
                                    checkRegisterStatus(value, email, passwd);
                                }

                                @Override
                                public void onError(Throwable e) {
                                    e.printStackTrace();
                                }

                                @Override
                                public void onComplete() {
                                    view.showProgress(false);
                                }
                            })
            );
        }
    }

    private void checkRegisterStatus(List<String> value, String name, String passwd) {
        if (value.get(0).equalsIgnoreCase("Success")) {
            view.showProgress(false);
            registerInterface.showDialog(name, passwd);
        } else {
            view.showProgress(false);
            Toast.makeText(view, "Register Gagal", Toast.LENGTH_SHORT).show();
            Log.d("Register Status", value.get(0) + "-" + value.get(1));
        }
    }

    public void onDestroy() {
        disposable.clear();
        view = null;
    }

    public int getUserType() {
        return UserType;
    }

    public void setUserType(int userType) {
        UserType = userType;
    }

    public interface RegisterInterface {
        void dialogChoosen(int user_type);

        void showDialog(String name, String passwd);

        void setStatusPassword(boolean b);

        void showError(String msg);
    }
}