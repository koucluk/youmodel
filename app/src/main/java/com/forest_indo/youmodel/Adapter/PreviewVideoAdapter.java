package com.forest_indo.youmodel.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.forest_indo.youmodel.Model.Video;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.Constant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Koucluck on 12/8/2016.
 */

public class PreviewVideoAdapter extends RecyclerView.Adapter<PreviewVideoAdapter.PreviewAdapterViewHolder> implements Constant {

    List<Video> Videos;
    Context context;
    List<Video> uploadingVideo;
    List<Integer> uploadingIndex;

    public PreviewVideoAdapter(List<Video> videos, Context context) {
        uploadingVideo = new ArrayList<>();
        Videos = videos;
        this.context = context;
    }

    @Override
    public PreviewAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video, parent, false);

        return new PreviewAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PreviewAdapterViewHolder holder, int position) {
        Log.d("UPDATED VIEW", "run");
        if (Videos.get(position).isUploading()) {
            holder.bar.setIndeterminate(true);
            holder.bar.setVisibility(View.VISIBLE);
            holder.imgThumbVideo.setImageResource(R.drawable.placeholder_image);
            holder.imgPlay.setVisibility(View.GONE);
        } else if (Videos.get(position).isSuccess()) {
            holder.bar.setVisibility(View.GONE);
            holder.imgPlay.setVisibility(View.VISIBLE);

            Log.d("URL THUMB", GALERY_THUMBS_SERVER_URL + Videos.get(position).getThumb_url());
            Picasso.with(context)
                    .load(GALERY_THUMBS_SERVER_URL + Videos.get(position).getThumb_url())
                    .resize(160, 160)
                    .centerCrop()
                    .into(holder.imgThumbVideo);
        } else {
            holder.bar.setVisibility(View.GONE);

            Log.d("URL THUMB", GALERY_THUMBS_SERVER_URL + Videos.get(position).getThumb_url());
            Picasso.with(context)
                    .load(GALERY_THUMBS_SERVER_URL + Videos.get(position).getThumb_url())
                    .resize(160, 160)
                    .centerCrop()
                    .into(holder.imgThumbVideo);
        }
    }

    public void updateItem(Video video) {
        Log.d("VIDEO UPLOADING", "RUN");
        Videos.add(0, video);
        uploadingVideo.add(0, video);
        notifyItemInserted(0);
        notifyDataSetChanged();
    }

    public void addNewItem(List<Video> videos) {
        Videos = videos;
        Log.d("VIDEO UPLOADING", "RUN");
        notifyDataSetChanged();
    }

    public int getVideoCount() {
        return Videos.size();
    }

    public void updateStatusUpload(int position, Video video) {
        Videos.set(position, video);
        notifyItemChanged(position);
    }

    @Override
    public int getItemCount() {
        if (Videos != null)
            return Videos.size() >= 4 ? 4 : Videos.size();
        else
            return 0;
    }

    public class PreviewAdapterViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_thumb_video)
        ImageView imgThumbVideo;

        @BindView(R.id.progress_item_upload)
        ProgressBar bar;

        @BindView(R.id.img_play)
        ImageView imgPlay;

        public PreviewAdapterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
