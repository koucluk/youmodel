package com.forest_indo.youmodel.Adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.forest_indo.youmodel.Model.MessageThread;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.View.CustomTextViewPhar;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Koucluck on 9/26/2016.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatView> {

    List<MessageThread> messageThreads = new ArrayList<>();
    int UserActive;
    int COUNTER;

    public ChatAdapter(List<MessageThread> messageThread, int user_active) {
        messageThreads = messageThread;
        UserActive = user_active;
    }

    @Override
    public ChatView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chat, parent, false);

        return new ChatView(view);
    }

    @Override
    public void registerAdapterDataObserver(RecyclerView.AdapterDataObserver observer) {
        super.registerAdapterDataObserver(observer);
    }

    @Override
    public void onBindViewHolder(ChatView holder, int position) {
        if (messageThreads.get(position).getRecipient_Id() == UserActive) {
            holder.holderRev.setVisibility(View.VISIBLE);
            holder.holderSent.setVisibility(View.GONE);
            holder.msgRev.setText(messageThreads.get(position).getMessage_Body());
        } else {
            holder.holderRev.setVisibility(View.GONE);
            holder.holderSent.setVisibility(View.VISIBLE);
            holder.msgSent.setText(messageThreads.get(position).getMessage_Body());
        }

        if (messageThreads.get(position).isIsSending()) {
            holder.txtStatus.setVisibility(View.VISIBLE);
        } else if (messageThreads.get(position).isIs_Error()) {
            holder.txtStatus.setVisibility(View.VISIBLE);
            holder.txtStatus.setText("Failed, Retrying... (" + COUNTER + ")");
        } else {
            holder.txtStatus.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBindViewHolder(ChatView holder, int position, List<Object> payloads) {

        if (payloads != null && !payloads.isEmpty() && (payloads.get(0) instanceof MessageThread)) {
            Log.d("UPDATE CALLED", ((MessageThread) payloads.get(0)).getRecipient_Id() + "");
            if (((MessageThread) payloads.get(0)).isIs_Error()) {
                holder.txtStatus.setText("Failed");
            } else {
                Log.d("UPDATE SUCCESS CALLED", ((MessageThread) payloads.get(0)).getRecipient_Id() + "");
                holder.txtStatus.setVisibility(View.GONE);
            }
        } else {
            super.onBindViewHolder(holder, position, payloads);
        }
    }

    public void updateSentStatus(boolean status) {

    }

    public void updateAdapter(List<MessageThread> messages) {
        messageThreads = messages;
        notifyDataSetChanged();
    }

    public void addToList(MessageThread thread) {
        messageThreads.add(thread);
        notifyDataSetChanged();
    }

    public void addToList(MessageThread thread, int position) {
        Log.d("POSITION UPDATED", position + "");
        messageThreads.set(position, thread);
        notifyItemChanged(position, messageThreads);
    }

    public void addToList(MessageThread thread, int position, int counter) {
        Log.d("POSITION UPDATED", position + "");
        COUNTER = counter;
        messageThreads.set(position, thread);
        notifyItemChanged(position, messageThreads);
    }

    @Override
    public int getItemCount() {
        return messageThreads.size();
    }

    public class ChatView extends RecyclerView.ViewHolder {

        LinearLayout holderRev, holderSent;
        CustomTextViewPhar msgRev, msgSent, timeRev, timeSent, txtStatus;

        public ChatView(View itemView) {
            super(itemView);

            holderRev = (LinearLayout) itemView.findViewById(R.id.holder_chat_receive);
            holderSent = (LinearLayout) itemView.findViewById(R.id.holder_chat_sent);

            msgRev = (CustomTextViewPhar) itemView.findViewById(R.id.txt_message_rev);
            msgSent = (CustomTextViewPhar) itemView.findViewById(R.id.txt_message_sent);

            timeRev = (CustomTextViewPhar) itemView.findViewById(R.id.txt_time_rev);
            timeSent = (CustomTextViewPhar) itemView.findViewById(R.id.txt_time_sent);
            txtStatus = (CustomTextViewPhar) itemView.findViewById(R.id.txt_status_sent);
        }
    }
}
