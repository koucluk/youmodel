package com.forest_indo.youmodel.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.forest_indo.youmodel.Model.Field;
import com.forest_indo.youmodel.Model.UserField;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.View.CustomTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Koucluck on 8/31/2016.
 */
public class UserKategoriAdapter extends RecyclerView.Adapter<UserKategoriAdapter.UserKategoriViewHolder> {

    HashMap<Integer, String> selected;

    List<Field> fields = null;
    List<UserField> userFields = null;

    Context context;

    public UserKategoriAdapter(List<Field> fields) {
        this.fields = fields;
        userFields = new ArrayList<>();
        selected = new HashMap<>();
    }

    public UserKategoriAdapter(List<Field> fields, List<UserField> userfields) {
        this.fields = fields;
        userFields = userfields;
        selected = new HashMap<>();
    }

    @Override
    public UserKategoriViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_kategori, parent, false);

        return new UserKategoriViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final UserKategoriViewHolder holder, final int position) {

        holder.txtPilih.setText(fields.get(position).getField_Name().toUpperCase());

        if (userFields != null || userFields.size() > 0) {
            for (int i = 0; i < userFields.size(); i++) {
                if (fields.get(position).getField_Id() == userFields.get(i).getField_Id()) {
                    selected.put(fields.get(position).getField_Id(), fields.get(position).getField_Name());
                    holder.btnPilih.setBackgroundResource(R.drawable.main_button_fill_pink_narrow);
                    holder.txtPilih.setTextColor(context.getResources().getColor(android.R.color.white));
                }
            }

            Log.d("SELECTED SIZE", selected.size() + "");
        }

        holder.btnPilih.setOnClickListener(view -> {
            if (!selected.containsKey(fields.get(position).getField_Id())) {
                selected.put(fields.get(position).getField_Id(), fields.get(position).getField_Name());
                holder.btnPilih.setBackgroundResource(R.drawable.main_button_fill_pink_narrow);
                holder.txtPilih.setTextColor(context.getResources().getColor(android.R.color.white));
            } else {
                selected.remove(fields.get(position).getField_Id());
                holder.btnPilih.setBackgroundResource(R.drawable.main_button_outline_pink_narrow);
                holder.txtPilih.setTextColor(context.getResources().getColor(R.color.base_pink));
            }
        });

    }

    public HashMap<Integer, String> getSelectedUserField() {
        return selected;
    }

    @Override
    public int getItemCount() {
        return fields.size();
    }

    class UserKategoriViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout btnPilih;
        CustomTextView txtPilih;

        public UserKategoriViewHolder(View itemView) {
            super(itemView);

            btnPilih = (RelativeLayout) itemView.findViewById(R.id.btn_pilih_kategori);
            txtPilih = (CustomTextView) itemView.findViewById(R.id.txt_pilih_kategori);
        }
    }
}
