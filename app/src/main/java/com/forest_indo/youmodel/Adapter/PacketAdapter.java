package com.forest_indo.youmodel.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.View.CustomButton;
import com.forest_indo.youmodel.Utils.View.CustomTextView;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by FIN_MULTIMEDIA on 28/09/2016.
 */

public class PacketAdapter extends RecyclerView.Adapter<PacketAdapter.PacketView> {

    int LAST_OPENED = 0;
    ExpandableLinearLayout expandableLinearLayout;

    @Override
    public PacketView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_packet, parent, false);
        return new PacketView(view);
    }

    @Override
    public void onBindViewHolder(final PacketView holder, final int position) {

        holder.setIsRecyclable(false);
        holder.holderBuyExpand.initLayout();
        holder.holderBuyExpand.setInRecyclerView(true);

        switch (position) {
            case 0:
                holder.holderBuy.setBackgroundResource(R.color.gray_white);
                break;
            case 1:
                holder.holderBuy.setBackgroundResource(R.color.gray_deep);
                break;
            case 2:
                holder.holderBuy.setBackgroundResource(R.color.base_pink);
                break;
        }

        holder.holderBuy.setOnClickListener(v -> toggle(holder.holderBuyExpand));

        holder.btnBuy.setOnClickListener(v -> toggle(holder.holderBuyExpand));
    }

    private void toggle(ExpandableLinearLayout expand) {
        if (!expand.isExpanded()) {
            closeExpanded();
            expand.expand();

            setLastExpandView(expand);
        } else {
            expandableLinearLayout = null;
            expand.collapse();
        }
    }

    private void closeExpanded() {
        if (expandableLinearLayout != null) {
            expandableLinearLayout.collapse();
        }
    }

    private void setLastExpandView(ExpandableLinearLayout expand) {
        expandableLinearLayout = expand;
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class PacketView extends RecyclerView.ViewHolder {

        @BindView(R.id.holder_buy_toggle)
        RelativeLayout holderBuy;

        @BindView(R.id.holder_buy_expand)
        ExpandableLinearLayout holderBuyExpand;

        @BindView(R.id.btn_buy_packet)
        CustomButton btnBuy;

        @BindView(R.id.txt_name_packet)
        CustomTextView txtName;

        public PacketView(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
