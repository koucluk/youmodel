package com.forest_indo.youmodel.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.forest_indo.youmodel.R;

/**
 * Created by Koucluck on 9/5/2016.
 */
public class SmallModelAgencyAdapter extends RecyclerView.Adapter<SmallModelAgencyAdapter.ViewHolder> {

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_model_agency, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position % 2 == 0) {
            holder.imgView.setImageResource(R.drawable.main_agency_image_daftarmodel_01);
        } else {
            holder.imgView.setImageResource(R.drawable.main_agency_image_daftarmodel_02);
        }
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgView;

        public ViewHolder(View itemView) {
            super(itemView);

            imgView = (ImageView) itemView.findViewById(R.id.img_model_agency);
        }
    }
}
