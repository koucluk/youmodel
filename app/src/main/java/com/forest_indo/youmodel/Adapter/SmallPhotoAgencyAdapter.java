package com.forest_indo.youmodel.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.forest_indo.youmodel.Model.Photo;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.Constant;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Koucluck on 9/5/2016.
 */
public class SmallPhotoAgencyAdapter extends RecyclerView.Adapter<SmallPhotoAgencyAdapter.ViewHolder> implements Constant {

    int USER_TYPE = 0;
    Context context;
    List<Photo> Photos;

    public SmallPhotoAgencyAdapter(int type, List<Photo> photos) {
        Photos = photos;
        USER_TYPE = type;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo_agency, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Picasso.with(context).load(GALERY_PHOTO_SERVER_URL + Photos.get(position).getPhoto_Url())
                .resize(160, 160)
                .centerCrop()
                .into(holder.imgView);
    }

    public void updateItem(Photo photo) {
        Photos.add(0, photo);
        notifyDataSetChanged();
    }

    public void addNewItem(List<Photo> photos) {
        Photos = photos;
        notifyDataSetChanged();
    }

    public int getPhotosCount() {
        return Photos.size();
    }

    @Override
    public int getItemCount() {
        if (Photos != null)
            return Photos.size() >= 4 ? 4 : Photos.size();
        else
            return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgView;

        public ViewHolder(View itemView) {
            super(itemView);
            imgView = (ImageView) itemView.findViewById(R.id.img_photo_agency);
        }
    }
}
