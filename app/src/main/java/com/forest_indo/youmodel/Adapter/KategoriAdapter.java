package com.forest_indo.youmodel.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.forest_indo.youmodel.Model.UserField;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.View.CustomTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Koucluck on 9/7/2016.
 */
public class KategoriAdapter extends RecyclerView.Adapter<KategoriAdapter.Holder> {

    Context context;

    List<UserField> Userfields;

    public KategoriAdapter(Context context, List<UserField> userFields) {
        this.context = context;
        Userfields = userFields;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_kategori, parent, false);

        context = parent.getContext();
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        if (Userfields != null) {
            Log.d("FIELD NAME", Userfields.get(position).getField_Name());
            holder.txtPilih.setText(Userfields.get(position).getField_Name().toUpperCase());
        }
    }

    @Override
    public int getItemCount() {
        return Userfields.size();
    }

    class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.btn_pilih_kategori)
        RelativeLayout btnPilih;

        @BindView(R.id.txt_pilih_kategori)
        CustomTextView txtPilih;

        public Holder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            txtPilih.setTextColor(context.getResources().getColor(android.R.color.white));
            btnPilih.setBackgroundResource(R.drawable.asset_button_fill_gray_narrow);
        }
    }
}
