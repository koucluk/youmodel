package com.forest_indo.youmodel.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.forest_indo.youmodel.Model.Message;
import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.Constant;
import com.forest_indo.youmodel.Utils.View.CircleTransformation;
import com.forest_indo.youmodel.Utils.View.CustomTextView;
import com.forest_indo.youmodel.Utils.View.CustomTextViewPhar;
import com.forest_indo.youmodel.View.Fragment.ChatDetailFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by FIN_MULTIMEDIA on 29/09/2016.
 */

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ChatListView> implements View.OnClickListener, Constant {

    Context context;
    MainPresenter presenter;
    MainPresenter.MainInterface mainInterface;
    List<Message> Messages = new ArrayList<>();

    public ChatListAdapter(List<Message> messageList) {
        Messages = messageList;
    }

    @Override
    public ChatListView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chat_list, parent, false);

        context = parent.getContext();
        mainInterface = (MainPresenter.MainInterface) context;
        presenter = mainInterface.getPresenter();

        return new ChatListView(view);
    }

    @Override
    public void onBindViewHolder(ChatListView holder, int position) {
        if (Messages.get(position).isMarked() && Messages.get(position).isRead()) {
            holder.imgIndicatorUnread.setBackgroundResource(R.drawable.round_chat_ava);
            holder.txtBodyChat.setTextColor(context.getResources().getColor(R.color.base_pink));
        } else {
            holder.imgIndicatorUnread.setBackgroundResource(android.R.color.white);
            holder.txtBodyChat.setTextColor(context.getResources().getColor(R.color.black_solid));
        }

        holder.txtBodyChat.setText(Messages.get(position).getLastMsg());
        holder.txtNameChat.setText(Messages.get(position).getName());

        Picasso.with(context)
                .load(PROFILEPICTURE_SERVER_URL + Messages.get(position).getPicture())
                .transform(new CircleTransformation())
                .error(R.drawable.asset_image_avatar_dafault_model)
                .placeholder(R.drawable.asset_image_avatar_dafault_model)
                .into(holder.imgAva);

        holder.ClickView.setOnClickListener(v -> {
            Fragment fragment = ChatDetailFragment.newInstance
                    (
                            Integer.parseInt(presenter.getUser().getUSER_ID()),
                            Messages.get(position).getWithId(),
                            Messages.get(position).getMessage_Id(),
                            Messages.get(position).getPicture()
                    );

            mainInterface.ChangeFragment(fragment, new String[]{"chat_frame", "message_id"}, true);
        });
    }

    public void updateList(List<Message> messages) {
        Messages = messages;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return Messages.size();
    }

    @Override
    public void onClick(View v) {
//        Fragment fragment = ChatDetailFragment.newInstance(0, 0, Messages.get());
//        mainInterface.ChangeFragment(fragment, new String[]{"chat_frame", "message_id"}, true);
    }

    public class ChatListView extends RecyclerView.ViewHolder {

        @BindView(R.id.img_ava_chat)
        ImageView imgAva;

        @BindView(R.id.holder_ava_chat)
        FrameLayout imgIndicatorUnread;

        @BindView(R.id.txt_name_chat)
        CustomTextView txtNameChat;

        @BindView(R.id.txt_body_chat)
        CustomTextViewPhar txtBodyChat;
        @BindView(R.id.txt_time_chat)
        CustomTextViewPhar txtTimeChat;

        @BindView(R.id.holder_click_chat)
        View ClickView;


        public ChatListView(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
