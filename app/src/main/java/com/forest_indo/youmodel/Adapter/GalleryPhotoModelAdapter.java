package com.forest_indo.youmodel.Adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.View.MainActivity;

/**
 * Created by Koucluck on 9/21/2016.
 */

public class GalleryPhotoModelAdapter extends RecyclerView.Adapter<GalleryPhotoModelAdapter.PhotoViewHolder> {

    TypedArray images;
    Context context;
    MainActivity activity;

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo_gallery, parent, false);
        context = parent.getContext();
        activity = (MainActivity) context;
        images = context.getResources().obtainTypedArray(R.array.random_imgs);

        return new PhotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {

        holder.imgPhoto.setImageResource(images.getResourceId(position, -1));

        holder.imgPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.showFullImage();
            }
        });

    }

    @Override
    public int getItemCount() {
        return 14;
    }

    class PhotoViewHolder extends RecyclerView.ViewHolder {

        ImageView imgPhoto;

        public PhotoViewHolder(View itemView) {
            super(itemView);

            imgPhoto = (ImageView) itemView.findViewById(R.id.img_photo_gallery);
        }
    }
}
