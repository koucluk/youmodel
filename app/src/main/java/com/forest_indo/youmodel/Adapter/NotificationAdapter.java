package com.forest_indo.youmodel.Adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.View.CustomTextViewPhar;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

/**
 * Created by FIN_MULTIMEDIA on 27/09/2016.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationView> {

    @Override
    public NotificationView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notification, parent, false);
        return new NotificationView(view);
    }

    @Override
    public void onBindViewHolder(NotificationView holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class NotificationView extends RecyclerView.ViewHolder {

        @BindView(R.id.holder_notification)
        RelativeLayout holderNotif;

        @BindView(R.id.img_ava_notif)
        ImageView imgAva;

        @BindView(R.id.txt_time_notif)
        CustomTextViewPhar txtTime;

        @BindView(R.id.btn_accept_botif)
        ImageButton btnAccept;

        @BindView(R.id.btn_reject_notif)
        ImageButton btnReject;

        public NotificationView(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
