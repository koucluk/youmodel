package com.forest_indo.youmodel.Adapter;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.forest_indo.youmodel.Model.BrowseAgencyResult;
import com.forest_indo.youmodel.Model.BrowseModelResult;
import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.View.CustomTextView;
import com.forest_indo.youmodel.View.Fragment.AgencyDetailFragment;
import com.forest_indo.youmodel.View.Fragment.ModelDetailFragment;
import com.forest_indo.youmodel.View.MainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Koucluck on 8/31/2016.
 */
public class UserEntryAdapter extends RecyclerView.Adapter<UserEntryAdapter.UserEntryViewHolder> {

    int USER_TYPE;
    MainPresenter.MainInterface activity;

    List<BrowseModelResult> ResultModel = new ArrayList<>();
    List<BrowseAgencyResult> ResultAgency = new ArrayList<>();

    public UserEntryAdapter(int User_type, ArrayList<?> browseModelResult) {
        USER_TYPE = User_type;
        if (User_type == 1) {
            ResultModel = (List<BrowseModelResult>) browseModelResult;
        } else {
            ResultAgency = (List<BrowseAgencyResult>) browseModelResult;
        }
    }

    public UserEntryAdapter(int User_Type) {

    }

    @Override
    public UserEntryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        activity = (MainActivity) parent.getContext();

        if (USER_TYPE == 1) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_model_entry, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_agency_entry, parent, false);
        }
        return new UserEntryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserEntryViewHolder holder, int position) {
        if (USER_TYPE == 2) {
            Log.d("NAME AGENCY", ResultAgency.get(position).getName());
            try {
                if (ResultAgency.get(position).getName1().equalsIgnoreCase("")) {
                    holder.txtName.setText(ResultAgency.get(position).getName().toUpperCase());
                } else {
                    holder.txtName.setText(ResultAgency.get(position).getName1().toUpperCase());
                }
            } catch (NullPointerException e) {
                holder.txtName.setText(ResultAgency.get(position).getName().toUpperCase());
            } finally {
                if (ResultAgency.get(position).isPremium()) {
                    holder.txtVIP.setVisibility(View.VISIBLE);
                } else {
                    holder.txtVIP.setVisibility(View.GONE);
                }
            }

            holder.imgUserRound.setOnClickListener(view -> {
                Fragment fragment = AgencyDetailFragment.newInstance(false, ResultAgency.get(position).getUser_Id());
                activity.ChangeFragment(fragment, new String[]{"agency_detail"}, true);
                activity.setTitle("AGENSI");
            });

        } else {
            try {
                if (ResultModel.get(position).getName1().equalsIgnoreCase("")) {
                    holder.txtName.setText(ResultModel.get(position).getName().toUpperCase());
                } else {
                    holder.txtName.setText(ResultModel.get(position).getName1().toUpperCase());
                }
            } catch (NullPointerException e) {
                holder.txtName.setText(ResultModel.get(position).getName().toUpperCase());
            } finally {
                if (ResultModel.get(position).isPremium()) {
                    holder.txtVIP.setVisibility(View.VISIBLE);
                } else {
                    holder.txtVIP.setVisibility(View.GONE);
                }
            }

            holder.imgUser.setImageResource(R.drawable.main_image_list_model_02);

            holder.imgUser.setOnClickListener(v -> {

                Fragment fragment = ModelDetailFragment.newInstance(false,
                        Integer.parseInt(ResultModel.get(position).getUser_Id()));

                activity.ChangeFragment(fragment, new String[]{"model_agency"}, true);
                activity.setTitle("DAFTAR MODEL");
            });
        }
    }

    @Override
    public int getItemCount() {
        if (USER_TYPE == 1) {
            return ResultModel.size();
        } else {
            return ResultAgency.size();
        }
    }

    class UserEntryViewHolder extends RecyclerView.ViewHolder {

        CustomTextView txtVIP, txtName;
        ImageView imgUser;
        ImageView imgUserRound;

        public UserEntryViewHolder(View itemView) {
            super(itemView);

            txtVIP = (CustomTextView) itemView.findViewById(R.id.txt_vip_badge);
            txtName = (CustomTextView) itemView.findViewById(R.id.txt_name_user_entry);
            if (USER_TYPE == 2) {
                imgUserRound = (ImageView) itemView.findViewById(R.id.img_user);
            } else {
                imgUser = (ImageView) itemView.findViewById(R.id.img_user);
            }

        }
    }
}
