package com.forest_indo.youmodel.Model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koucluck on 11/15/2016.
 */

@Getter
@Setter
public class BrowseAgencyResult {

    int User_Id;
    String Name;
    int User_Type;
    boolean Premium;
    String Logo_Url;
    String Name1;
    String Lokasi;
    String About;
    String Since;
    String Website;
    String PhoneNumber;

    public BrowseAgencyResult(int user_Id, String name, int user_Type, boolean premium, String logo_Url,
                              String name1, String lokasi, String about, String since, String website, String phoneNumber) {
        User_Id = user_Id;
        Name = name;
        User_Type = user_Type;
        Premium = premium;
        Logo_Url = logo_Url;
        Name1 = name1;
        Lokasi = lokasi;
        About = about;
        Since = since;
        Website = website;
        PhoneNumber = phoneNumber;
    }
}
