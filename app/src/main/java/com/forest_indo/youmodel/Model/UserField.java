package com.forest_indo.youmodel.Model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koucluck on 11/1/2016.
 */

@Getter
@Setter
public class UserField extends RealmObject {

    @PrimaryKey
    int Field_Id;

    @SerializedName("Field_name")
    String Field_Name;

    int User_Id;

    public UserField() {
    }
}
