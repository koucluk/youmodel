package com.forest_indo.youmodel.Model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koucluck on 11/18/2016.
 */

@Getter
@Setter
public class Notification {

    int User_Id;
    int Agency_Id;
    String Name;
    String Logo_Url;
    String TimeStamp;

    public Notification(int user_Id, int agency_Id, String name, String logo_Url, String timeStamp) {
        User_Id = user_Id;
        Agency_Id = agency_Id;
        Name = name;
        Logo_Url = logo_Url;
        TimeStamp = timeStamp;
    }
}
