package com.forest_indo.youmodel.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koucluck on 10/24/2016.
 */

@Getter
@Setter
public class ProfileModel extends RealmObject {

    @PrimaryKey
    int User_Id;

    String Photo_Url;
    String Name;
    int Gender;
    String Dob;
    int Age;
    int Weight;
    int Height;
    String Lokasi;
    int Agency_Id;
    int Pending_Hire;
    String Phone_Number;

    public ProfileModel() {
    }

    @Getter
    @Setter
    public static class RequestFilter {

        String Userid;
        String Gender;
        int Age1;
        int Age2;
        int Height1;
        int Height2;
        int Weight1;
        int Weight2;
        String Lokasi;
        String Field_Id;

        public RequestFilter(String userid, String gender, int usia1, int usia2, int height1,
                             int height2, int weight1, int weight2, String lokasi, String field_Id) {
            Userid = userid;
            Gender = gender;
            Age1 = usia1;
            Age2 = usia2;
            Height1 = height1;
            Height2 = height2;
            Weight1 = weight1;
            Weight2 = weight2;
            Lokasi = lokasi;
            Field_Id = field_Id;
        }
    }
}
