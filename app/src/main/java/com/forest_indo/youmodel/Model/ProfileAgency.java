package com.forest_indo.youmodel.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koucluck on 10/24/2016.
 */

@Getter
@Setter
public class ProfileAgency extends RealmObject {

    @PrimaryKey
    int User_id;

    String Name;
    String Logo_Url;
    String About;
    int Since;
    String Lokasi;
    String Video_Url;
    String Website;
    int Hire_Count;

    public ProfileAgency() {
    }

    public ProfileAgency(int user_id, String nameAgency, String logo_Url, String about, int since, String lokasi, String video_Url, String website, int hire_Count) {
        User_id = user_id;
        Name = nameAgency;
        Logo_Url = logo_Url;
        About = about;
        Since = since;
        Lokasi = lokasi;
        Video_Url = video_Url;
        Website = website;
        Hire_Count = hire_Count;
    }

}
