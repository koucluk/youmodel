package com.forest_indo.youmodel.Model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koucluck on 8/25/2016.
 */
@Getter
@Setter
public class User extends RealmObject {

    @PrimaryKey
    String USER_ID;

    @SerializedName("EMAIL")
    String Email;

    @SerializedName("NAME")
    String Username;

    @SerializedName("PASSWOD")
    String Password;

    @SerializedName("USER_TYPE")
    int User_Type;

    @SerializedName("ACTIVATED")
    boolean Activated;

    @SerializedName("PREMIUM")
    boolean Premium;

    @SerializedName("ONLINE")
    boolean Online;

    @SerializedName("GCM_ID")
    String GCM_ID;

    @SerializedName("IS_HIRED")
    boolean Is_hired;

    @SerializedName("RATES")
    int Rates;

    @SerializedName("LIMIT_LEFT")
    int Limit_Left;

    @SerializedName("DATE_REGISTER")
    String Date_Regiter;

    public User() {
    }

    public User(String email, String username, String password, int user_Type, boolean activated,
                boolean premium, boolean online, String GCM_ID, boolean is_hired, int rates, int limit_Left, String date_Regiter) {
        Email = email;
        Username = username;
        Password = password;
        User_Type = user_Type;
        Activated = activated;
        Premium = premium;
        Online = online;
        this.GCM_ID = GCM_ID;
        Is_hired = is_hired;
        Rates = rates;
        Limit_Left = limit_Left;
        Date_Regiter = date_Regiter;
    }

    @Getter
    @Setter
    public static class UserLogin {
        public String Email;
        public String Password;

        public UserLogin(String email, String password) {
            Email = email;
            Password = password;
        }
    }

    @Getter
    @Setter
    public static class InitiateSeeker {
        int User_id;
        String Name;

        public InitiateSeeker(int user_id, String name) {
            User_id = user_id;
            Name = name;
        }
    }

    @Getter
    @Setter
    public static class InitiateModel {
        int User_id;
        int Gender;
        String Name;

        public InitiateModel(int user_id, int gender, String name) {
            User_id = user_id;
            Gender = gender;
            Name = name;
        }
    }

    @Getter
    @Setter
    public static class InitiateAgency {
        int User_id;
        String Name;

        public InitiateAgency(int user_id, String name) {
            User_id = user_id;
            Name = name;
        }
    }

    @Getter
    @Setter
    public static class UserRequest {
        int User_id;

        public UserRequest(int user_id) {
            User_id = user_id;
        }
    }

    @Getter
    @Setter
    public static class UserIdRequest {
        int User_Id;

        public UserIdRequest(int user_id) {
            User_Id = user_id;
        }
    }
}
