package com.forest_indo.youmodel.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koucluck on 8/25/2016.
 */

@Getter
@Setter
public class Message extends RealmObject {

    @PrimaryKey
    int Message_Id;
    String Name;
    String Picture;
    String LastMsg;
    boolean isRead;
    boolean marked;
    int WithId;
    String TimeStamp;

    public Message() {
    }

    public Message(int message_Id, String name, String picture, String lastMsg, boolean isRead, boolean marked, int withId, String timeStamp) {
        Message_Id = message_Id;
        Name = name;
        Picture = picture;
        LastMsg = lastMsg;
        this.isRead = isRead;
        this.marked = marked;
        WithId = withId;
        TimeStamp = timeStamp;
    }
}
