package com.forest_indo.youmodel.Model;

import io.realm.RealmObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koucluck on 11/28/2016.
 */

@Getter
@Setter
public class HistoryMessage extends RealmObject {

    int Message_Id;
    int User_One;
    int User_Two;

    public HistoryMessage() {
    }
}
