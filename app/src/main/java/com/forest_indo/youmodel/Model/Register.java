package com.forest_indo.youmodel.Model;

/**
 * Created by Koucluck on 9/7/2016.
 */
public class Register {
    String Email;
    String Password;
    String Name;
    String GCM_ID;

    int User_Type;

    public Register(String email, String password, String name, String GCM_ID, int user_Type) {
        Email = email;
        Password = password;
        Name = name;
        this.GCM_ID = GCM_ID;
        User_Type = user_Type;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getGCM_ID() {
        return GCM_ID;
    }

    public void setGCM_ID(String GCM_ID) {
        this.GCM_ID = GCM_ID;
    }

    public int getUser_Type() {
        return User_Type;
    }

    public void setUser_Type(int user_Type) {
        User_Type = user_Type;
    }

    public class RegisterStatus {

        String Status;

        public RegisterStatus(String status) {
            Status = status;
        }

        public String getStatus() {
            return Status;
        }

        public void setStatus(String status) {
            Status = status;
        }
    }
}
