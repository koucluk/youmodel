package com.forest_indo.youmodel.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koucluck on 8/25/2016.
 */

@Getter
@Setter
public class Photo extends RealmObject {
    @PrimaryKey
    int Photo_Id;

    String Photo_Url;
    int User_Id;

    boolean Published;

    public Photo() {
    }

    public Photo(int photo_Id, String photo_Url, int user_Id, boolean published) {
        Photo_Id = photo_Id;
        Photo_Url = photo_Url;
        User_Id = user_Id;
        Published = published;
    }
}
