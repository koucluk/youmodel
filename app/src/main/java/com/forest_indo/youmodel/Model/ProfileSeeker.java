package com.forest_indo.youmodel.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koucluck on 10/24/2016.
 */

@Getter
@Setter
public class ProfileSeeker extends RealmObject {

    @PrimaryKey
    int User_Id;

    String Photo_Url;
    String Name;
    String Phone_Number;
    String Lokasi;
    boolean Is_Banned;

    public ProfileSeeker() {
    }

    public ProfileSeeker(int user_Id, String photo_Url, String name, String phone_Number, String lokasi, boolean is_Banned) {
        User_Id = user_Id;
        Photo_Url = photo_Url;
        Name = name;
        Phone_Number = phone_Number;
        Lokasi = lokasi;
        Is_Banned = is_Banned;
    }

}
