package com.forest_indo.youmodel.Model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koucluck on 11/11/2016.
 */

public class BrowseRequest {

    @Getter
    @Setter
    public static class BrowseModel {
        String Gender;
        int Age1;
        int Age2;
        int Weight1;
        int Weight2;
        int Height1;
        int Height2;
        String Lokasi;
        String Field_Id;

        public BrowseModel() {

        }
    }

    @Getter
    @Setter
    public static class BrowseAgency {
        String Userid;
        String Lokasi;
        String Field_Id;

        public BrowseAgency(String userid, String lokasi, String field_Id) {
            Userid = userid;
            Lokasi = lokasi;
            Field_Id = field_Id;
        }
    }
}
