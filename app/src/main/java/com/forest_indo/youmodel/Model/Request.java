package com.forest_indo.youmodel.Model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koucluck on 11/18/2016.
 */

public class Request {

    @Getter
    @Setter
    public static class GiveRate {
        int User_Id;
        int Value;

        public GiveRate(int user_Id, int value) {
            User_Id = user_Id;
            Value = value;
        }
    }

    @Getter
    @Setter
    public static class GetRates {
        int User_Id;

        public GetRates(int user_Id) {
            User_Id = user_Id;
        }
    }

    @Getter
    @Setter
    public static class UserRequest {
        int User_Id;
        int User_Type;

        public UserRequest(int user_Id, int user_Type) {
            User_Id = user_Id;
            User_Type = user_Type;
        }
    }

    @Getter
    @Setter
    public static class HireModel {
        int Agency_Id;
        int User_Id;

        public HireModel(int agency_Id, int user_Id) {
            Agency_Id = agency_Id;
            User_Id = user_Id;
        }
    }

    @Getter
    @Setter
    public static class UpdateLimit {
        int User_Id;
        int Limit;

        public UpdateLimit(int user_Id, int limit) {
            User_Id = user_Id;
            Limit = limit;
        }
    }

    @Getter
    @Setter
    public static class RequestMessage {
        int User1;
        int User2;

        public RequestMessage(int user1, int user2) {
            User1 = user1;
            User2 = user2;
        }
    }

    @Getter
    @Setter
    public static class RequestMessageList {
        int Message_ID;

        public RequestMessageList() {
        }

        public RequestMessageList(int message_Id) {
            Message_ID = message_Id;
        }
    }

    @Getter
    @Setter
    public static class RequestSendMessage {
        int Message_Id;
        int Recipient_Id;
        String Message_Body;
        String TimeStamp;

        public RequestSendMessage(int message_Id, int recipient_Id, String message_Body, String timeStamp) {
            Message_Id = message_Id;
            Recipient_Id = recipient_Id;
            Message_Body = message_Body;
            TimeStamp = timeStamp;
        }
    }
}
