package com.forest_indo.youmodel.Model;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koucluck on 8/9/2016.
 */
@Getter
@Setter
public class Profile {

    @SerializedName("User_Id")
    public int User_Id;

    public Profile(int user_Id) {
        User_Id = user_Id;
    }

    public static class ProfileRequest {
        int User_id;
        int User_type;

        public ProfileRequest(int user_id, int user_type) {
            User_id = user_id;
            User_type = user_type;
        }
    }

    @Getter
    @Setter
    public static class ProfileData {
        int User_Type;
        int User_Id;
        String Name;
        String Lokasi;
        String PhoneNumber;
        int Gender;
        String DOB;
        int Age;
        int Height;
        int Weight;
        String LogoUrl;
        String About;
        int Since;
        String VideoUrl;
        String Website;

        public ProfileData(int user_Type, int user_Id, String name, String lokasi, String phoneNumber,
                           int gender, String DOB, int age, int height, int weight, String logoUrl, String about,
                           int since, String videoUrl, String website) {
            User_Type = user_Type;
            User_Id = user_Id;
            Name = name;
            Lokasi = lokasi;
            PhoneNumber = phoneNumber;
            Gender = gender;
            this.DOB = DOB;
            Age = age;
            Height = height;
            Weight = weight;
            LogoUrl = logoUrl;
            About = about;
            Since = since;
            VideoUrl = videoUrl;
            Website = website;
        }
    }
}
