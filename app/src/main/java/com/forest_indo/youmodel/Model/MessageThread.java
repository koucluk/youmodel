package com.forest_indo.youmodel.Model;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koucluck on 8/25/2016.
 */

@Getter
@Setter
public class MessageThread extends RealmObject {

    @PrimaryKey
    int Thread_Id;

    int Message_Id;
    int Recipient_Id;
    String TimeStamp;
    String Message_Body;
    boolean Is_Read;
    boolean Is_Sent;

    @Ignore
    boolean Is_Error;
    @Ignore
    boolean IsSending;

    public MessageThread() {
    }

    public MessageThread(int thread_Id, int message_Id, int recipient_Id, String timeStamp, String message_Body, boolean is_Read, boolean is_Sent, boolean isSending, boolean isError) {
        Thread_Id = thread_Id;
        Message_Id = message_Id;
        Recipient_Id = recipient_Id;
        TimeStamp = timeStamp;
        Message_Body = message_Body;
        Is_Read = is_Read;
        Is_Sent = is_Sent;
        IsSending = isSending;
        Is_Error = isError;
    }
}
