package com.forest_indo.youmodel.Model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koucluck on 8/25/2016.
 */
@Getter
@Setter
public class Field extends RealmObject {

    @PrimaryKey
    int Field_Id;
    
    @SerializedName("Name")
    String Field_Name;

    public Field() {
    }

    public Field(int field_Id, String field_Name) {
        Field_Id = field_Id;
        Field_Name = field_Name;
    }

    public static class UserFieldRequest {
        String Field_Name;
        int Field_Id;
        int User_id;

        public UserFieldRequest(String field_Name, int field_Id, int user_id) {
            Field_Name = field_Name;
            Field_Id = field_Id;
            User_id = user_id;
        }
    }
}
