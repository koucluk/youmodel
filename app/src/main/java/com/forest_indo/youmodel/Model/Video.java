package com.forest_indo.youmodel.Model;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koucluck on 8/25/2016.
 */

@Getter
@Setter
public class Video extends RealmObject {

    @PrimaryKey
    int Video_Id;
    String Video_url;
    String Thumb_url;
    int User_Id;

    boolean Published;

    @Ignore
    boolean Error;
    @Ignore
    boolean Uploading;
    @Ignore
    boolean Success;

    public Video() {
    }

    public Video(int video_Id, String video_Url, String thumb_url, int user_Id, boolean published, boolean error, boolean uploading, boolean success) {
        Video_Id = video_Id;
        Video_url = video_Url;
        Thumb_url = thumb_url;
        User_Id = user_Id;
        Published = published;
        Error = error;
        Uploading = uploading;
        Success = success;
    }
}
