package com.forest_indo.youmodel.Model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koucluck on 11/14/2016.
 */

@Getter
@Setter
public class BrowseModelResult extends RealmObject {

    @PrimaryKey
    String User_Id;

    @SerializedName("PREMIUM")
    boolean Premium;
    @SerializedName("ONLINE")
    boolean Online;

    String Photo_Url;
    String Name;
    String Name1;

    int Gender;
    String Dob;
    int Age;
    int Height;
    int Weight;
    String Lokasi;
    String Phone_Number;

    public BrowseModelResult() {

    }

    public BrowseModelResult(String USER_ID, boolean premium, boolean online, String photo_Url, String name, String name1,
                             int gender, String dob, int age, int height, int weight, String lokasi, String phone_Number) {
        this.User_Id = USER_ID;
        Premium = premium;
        Online = online;
        Photo_Url = photo_Url;
        Name = name;
        Name1 = name1;
        Gender = gender;
        Dob = dob;
        Age = age;
        Weight = weight;
        Lokasi = lokasi;
        Phone_Number = phone_Number;
    }
}
