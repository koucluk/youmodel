package com.forest_indo.youmodel.View.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.forest_indo.youmodel.Adapter.KategoriAdapter;
import com.forest_indo.youmodel.Adapter.SmallModelAgencyAdapter;
import com.forest_indo.youmodel.Adapter.SmallPhotoAgencyAdapter;
import com.forest_indo.youmodel.Model.BrowseAgencyResult;
import com.forest_indo.youmodel.Model.ProfileAgency;
import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.View.CustomButton;
import com.forest_indo.youmodel.Utils.View.CustomTextView;
import com.forest_indo.youmodel.Utils.View.CustomTextViewPhar;
import com.forest_indo.youmodel.View.MainActivity;
import com.github.ornolfr.ratingview.RatingView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Koucluck on 9/5/2016.
 */
public class AgencyDetailFragment extends Fragment {

    private static final String ISMYPROFILE = "ismyprofile";
    private static final String USER_ID = "userid";
    @BindView(R.id.rec_model_agency)
    RecyclerView recModel;
    @BindView(R.id.rec_photo_agency)
    RecyclerView recFoto;
    @BindView(R.id.rec_field_agency)
    RecyclerView recField;
    @BindView(R.id.rating_agency)
    RatingView ratingAgency;
    @BindView(R.id.btn_more_model_agency)
    CustomButton btnMoreModel;
    @BindView(R.id.btn_more_photo_agency)
    CustomButton btnMorePhoto;
    @BindView(R.id.btn_chat_agency)
    CustomButton btnChat;
    @BindView(R.id.btn_edit_agency)
    CustomButton btnEdit;
    @BindView(R.id.holder_edit_agency)
    RelativeLayout holderEditProfile;
    @BindView(R.id.holder_more_model_agency)
    RelativeLayout holderMoreModel;
    @BindView(R.id.holder_more_photo_agency)
    RelativeLayout holderMorePhoto;
    @BindView(R.id.holder_chat_agency)
    LinearLayout holderChat;
    @BindView(R.id.holder_button_model_agency)
    LinearLayout holderModelButton;
    @BindView(R.id.holder_button_photo_agency)
    LinearLayout holderPhotoButton;
    @BindView(R.id.txt_name_agency)
    CustomTextView txtName;
    @BindView(R.id.txt_lokasi_agency)
    CustomTextView txtLokasi;
    @BindView(R.id.txt_about_agency)
    CustomTextViewPhar txtAbout;
    @BindView(R.id.holder_vip_agency)
    RelativeLayout holderVipBadge;

    MainPresenter.MainInterface mainInterface;
    MainPresenter presenter;
    MainActivity activity;
    Unbinder unbinder;
    private int VAL_USER_ID;
    private boolean isMyProfile;
    private AlertDialog dialogRate;

    public static AgencyDetailFragment newInstance(boolean myProfile, int userid) {
        AgencyDetailFragment fragment = new AgencyDetailFragment();
        Bundle args = new Bundle();
        args.putBoolean(ISMYPROFILE, myProfile);
        args.putInt(USER_ID, userid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            isMyProfile = getArguments().getBoolean(ISMYPROFILE);
            VAL_USER_ID = getArguments().getInt(USER_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_agency, container, false);
        unbinder = ButterKnife.bind(this, view);
        mainInterface.turnBehavior(true);
        ratingAgency.setOnTouchListener((v, event) -> false);

        setupView();
        setupRecycler();

        return view;
    }

    private void setupView() {
        if (isMyProfile) {
            mainInterface.setTitle("PROFIL SAYA");
            holderChat.setVisibility(View.INVISIBLE);
            holderEditProfile.setVisibility(View.VISIBLE);

            holderModelButton.setVisibility(View.VISIBLE);
            holderMoreModel.setVisibility(View.INVISIBLE);

            holderPhotoButton.setVisibility(View.VISIBLE);
            holderMorePhoto.setVisibility(View.INVISIBLE);
            fillMyProfile();
        } else {
            mainInterface.setTitle("RINCIAN");
            holderEditProfile.setVisibility(View.GONE);
            holderChat.setVisibility(View.VISIBLE);

            holderModelButton.setVisibility(View.INVISIBLE);
            holderMoreModel.setVisibility(View.VISIBLE);

            holderPhotoButton.setVisibility(View.INVISIBLE);
            holderMorePhoto.setVisibility(View.VISIBLE);
            fillDetailProfile();
        }

        btnChat.setOnClickListener(v -> {
            Fragment fragment = ChatDetailFragment.newInstance(Integer.parseInt(presenter.getUser().getUSER_ID()),
                    VAL_USER_ID, 0, "");

            activity.ChangeFragment(fragment, new String[]{"chat_frag"}, true);
            activity.setTitle("OBROLAN");
        });

        btnEdit.setOnClickListener(v -> {
            Fragment fragment = AgencyEditProfileFragment.newInstance("", "");
            mainInterface.ChangeFragment(fragment, new String[]{"edit_agency"}, true);
            mainInterface.setTitle("EDIT PROFIL");
        });
    }

    private void fillDetailProfile() {
        ratingAgency.setRating(presenter.getRates(String.valueOf(VAL_USER_ID)));

        BrowseAgencyResult agency = presenter.getResultAgency(VAL_USER_ID);
        activity.showProgress(false);
        try {
            txtName.setText(agency.getName1().toUpperCase());
        } catch (NullPointerException e) {
            txtName.setText(agency.getName().toUpperCase());
        }

        txtAbout.setText(agency.getAbout());

        try {
            txtLokasi.setText(agency.getLokasi().toUpperCase());
        } catch (NullPointerException e) {
            txtLokasi.setText("");
        }

        recField.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recField.setAdapter(new KategoriAdapter(getActivity(), presenter.getUserField(VAL_USER_ID)));
    }

    private void fillMyProfile() {
        ratingAgency.setRating(presenter.getRates(String.valueOf(VAL_USER_ID)));

        ProfileAgency agency = presenter.getAgency();

        try {
            txtName.setText(agency.getName().toUpperCase());
        } catch (NullPointerException e) {
            txtName.setText(presenter.getUser().getUsername().toUpperCase());
        }

        try {
            txtAbout.setText(agency.getAbout());
        } catch (NullPointerException e) {
            txtAbout.setText("NONE");
        }

        try {
            txtLokasi.setText(agency.getLokasi().toUpperCase());
        } catch (NullPointerException e) {
            txtLokasi.setText("");
        }

        if (presenter.getUser().isPremium()) {
            holderVipBadge.setVisibility(View.VISIBLE);
        } else {
            holderVipBadge.setVisibility(View.GONE);
        }

        recField.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recField.setAdapter(new KategoriAdapter(getActivity(), presenter.getUserField()));
    }

    private void setupRecycler() {
        recModel.setLayoutManager(new GridLayoutManager(getActivity(), 5));
        recModel.setAdapter(new SmallModelAgencyAdapter());

        recFoto.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recFoto.setAdapter(new SmallPhotoAgencyAdapter(0, null));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MainActivity) context;
        mainInterface = (MainPresenter.MainInterface) context;
        presenter = mainInterface.getPresenter();
    }

    @OnClick(R.id.btn_more_model_agency)
    void moreModel() {
        Fragment fragment = MainEntryFragment.newInstance("model", false);
        //pass id agency {id}
        mainInterface.ChangeFragment(fragment, new String[]{"model_agency", "id"}, true);
        mainInterface.setTitle("DAFTAR MODEL");
    }

    @OnClick(R.id.btn_more_photo_agency)
    void morePhoto() {
        Fragment fragment = PhotoGalleryFragment.newInstance("agensi", "");
        //pass id agency
        mainInterface.ChangeFragment(fragment, new String[]{"model_agency", "id"}, true);
        mainInterface.setTitle("GALERI FOTO");
    }

    @OnClick(R.id.btn_rate_agency)
    void rateAgency() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.popup_rate, null);
        view.findViewById(R.id.btn_submit_rate).setOnClickListener(v -> {

            //presenter submit rating
            presenter.giveRates(VAL_USER_ID, ((RatingView) view.findViewById(R.id.rate_user)).getRating(), ratingAgency);
            dialogRate.dismiss();
        });

        builder.setView(view)
                .setCancelable(true);

        dialogRate = builder.create();
        dialogRate.show();
    }

}
