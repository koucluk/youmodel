package com.forest_indo.youmodel.View.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.forest_indo.youmodel.Adapter.ChatAdapter;
import com.forest_indo.youmodel.Model.MessageThread;
import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.Constant;
import com.forest_indo.youmodel.Utils.View.CircleTransformation;
import com.forest_indo.youmodel.Utils.View.CustomEditText;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChatDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatDetailFragment extends Fragment implements Constant {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String FROM = "from";
    private static final String DEST = "dest";
    private static final String MSG_ID = "msg_id";
    private static final String PICTURE = "pict";

    @BindView(R.id.rec_chat)
    RecyclerView recChat;

    @BindView(R.id.et_write_chat)
    CustomEditText etWrite;
    @BindView(R.id.btn_send_chat)
    ImageButton btnSendChat;
    @BindView(R.id.img_ava_chat)
    ImageView imgAva;

    MainPresenter presenter;
    MainPresenter.MainInterface mainInterface;
    // TODO: Rename and change types of parameters
    private int VAL_FROM;
    private int VAL_DEST;
    private int VAL_MSG_ID;
    private String VAL_PICTURE;


    public ChatDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param from    From User.
     * @param dest    Destination User.
     * @param msg_id  Message Id.
     * @param picture
     * @return A new instance of fragment ChatDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChatDetailFragment newInstance(int from, int dest, int msg_id, String picture) {
        ChatDetailFragment fragment = new ChatDetailFragment();
        Bundle args = new Bundle();
        args.putInt(FROM, from);
        args.putInt(DEST, dest);
        args.putInt(MSG_ID, msg_id);
        args.putString(PICTURE, picture);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            VAL_FROM = getArguments().getInt(FROM);
            VAL_DEST = getArguments().getInt(DEST);
            VAL_MSG_ID = getArguments().getInt(MSG_ID);
            VAL_PICTURE = getArguments().getString(PICTURE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frame_chat_detail, container, false);
        mainInterface.setTitle("OBROLAN");
        mainInterface.turnBehavior(false);
        ButterKnife.bind(this, view);

        setupView();

        return view;
    }

    private void setupView() {

        Picasso.with(getActivity())
                .load(PROFILEPICTURE_SERVER_URL + VAL_PICTURE)
                .transform(new CircleTransformation())
                .into(imgAva);

        if (VAL_MSG_ID > 0) {
            setupRec(presenter.getChat(VAL_MSG_ID, recChat));
        } else {
            setupRec(presenter.getChat(VAL_DEST, VAL_FROM));
            VAL_MSG_ID = presenter.getActiveChat();
        }
    }

    private void setupRec(List<MessageThread> messageThreads) {
        DefaultItemAnimator animator = new DefaultItemAnimator() {
            @Override
            public boolean canReuseUpdatedViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, @NonNull List<Object> payloads) {
                return true;
            }
        };

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setStackFromEnd(true);

        recChat.setLayoutManager(layoutManager);
        recChat.setAdapter(new ChatAdapter(messageThreads, Integer.parseInt(presenter.getUser().getUSER_ID())));
        recChat.setItemAnimator(animator);

        recChat.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            if (bottom < oldBottom) {
                if (recChat.getAdapter().getItemCount() > 0)
                    recChat.postDelayed(() -> recChat.smoothScrollToPosition(recChat.getAdapter().getItemCount() - 1), 100);

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainInterface = (MainPresenter.MainInterface) context;
        presenter = mainInterface.getPresenter();
    }

    @OnClick(R.id.btn_send_chat)
    public void sendChat() {
        if (!etWrite.getText().toString().equalsIgnoreCase("")) {
            String body = etWrite.getText().toString();
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");

            String time = simpleDateFormat.format(calendar.getTime());
            String timeStamp = time + "|" + calendar.get(Calendar.DAY_OF_WEEK)
                    + "-" + calendar.get(Calendar.MONTH) + "-" + calendar.get(Calendar.YEAR);

            MessageThread thread = new MessageThread(0, VAL_MSG_ID, VAL_DEST, timeStamp, body, false, false, true, false);

            etWrite.setText("");

            ((ChatAdapter) recChat.getAdapter()).addToList(thread);
            recChat.getLayoutManager().scrollToPosition(recChat.getAdapter().getItemCount() - 1);

            presenter.sendChat(VAL_MSG_ID, VAL_DEST, body, timeStamp, recChat);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.Destroy();
    }
}