package com.forest_indo.youmodel.View;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.forest_indo.youmodel.Model.ProfileAgency;
import com.forest_indo.youmodel.Model.ProfileModel;
import com.forest_indo.youmodel.Model.ProfileSeeker;
import com.forest_indo.youmodel.Model.User;
import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.APIUtils.APIRequest;
import com.forest_indo.youmodel.Utils.Constant;
import com.forest_indo.youmodel.Utils.View.CircleTransformation;
import com.forest_indo.youmodel.Utils.View.CustomButton;
import com.forest_indo.youmodel.Utils.View.CustomTextView;
import com.forest_indo.youmodel.Utils.View.CustomTextViewPhar;
import com.forest_indo.youmodel.View.Fragment.AboutFragment;
import com.forest_indo.youmodel.View.Fragment.AgencyDetailFragment;
import com.forest_indo.youmodel.View.Fragment.ContactFragment;
import com.forest_indo.youmodel.View.Fragment.GuestDetailFragment;
import com.forest_indo.youmodel.View.Fragment.MainFragment;
import com.forest_indo.youmodel.View.Fragment.ModelDetailFragment;
import com.forest_indo.youmodel.View.Fragment.NotificationFragment;
import com.forest_indo.youmodel.View.Fragment.PacketFragment;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.greenfrvr.rubberloader.RubberLoaderView;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmObject;

public class MainActivity extends AppCompatActivity implements BaseView, MainPresenter.MainInterface, Constant {

    MainPresenter presenter;

    LinearLayout holderFilter;
    ExpandableLinearLayout expandableLinearLayout;

    @BindView(R.id.main_frame)
    FrameLayout mainFrame;

    @BindView(R.id.first_action)
    ImageButton imgFirstAct;

    @BindView(R.id.second_action)
    ImageView imgSecondAct;

    @BindView(R.id.third_action)
    ImageView imgthirdAct;

    @BindView(R.id.second_action_replacement)
    CustomTextView imgSecondActRep;

    @BindView(R.id.nav_about_select)
    CustomTextView navAbout;
    @BindView(R.id.nav_contact_select)
    CustomTextView navContact;
    @BindView(R.id.txt_user_name_drawer)
    CustomTextView txtNameDrawer;
    @BindView(R.id.txt_user_type_drawer)
    CustomTextView txtUserTypeDrawer;
    @BindView(R.id.img_ava_profile)
    ImageView imgAvaDrawer;
    @BindView(R.id.holder_name_profile)
    LinearLayout holderName;

    @BindView(R.id.drawer_main)
    DrawerLayout drawerMain;

    @BindView(R.id.nav_home_select)
    RelativeLayout navHome;
    @BindView(R.id.nav_agency_select)
    RelativeLayout navAgency;
    @BindView(R.id.nav_model_select)
    RelativeLayout navModel;
    @BindView(R.id.nav_notif_select)
    RelativeLayout navNotif;
    @BindView(R.id.nav_vip_select)
    RelativeLayout navVip;
    @BindView(R.id.nav_logout_panel)
    RelativeLayout navLogout;
    @BindView(R.id.holder_badge_message)
    RelativeLayout holderBadgeMessage;

    @BindView(R.id.nav_header_panel)
    LinearLayout navProfile;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.holder_loader)
    FrameLayout holderLoader;
    @BindView(R.id.loader)
    RubberLoaderView dotLoader;

    @Inject
    APIRequest request;

    Unbinder binder;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        binder = ButterKnife.bind(this);
        presenter = new MainPresenter(this, Realm.getDefaultInstance());

        boolean isNewUser = getIntent().getBooleanExtra("NEW_USER", false);
        int User_Type = getIntent().getIntExtra("USER_TYPE", -1);
        String name = getIntent().getStringExtra("USER_NAME");
        String passwd = getIntent().getStringExtra("USER_PASSWD");
        String USER_ID = getIntent().getStringExtra("USER_ID");

        presenter.checkUser(isNewUser, User_Type, name, passwd, USER_ID);
    }

    public void setupViewWithUser(User user) {
        txtNameDrawer.setText(user.getUsername().toUpperCase());
    }

    public <T extends RealmObject> void setupViewWithProfile(T t, int type) {
        switch (type) {
            case 1:
                ProfileModel model = (ProfileModel) t;
                txtNameDrawer.setText(model.getName().toUpperCase());
                Picasso.with(getApplicationContext())
                        .load(PROFILEPICTURE_SERVER_URL + model.getPhoto_Url())
                        .transform(new CircleTransformation())
                        .error(R.drawable.asset_image_avatar_dafault_model)
                        .placeholder(R.drawable.asset_image_avatar_dafault_model)
                        .into(imgAvaDrawer);
                break;
            case 2:
                ProfileAgency agency = (ProfileAgency) t;
                Log.d("Name Agency", agency.getName());
                txtNameDrawer.setText(agency.getName().toUpperCase());
                break;
            case 3:
                ProfileSeeker seeker = (ProfileSeeker) t;
                txtNameDrawer.setText(seeker.getName().toUpperCase());
                break;
        }
    }

    public void showFullImage() {

        AlertDialog dialog;
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.frame_image_fullscreen, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                .setView(view)
                .setCancelable(true);

        ((ImageView) view.findViewById(R.id.img_photo)).setImageResource(R.drawable.asset_image_galerifoto_xb);

        dialog = builder.create();

        dialog.show();
    }

    @Override
    public void showOneButtonDialog(String message, String action) {
        if (alertDialog != null) {
            alertDialog = null;
        }
        View view = LayoutInflater.from(getApplicationContext())
                .inflate(R.layout.popup_one_button, null);
        ((CustomButton) view.findViewById(R.id.btn_action1_dialog)).setText(action);
        ((CustomTextViewPhar) view.findViewById(R.id.txt_dialog_message)).setText(message);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                .setCancelable(false)
                .setView(view);

        alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void showTwoButtonDialog(String message, String action1, String action2) {
        if (alertDialog != null) {
            alertDialog = null;
        }
        View view = LayoutInflater.from(getApplicationContext())
                .inflate(R.layout.popup_two_button, null);
        ((CustomButton) view.findViewById(R.id.btn_action1_dialog)).setText(action1);
        ((CustomButton) view.findViewById(R.id.btn_action2_dialog)).setText(action2);
        ((CustomTextViewPhar) view.findViewById(R.id.txt_dialog_message)).setText(message);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                .setCancelable(false)
                .setView(view);


        alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public AlertDialog getDialog() {
        return alertDialog;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            imgFirstAct.setImageResource(R.drawable.main_icon_navmenu);
            imgSecondAct.setVisibility(View.VISIBLE);
            imgSecondActRep.setVisibility(View.GONE);
        } else if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            imgSecondActRep.setText("AGENSI");
        }
    }

    @Override
    public MainPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void ChangeFragment(Fragment fragment, String[] args, boolean poppedBackStack) {
        hideKeyboard();
        if (poppedBackStack) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(null)
                    .replace(mainFrame.getId(), fragment, args[0])
                    .commit();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(mainFrame.getId(), fragment, args[0])
                    .commit();
        }
    }

    @Override
    public void setupDrawer(boolean isGuest) {
        if (isGuest) {
            navNotif.setVisibility(View.GONE);
            navVip.setVisibility(View.GONE);
        }

        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawerMain, 0, 0) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerMain.setDrawerListener(drawerToggle);

        navHome.setOnClickListener(v -> {
            Fragment fragment = MainFragment.newInstance("", "");
            ChangeFragment(fragment, new String[]{"main"}, false);
            setTitle("home");
            drawerMain.closeDrawer(GravityCompat.START);
        });

        navAgency.setOnClickListener(v -> {
            presenter.applyAgencyFilterDefault();
            setTitle("AGENSI");
            drawerMain.closeDrawer(GravityCompat.START);
        });

        navModel.setOnClickListener(v -> {
            presenter.applyModelFilterDefault();
            setTitle("MODEL");
            drawerMain.closeDrawer(GravityCompat.START);
        });

        navNotif.setOnClickListener(v -> {
            Fragment fragment = NotificationFragment.newInstance("", "");
            ChangeFragment(fragment, new String[]{"notif_frame"}, true);
            setTitle("NOTIFIKASI");
            drawerMain.closeDrawer(GravityCompat.START);
        });

        navAbout.setOnClickListener(v -> {
            Fragment fragment = AboutFragment.newInstance("", "");
            ChangeFragment(fragment, new String[]{"about_frame"}, true);
            setTitle("TENTANG KAMI");
            drawerMain.closeDrawer(GravityCompat.START);
        });

        navContact.setOnClickListener(v -> {
            Fragment fragment = ContactFragment.newInstance("", "");
            ChangeFragment(fragment, new String[]{"contact_frame"}, true);
            setTitle("HUBUNGI KAMI");
            drawerMain.closeDrawer(GravityCompat.START);
        });

        navLogout.setOnClickListener(v -> presenter.doLogout());

        navProfile.setOnClickListener(v -> {
            gotoProfile();
            drawerMain.closeDrawer(GravityCompat.START);
        });

        navVip.setOnClickListener(v -> {
            Fragment fragment = PacketFragment.newInstance("", "");
            ChangeFragment(fragment, new String[]{"packet_frame"}, true);
            setTitle("LANGGANAN");
            drawerMain.closeDrawer(GravityCompat.START);
        });

        imgAvaDrawer.setOnClickListener(v -> {
            gotoProfile();
            drawerMain.closeDrawer(GravityCompat.START);
        });

        holderName.setOnClickListener(v -> {
            gotoProfile();
            drawerMain.closeDrawer(GravityCompat.START);
        });
    }

    /*@Override
    public void updateDrawer(String filename) {
        Picasso.with(getApplicationContext())
                .load(PROFILEPICTURE_SERVER_URL + filename)
                .transform(new CircleTransformation())
                .error(R.drawable.asset_image_avatar_dafault_model)
                .placeholder(R.drawable.asset_image_avatar_dafault_model)
                .into(imgAvaDrawer);

    }*/

    private void gotoProfile() {
        Fragment fragment = null;

        switch (presenter.getUser().getUser_Type()) {
            case 1:
                fragment = ModelDetailFragment.newInstance(true, 0);
                break;
            case 2:
                fragment = AgencyDetailFragment.newInstance(true, 0);
                break;
            case 3:
                fragment = GuestDetailFragment.newInstance(true, "");
                break;
        }

        ChangeFragment(fragment, new String[]{"myprofile"}, true);
    }

    @Override
    public void setTitle(String title) {
        if (title.equals("home")) {
            imgSecondAct.setVisibility(View.VISIBLE);
            imgSecondActRep.setVisibility(View.GONE);
            imgFirstAct.setVisibility(View.VISIBLE);
            imgFirstAct.setImageResource(R.drawable.main_icon_navmenu);
            holderBadgeMessage.setVisibility(View.VISIBLE);
        } else if (title.equalsIgnoreCase("OBROLAN1")) {
            imgSecondAct.setVisibility(View.GONE);
            imgSecondActRep.setVisibility(View.VISIBLE);
            imgSecondActRep.setText(title.substring(0, title.indexOf('1')));
            imgFirstAct.setImageResource(R.drawable.main_icon_arrow_left_white);
            holderBadgeMessage.setVisibility(View.GONE);
        } else if (title.equalsIgnoreCase("newUser")) {
            imgSecondAct.setVisibility(View.GONE);
            imgSecondActRep.setVisibility(View.VISIBLE);
            imgSecondActRep.setText("USER BARU");
            imgFirstAct.setVisibility(View.INVISIBLE);
            holderBadgeMessage.setVisibility(View.VISIBLE);
        } else {
            imgSecondAct.setVisibility(View.GONE);
            imgSecondActRep.setVisibility(View.VISIBLE);
            imgSecondActRep.setText(title);
            imgFirstAct.setImageResource(R.drawable.main_icon_arrow_left_white);
            holderBadgeMessage.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void turnBehavior(boolean useFlag) {
        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();

        if (useFlag) {
            params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
                    | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
        } else {
            params.setScrollFlags(0);
        }
    }

    @Override
    public void refreshUser() {
        switch (presenter.getUser().getUser_Type()) {
            case 1:
                txtUserTypeDrawer.setText("MODEL");
                try {
                    if (!presenter.getModel().getName().equals("")) {
                        setupViewWithProfile(presenter.getModel(), 1);
                    } else {
                        throw new NullPointerException();
                    }
                } catch (NullPointerException e) {
                    Log.d("NAME MODEL", "MODELss NAME NULL");
                    setupViewWithUser(presenter.getUser());
                }
                break;
            case 2:
                txtUserTypeDrawer.setText("AGENSI");
                try {
                    if (!presenter.getAgency().getName().equals("")) {
                        setupViewWithProfile(presenter.getAgency(), 2);
                    } else {
                        throw new NullPointerException();
                    }
                } catch (NullPointerException e) {
                    Log.d("NAME AGENCY", "AGENCY NAME NULL");
                    setupViewWithUser(presenter.getUser());
                }
                break;
            case 3:
                txtUserTypeDrawer.setText("GUEST");
                try {
                    setupViewWithProfile(presenter.getSeeker(), 3);
                } catch (NullPointerException e) {
                    setupViewWithUser(presenter.getUser());
                }
                break;
        }
    }

    @Override
    public void setUserData(User user) {
        
    }

    @Override
    public void showProgress(boolean flag) {
        if (flag) {
            holderLoader.setVisibility(View.VISIBLE);
            holderLoader.setClickable(true);
            holderLoader.setOnClickListener(v -> {
                return;
            });

            dotLoader.startLoading();
        } else {
            holderLoader.setVisibility(View.GONE);
            holderLoader.setClickable(false);
            holderLoader.setOnClickListener(null);
            dotLoader.stopLoading();
        }
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getApplicationContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        View v = this.getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @OnClick(R.id.first_action)
    void drawerClick() {
        Fragment mainFragment = getSupportFragmentManager().findFragmentByTag("main");
        if (mainFragment.isVisible()) {
            drawerMain.openDrawer(Gravity.LEFT);
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    @OnClick(R.id.holder_badge_message)
    void gotoMessage() {
        presenter.gotoMessage();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.Destroy();
        binder.unbind();
    }
}
