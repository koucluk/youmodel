package com.forest_indo.youmodel.View;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.forest_indo.youmodel.Presenter.RegisterPresenter;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.View.CustomEditText;
import com.forest_indo.youmodel.Utils.View.CustomTextViewPhar;
import com.forest_indo.youmodel.View.Dialog.DialogRegister;
import com.greenfrvr.rubberloader.RubberLoaderView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Koucluck on 8/31/2016.
 */
public class RegisterActivity extends AppCompatActivity implements BaseView, RegisterPresenter.RegisterInterface {

    ImageButton btnBack;
    RegisterPresenter presenter;
    Unbinder unbinder;
    AlertDialog dialog = null;
    @BindView(R.id.txt_passwd)
    CustomEditText etPasswd;
    @BindView(R.id.txt_nama)
    CustomEditText etName;
    @BindView(R.id.txt_email)
    CustomEditText etEmail;
    @BindView(R.id.holder_loader)
    FrameLayout loader;
    @BindView(R.id.loader)
    RubberLoaderView dotLoader;
    @BindView(R.id.txt_login_error)
    CustomTextViewPhar txtError;

    private boolean IS_PASSWD_SHOWED;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_register);

        unbinder = ButterKnife.bind(this);
        presenter = new RegisterPresenter(this, this);
        showDialogRegister();
    }

    @OnClick(R.id.btn_register)
    public void goRegister() {
        presenter.registerUser(etEmail.getText().toString(), etName.getText().toString(), etPasswd.getText().toString());
    }

    @OnClick(R.id.btn_back_register)
    public void goBack() {
        finish();
    }

    @OnClick(R.id.btn_show_pass_register)
    public void showPass() {
        presenter.showPass(etPasswd, IS_PASSWD_SHOWED);
    }

    private void showDialogRegister() {
        DialogRegister dialogRegister = new DialogRegister();
        dialogRegister.setCancelable(false);
        dialogRegister.show(getSupportFragmentManager(), "dialog_register");
    }

    @Override
    public void dialogChoosen(int user_type) {
        presenter.setUserType(user_type);
    }

    @Override
    public void showDialog(final String email, final String passwd) {
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.popup_one_button, null);

        view.findViewById(R.id.btn_action1_dialog).setOnClickListener(v -> {
            dialog.dismiss();

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra("NEW_USER", true);
            intent.putExtra("USER_TYPE", presenter.getUserType());
            intent.putExtra("USER_NAME", email);
            intent.putExtra("USER_PASSWD", passwd);
            startActivity(intent);
            finish();
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this)
                .setCancelable(false)
                .setView(view);

        dialog = builder.create();
        dialog.show();
    }

    @Override
    public void setStatusPassword(boolean status) {
        IS_PASSWD_SHOWED = status;
    }

    @Override
    public void showError(String msg) {
        Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.error_anim);
        Animation animDisolve = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.disolve_anim);

        Runnable runnable = () -> {
            txtError.startAnimation(animDisolve);
            txtError.setVisibility(View.GONE);
        };

        Handler handler = new Handler();
        handler.removeCallbacks(runnable);

        txtError.setVisibility(View.VISIBLE);
        txtError.setText(msg);
        txtError.startAnimation(anim);

        handler.postDelayed(runnable, 5000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void showProgress(boolean status) {
        if (status) {
            loader.setVisibility(View.VISIBLE);
            dotLoader.startLoading();
        } else {
            loader.setVisibility(View.GONE);
            dotLoader.stopLoading();
        }
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getApplicationContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        View v = this.getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @Override
    public void showOneButtonDialog(String message, String action) {

    }

    @Override
    public void showTwoButtonDialog(String message, String action1, String action2) {

    }

    @Override
    public AlertDialog getDialog() {
        return null;
    }
}