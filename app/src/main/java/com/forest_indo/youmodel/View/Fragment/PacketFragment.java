package com.forest_indo.youmodel.View.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.forest_indo.youmodel.Adapter.PacketAdapter;
import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PacketFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PacketFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    MainPresenter.MainInterface mainInterface;
    MainPresenter mainPresenter;
    @BindView(R.id.rec_packet)
    RecyclerView recPacket;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public PacketFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PacketFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PacketFragment newInstance(String param1, String param2) {
        PacketFragment fragment = new PacketFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_packet, container, false);
        ButterKnife.bind(this, view);
        mainInterface.setTitle("LANGGANAN");
        mainInterface.turnBehavior(false);

        setupRec();
        return view;
    }

    private void setupRec() {
        recPacket.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recPacket.setAdapter(new PacketAdapter());
//        ((PacketAdapter) recPacket.getAdapter()).
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainInterface = (MainPresenter.MainInterface) context;
        mainPresenter = mainInterface.getPresenter();
    }
}
