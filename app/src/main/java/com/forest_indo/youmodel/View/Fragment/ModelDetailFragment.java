package com.forest_indo.youmodel.View.Fragment;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.forest_indo.youmodel.Adapter.KategoriAdapter;
import com.forest_indo.youmodel.Adapter.PreviewVideoAdapter;
import com.forest_indo.youmodel.Adapter.SmallPhotoAgencyAdapter;
import com.forest_indo.youmodel.Model.BrowseModelResult;
import com.forest_indo.youmodel.Model.ProfileModel;
import com.forest_indo.youmodel.Model.Video;
import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.Constant;
import com.forest_indo.youmodel.Utils.Videos.controls.TimelineItem;
import com.forest_indo.youmodel.Utils.View.CircleTransformation;
import com.forest_indo.youmodel.Utils.View.CustomButton;
import com.forest_indo.youmodel.Utils.View.CustomTextView;
import com.forest_indo.youmodel.Utils.View.CustomTextViewPhar;
import com.forest_indo.youmodel.View.MainActivity;
import com.github.ornolfr.ratingview.RatingView;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ModelDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ModelDetailFragment extends Fragment implements Constant {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ISMYPROFILE = "ismyprofile";
    private static final String USER_ID = "user_id";

    @BindView(R.id.holder_vip_badge)
    RelativeLayout holderVip;

    @BindView(R.id.rec_photo_model)
    RecyclerView recPhoto;
    @BindView(R.id.rec_video_model)
    RecyclerView recVideo;
    @BindView(R.id.rec_field_model)
    RecyclerView recField;

    @BindView(R.id.txt_name_model)
    CustomTextView txtName;
    @BindView(R.id.txt_lokasi_model)
    CustomTextView txtLokasi;
    @BindView(R.id.txt_about_model)
    CustomTextViewPhar txtAbout;
    @BindView(R.id.txt_photo_empty_model)
    CustomTextView txtEmptyPhoto;

    @BindView(R.id.btn_more_photo_model)
    CustomButton btnMorePhoto;
    @BindView(R.id.btn_more_video_model)
    CustomButton btnMoreVideo;

    @BindView(R.id.rating_model)
    RatingView ratingModel;

    @BindView(R.id.holder_edit_model)
    RelativeLayout holderEditProfile;
    @BindView(R.id.holder_more_photo_model)
    RelativeLayout holderMorePhoto;
    @BindView(R.id.holder_more_video_model)
    RelativeLayout holderMoreVideo;

    @BindView(R.id.holder_button_video_model)
    LinearLayout holderVideoButton;
    @BindView(R.id.holder_chat_model)
    LinearLayout holderChat;
    @BindView(R.id.holder_button_photo_model)
    LinearLayout holderPhotoButton;

    @BindView(R.id.btn_upload_photo_model)
    CustomButton btnUploadPhoto;
    @BindView(R.id.btn_all_photo_model)
    CustomButton btnAllPhoto;
    @BindView(R.id.btn_chat_model)
    CustomButton btnChat;
    @BindView(R.id.btn_edit_model)
    CustomButton btnEdit;
    @BindView(R.id.btn_rate_model)
    CustomButton btnRate;
    @BindView(R.id.btn_rekrut_model)
    CustomButton btnRekrut;

    @BindView(R.id.img_profile_model)
    ImageView imgProfil;

    MainPresenter.MainInterface mainInterface;
    MainPresenter presenter;
    TimelineItem timelineItem;
    // TODO: Rename and change types of parameters
    private int VAL_USER_ID;
    private int VAL_USER_TYPE;
    private boolean isMyProfile;
    private Unbinder binder;
    private MainActivity activity;
    private AlertDialog dialogRate;

    public ModelDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param myProfile Parameter 1.
     * @param userid    Parameter 2.
     * @return A new instance of fragment ModelDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ModelDetailFragment newInstance(boolean myProfile, int userid) {
        ModelDetailFragment fragment = new ModelDetailFragment();
        Bundle args = new Bundle();
        args.putBoolean(ISMYPROFILE, myProfile);
        args.putInt(USER_ID, userid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isMyProfile = getArguments().getBoolean(ISMYPROFILE);
            VAL_USER_ID = getArguments().getInt(USER_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_model, container, false);
        binder = ButterKnife.bind(this, view);

        timelineItem = new TimelineItem(getActivity());
        
        mainInterface.turnBehavior(true);

        ratingModel.setOnTouchListener((v, event) -> false);

        setupView();
        return view;
    }

    private void setupView() {
        if (isMyProfile) {
            mainInterface.setTitle("PROFIL SAYA");
            btnRekrut.setVisibility(View.GONE);
            holderChat.setVisibility(View.INVISIBLE);
            holderEditProfile.setVisibility(View.VISIBLE);

            holderMorePhoto.setVisibility(View.INVISIBLE);
            holderPhotoButton.setVisibility(View.VISIBLE);

            holderMoreVideo.setVisibility(View.INVISIBLE);
            holderVideoButton.setVisibility(View.VISIBLE);

            fillMyProfile();
        } else {
            mainInterface.setTitle("RINCIAN");
            holderChat.setVisibility(View.VISIBLE);
            holderEditProfile.setVisibility(View.GONE);

            holderMorePhoto.setVisibility(View.VISIBLE);
            holderPhotoButton.setVisibility(View.INVISIBLE);

            holderMoreVideo.setVisibility(View.VISIBLE);
            holderVideoButton.setVisibility(View.INVISIBLE);

            if (presenter.getUser().getUser_Type() == 2) {
                btnRekrut.setVisibility(View.VISIBLE);
                btnRekrut.setText("REKRUT (" + presenter.getUser().getLimit_Left() + ")");
            } else {
                btnRekrut.setVisibility(View.GONE);
            }
            fillDetailProfile();
        }

        btnChat.setOnClickListener(v -> {
            Fragment fragment = ChatDetailFragment.newInstance(Integer.parseInt(presenter.getUser().getUSER_ID()),
                    VAL_USER_ID, 0, "");

            activity.ChangeFragment(fragment, new String[]{"chat_frag"}, true);
            activity.setTitle("OBROLAN");
        });

        btnEdit.setOnClickListener(v -> {
            Fragment fragment = ModelEditProfileFragment.newInstance("", "");
            activity.ChangeFragment(fragment, new String[]{"edit_model"}, true);
        });

        btnMorePhoto.setOnClickListener(v -> {
            Fragment fragment = PhotoGalleryFragment.newInstance("", "");
            activity.ChangeFragment(fragment, new String[]{"photo_model"}, true);
            activity.setTitle("GALERI FOTO");
        });

        btnRekrut.setOnClickListener(v -> {
            presenter.doRecruit(VAL_USER_ID);
        });

        btnRate.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            View view = LayoutInflater.from(getActivity()).inflate(R.layout.popup_rate, null);
            view.findViewById(R.id.btn_submit_rate).setOnClickListener(vi -> {

                dialogRate.dismiss();
                presenter.giveRates(VAL_USER_ID, ((RatingView) view.findViewById(R.id.rate_user)).getRating(), ratingModel);
            });

            builder.setView(view)
                    .setCancelable(true);

            dialogRate = builder.create();
            dialogRate.show();
        });
    }

    private void fillDetailProfile() {
        activity.showProgress(true);
        ratingModel.setRating(presenter.getRates(String.valueOf(VAL_USER_ID)));

        BrowseModelResult model = presenter.getResultModel(VAL_USER_ID);
        activity.showProgress(false);
        try {
            txtName.setText(model.getName1().toUpperCase());
        } catch (NullPointerException e) {
            txtName.setText(model.getName().toUpperCase());
        }

        String gender = model.getGender() > 0 ?
                (model.getGender() == 1 ? "Laki-laki\n" : "Perempuan\n") : "";
        String dob = model.getDob().equals("") ? "" : model.getDob() + "\n";

        String appearance = model.getWeight() + " kg/" + model.getHeight() + " cm\n";
        String phone = model.getPhone_Number() + "\n";

        String about = gender + dob + appearance + phone;

        txtAbout.setText(about);

        try {
            txtLokasi.setText(model.getLokasi().toUpperCase());
        } catch (NullPointerException e) {
            txtLokasi.setText("");
        }

        recField.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recField.setAdapter(new KategoriAdapter(getActivity(), presenter.getUserField(VAL_USER_ID)));
    }

    private void fillMyProfile() {
        ratingModel.setRating(presenter.getRates(String.valueOf(VAL_USER_ID)));

        ProfileModel model = presenter.getModel();

        if (presenter.getUser().isPremium()) {
            holderVip.setVisibility(View.VISIBLE);
        } else {
            holderVip.setVisibility(View.GONE);
        }

        String photoPath = getActivity().getFilesDir() + PROFILEPICTURE_LOCAL_PATH + model.getPhoto_Url();

        Log.d("PHOTO PATH LOCAL", photoPath);
        File photoProfile = new File(photoPath);

        if (photoProfile.exists()) {
            Picasso.with(getActivity())
                    .load(photoProfile)
                    .transform(new CircleTransformation())
                    .error(R.drawable.asset_image_avatar_dafault_model)
                    .placeholder(R.drawable.asset_image_avatar_dafault_model)
                    .into(imgProfil);
        } else {
            Picasso.with(getActivity())
                    .load(PROFILEPICTURE_SERVER_URL + model.getPhoto_Url())
                    .transform(new CircleTransformation())
                    .error(R.drawable.asset_image_avatar_dafault_model)
                    .placeholder(R.drawable.asset_image_avatar_dafault_model)
                    .into(imgProfil);
        }

        try {
            if (!model.getName().equals(""))
                txtName.setText(model.getName().toUpperCase());
            else {
                txtName.setText(presenter.getUser().getUsername().toUpperCase());
            }
        } catch (NullPointerException e) {
            txtName.setText(presenter.getUser().getUsername().toUpperCase());
        }

        try {
            txtLokasi.setText(model.getLokasi().toUpperCase());
        } catch (NullPointerException e) {
            txtLokasi.setText("");
        }

        String gender = model.getGender() > 0 ?
                (model.getGender() == 1 ? "Laki-laki\n" : "Perempuan\n") : "";

        String dob = model.getDob() == null ? "" : model.getDob() + "\n";

        String appearance = model.getWeight() + " kg/" + model.getHeight() + " cm\n";
        String phone = model.getPhone_Number() == null ? "" : model.getPhone_Number() + "\n";

        String about = gender + dob + appearance + phone;

        txtAbout.setText(about);

        recField.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recField.setAdapter(new KategoriAdapter(getActivity(), presenter.getUserField()));

        recPhoto.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recVideo.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        presenter.getModelPhotos(Integer.parseInt(presenter.getUser().getUSER_ID()), recPhoto, txtEmptyPhoto);
        presenter.getModelVideos(Integer.parseInt(presenter.getUser().getUSER_ID()), recVideo, txtEmptyPhoto);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        activity = (MainActivity) context;
        mainInterface = (MainPresenter.MainInterface) context;
        presenter = mainInterface.getPresenter();
    }

    @OnClick(R.id.btn_upload_photo_model)
    public void uploadPhoto() {
        final CharSequence[] items = {"Dari Kamera", "Ambil Gambar dari Galeri",
                "Batal"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Unggah Foto!");
        builder.setItems(items, (dialog, item) -> {

            if (items[item].equals("Dari Kamera")) {
                cameraIntent(1);
            } else if (items[item].equals("Ambil Gambar dari Galeri")) {
                galleryIntent(1);
            } else if (items[item].equals("Batal")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @OnClick(R.id.btn_all_photo_model)
    public void seeAllPhotos() {

    }

    @OnClick(R.id.btn_upload_video_model)
    public void uploadVideo() {
        final CharSequence[] items = {"Dari Kamera", "Ambil Video dari Galeri",
                "Batal"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Unggah Video!");
        builder.setItems(items, (dialog, item) -> {

            if (items[item].equals("Dari Kamera")) {
                cameraIntent(2);
            } else if (items[item].equals("Ambil Video dari Galeri")) {
                galleryIntent(2);
            } else if (items[item].equals("Batal")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @OnClick(R.id.btn_all_video_model)
    public void seeAllVideos() {

    }

    private void galleryIntent(int type) {
        if (type == 1) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Pilih Foto"), REQUEST_GALERY_PHOTO);
        } else {
            Log.d("PICK VIDEO", "RUN");

            Intent intent = new Intent();
            intent.setType("video/*");
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);//
            startActivityForResult(Intent.createChooser(intent, "Pilih Video"), REQUEST_GALERY_VIDEO);
        }
    }

    private void cameraIntent(int type) {
        if (type == 1) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA_PHOTO);
        } else {
            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA_VIDEO);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_GALERY_PHOTO)
                onSelectPhotoFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA_PHOTO)
                onCapturePhotoImageResult(data);
            else if (requestCode == REQUEST_GALERY_VIDEO)
                onSelectVideoFromGalleryResult(data);
            else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                processAndUploadPhoto(result.getUri());
            }
        }
    }

    private void onCapturePhotoImageResult(Intent data) {
        if (data != null) {
            try {
                CropImage.activity(data.getData())
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .start(getContext(), this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void processAndUploadVideo(String data) {

        File file = new File(data);

        String ex = file.getName();

        File newFile = new File(getActivity().getCacheDir() + "/" +
                "ym_videos_1_" + presenter.getUser().getUSER_ID() + "_"
                + (((PreviewVideoAdapter) recVideo.getAdapter()).getVideoCount() + 1)
                + ex.substring(ex.lastIndexOf('.')));

        File newFileThumb = new File(getActivity().getCacheDir(),
                "ym_thumb_1_" + presenter.getUser().getUSER_ID() + "_"
                        + (((PreviewVideoAdapter) recVideo.getAdapter()).getVideoCount() + 1)
                        + ".png");

        try {
            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(data, MediaStore.Images.Thumbnails.MINI_KIND);

            imgProfil.setImageBitmap(thumb);
            OutputStream outputStream = new FileOutputStream(newFileThumb);
            thumb.compress(Bitmap.CompressFormat.PNG, 50, outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d("THUMB PATH", newFileThumb.getName());

        FileChannel inChannel = null;
        FileChannel outChannel = null;

        try {
            inChannel = new FileInputStream(file).getChannel();
            outChannel = new FileOutputStream(newFile).getChannel();

            inChannel.transferTo(0, inChannel.size(), outChannel);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inChannel != null) {
                    inChannel.close();
                }

                if (outChannel != null) {
                    outChannel.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        file.delete();

        File[] files = new File[]{newFile, newFileThumb};

        if (recVideo.getAdapter().getItemCount() > 0) {
            Video video = new Video(0, newFile.getName(), newFileThumb.getName(), Integer.parseInt(presenter.getUser().getUSER_ID()), true, false, true, false);
            ((PreviewVideoAdapter) recVideo.getAdapter()).updateItem(video);
        } else {
            txtEmptyPhoto.setVisibility(View.GONE);
            recVideo.setVisibility(View.VISIBLE);
            List<Video> videos = new ArrayList<>();
            Video video = new Video(0, newFile.getName(), newFileThumb.getName(), Integer.parseInt(presenter.getUser().getUSER_ID()), true, false, true, false);
            videos.add(video);
            ((PreviewVideoAdapter) recVideo.getAdapter()).addNewItem(videos);
        }

        presenter.uploadVideo(files, presenter.getUser().getUSER_ID(), recVideo);
    }

    private void processAndUploadPhoto(Uri uri) {
        File file = new File(uri.getPath());
        String ex = file.getName();

        File newFile = new File(getActivity().getCacheDir() + "/" +
                "ym_photos_1_" + presenter.getUser().getUSER_ID() + "_"
                + (((SmallPhotoAgencyAdapter) recPhoto.getAdapter()).getPhotosCount() + 1)
                + ex.substring(ex.lastIndexOf('.')));

        FileChannel inChannel = null;
        FileChannel outChannel = null;

        try {
            inChannel = new FileInputStream(file).getChannel();
            outChannel = new FileOutputStream(newFile).getChannel();

            inChannel.transferTo(0, inChannel.size(), outChannel);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inChannel != null)
                    inChannel.close();

                if (outChannel != null)
                    outChannel.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        file.delete();

        Log.d("ABS FILE PATH", newFile.getAbsolutePath());
        try {
            Log.d("CANON FILE PATH", newFile.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        presenter.uploadPhoto(newFile, presenter.getUser().getUSER_ID(), recPhoto, txtEmptyPhoto);
    }

    private void onSelectPhotoFromGalleryResult(Intent data) {
        if (data != null) {
            try {
                CropImage.activity(data.getData())
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .start(getContext(), this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void onSelectVideoFromGalleryResult(Intent data) {
        if (data != null) {
            String path = generatePath(data.getData(), getActivity());
            String workFolder = getActivity().getFilesDir().getAbsolutePath() + "/";

//            presenter.compressVideo(path, workFolder);
//            processAndUploadVideo(generatePath(data.getData(), getActivity()));
        }
    }

    public String generatePath(Uri uri, Context context) {
        String filePath = null;
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        if (isKitKat) {
            filePath = generateFromKitkat(uri, context);
        }

        if (filePath != null) {
            return filePath;
        }

        Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.Video.Media._ID,
                MediaStore.MediaColumns.DATA}, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }
        return filePath == null ? uri.getPath() : filePath;
    }

    private String generateFromKitkat(Uri uri, Context context) {
        String filePath = null;

        if (DocumentsContract.isDocumentUri(context, uri)) {
            String wholeID = DocumentsContract.getDocumentId(uri);

            String id = wholeID.split(":")[1];

            String[] column = {MediaStore.Video.Media.DATA};
            String sel = MediaStore.Video.Media._ID + "=?";

            Cursor cursor = context.getContentResolver().
                    query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                            column, sel, new String[]{id}, null);

            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }

            Log.d("PATH", filePath);

            cursor.close();
        }
        return filePath;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (binder != null) {
            binder.unbind();
            binder = null;
        }

        presenter.Destroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (binder != null) {
            binder.unbind();
            binder = null;
        }

        presenter.Destroy();
    }
}