package com.forest_indo.youmodel.View.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.forest_indo.youmodel.Model.ProfileSeeker;
import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GuestDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GuestDetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    MainPresenter.MainInterface mainInterface;
    MainPresenter mainPresenter;
    @BindView(R.id.holder_blokir)
    LinearLayout holderBlokir;
    // TODO: Rename and change types of parameters
    private boolean isMyProfile;
    private String mParam2;


    public GuestDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param myprofile Parameter 1.
     * @param param2    Parameter 2.
     * @return A new instance of fragment GuestDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GuestDetailFragment newInstance(boolean myprofile, String param2) {
        GuestDetailFragment fragment = new GuestDetailFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, myprofile);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isMyProfile = getArguments().getBoolean(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_guest, container, false);
        ButterKnife.bind(this, view);

        mainInterface.setTitle("RINCIAN");

        setupView(mainPresenter.getSeeker());

        return view;
    }

    private void setupView(ProfileSeeker seeker) {
        if (isMyProfile) {
            holderBlokir.setVisibility(View.GONE);
        } else {
            holderBlokir.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainInterface = (MainPresenter.MainInterface) context;
        mainPresenter = mainInterface.getPresenter();
    }
}
