package com.forest_indo.youmodel.View.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.forest_indo.youmodel.Model.ProfileAgency;
import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.View.CustomEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AgencyEditProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AgencyEditProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    MainPresenter mainPresenter;
    MainPresenter.MainInterface mainInterface;

    @BindView(R.id.et_name_agency)
    CustomEditText etName;

    @BindView(R.id.et_about_agency)
    CustomEditText etAbout;

    @BindView(R.id.et_website_agency)
    CustomEditText etWebsite;

    @BindView(R.id.et_addr_agency)
    CustomEditText etAddr;

    @BindView(R.id.rec_kategori_agency)
    RecyclerView recKategori;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public AgencyEditProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AgencyEditProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AgencyEditProfileFragment newInstance(String param1, String param2) {
        AgencyEditProfileFragment fragment = new AgencyEditProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frame_agency_edit_profile, container, false);
        mainInterface.setTitle("EDIT PROFILE");

        ButterKnife.bind(this, view);

        setViews(mainPresenter.getAgency());
        recKategori.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        mainPresenter.setupKategoriUpdatable(recKategori);
        return view;
    }

    @OnClick(R.id.btn_konfirm)
    public void submitData() {
        mainPresenter.submitProfileData(etName.getText().toString(), "SURABAYA", "", 0, "", 0, 0, 0, "",
                etAbout.getText().toString(), 0, "", etWebsite.getText().toString(), recKategori);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mainInterface = (MainPresenter.MainInterface) context;
        mainPresenter = mainInterface.getPresenter();
    }

    public void setViews(ProfileAgency views) {
        
    }
}