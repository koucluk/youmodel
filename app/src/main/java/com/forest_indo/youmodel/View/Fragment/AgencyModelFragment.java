package com.forest_indo.youmodel.View.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.View.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AgencyModelFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AgencyModelFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.rec_model_agency)
    RecyclerView recModel;
    Unbinder binder;
    MainPresenter presenter;
    MainPresenter.MainInterface mainInterface;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public AgencyModelFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AgencyModelFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AgencyModelFragment newInstance(String param1, String param2) {
        AgencyModelFragment fragment = new AgencyModelFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        presenter = new MainPresenter((MainActivity) getActivity(), Realm.getDefaultInstance());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frame_agency_model, container, false);

        binder = ButterKnife.bind(this, view);
        mainInterface.setTitle("RINCIAN");
        mainInterface.turnBehavior(true);

        presenter.setupRecModel(recModel);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (binder != null) {
            binder.unbind();
            binder = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (binder != null) {
            binder.unbind();
            binder = null;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainInterface = (MainPresenter.MainInterface) context;
    }
}
