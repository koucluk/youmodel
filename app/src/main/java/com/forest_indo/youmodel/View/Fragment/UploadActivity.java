package com.forest_indo.youmodel.View.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.forest_indo.youmodel.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.Header;

public class UploadActivity extends AppCompatActivity {

    private static final int TAKE_PHOTO_CODE = 9090;
    private AsyncHttpClient client;
    private TextView status;
    private Context ctx;

    private static final String TAG = "UploadingStuff";
    private String mosUrl = "http://210.5.41.103/AppProfilePic/ProfilePic.aspx";
    private String dir = Environment.getExternalStorageDirectory() + "/TestUpload/";
    private String filePath = dir+"testimg.jpg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        ctx = this;

        status = (TextView) findViewById(R.id.status_tv);

        client = new AsyncHttpClient();

        findViewById(R.id.test_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doCameraToDevice();
                //testFileRequestParamUpload();
                //doPostUpload();
                //doLoopjPostUpload();
                //convertToByteArrayUpload();
                //new convertToByteTask().execute();
            }
        });
    }

    private void convertToByteArrayUpload() {
        File file = new File(filePath);

        byte[] b = new byte[(int) file.length()];
        String bytePrint = "";
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            fileInputStream.read(b);
            Log.d(TAG, "Length: "+b.length);
            /*for (int i = 0; i < b.length; i++) {
                //System.out.print((char)b[i]);
                bytePrint = bytePrint + (char) b[i];
                Log.d(TAG, "Byte: "+b[i]);
            }*/

            Log.d(TAG, "BytePrint: " + bytePrint);
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found.");
            e.printStackTrace();
        }
        catch (IOException e1) {
            System.out.println("Error Reading The File.");
            e1.printStackTrace();
        }
    }

    private class convertToByteTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            Log.d(TAG, "Conversion Started");
            convertToByteArrayUpload();
            return null;
        }
    }

    private void doLoopjPostUpload() {
        try {
            final String dir = Environment.getExternalStorageDirectory() + "/TestUpload/";
            String fileName = dir+"testimg.jpg";
            File imgFile = new File(fileName);

            RequestParams params = new RequestParams();
            params.put("image", imgFile);
            client.post(mosUrl, params, new TextHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    Log.d(TAG, "Upload Started");
                }

                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);
                    Log.d(TAG, "Progress: "+bytesWritten+" "+totalSize);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d(TAG, "Failed: "+statusCode
                            +responseString
                            +throwable.toString());
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Log.d(TAG, "Success: "+statusCode
                            +responseString);
                }
            });
        } catch (Exception e) {
            Log.d("PostUpload", "Exception", e);
        }
    }

    private void doPostUpload() {
        new PostUploadTask().execute();
    }

    private void testFileRequestParamUpload() {
        HttpURLConnection connection = null;
        DataOutputStream outputStream = null;
        DataInputStream inputStream = null;
        final String dir = Environment.getExternalStorageDirectory() + "/TestUpload/";
        /*File newdir = new File(dir);
        newdir.mkdirs();*/
        String pathToOurFile = dir+"testimg.jpg";
        //String pathToOurFile = "/data/file_to_send.mp3";
        String urlServer = "http://210.5.41.103/AppProfilePic/ProfilePic.aspx";
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary =  "*****";

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1*1024*1024;

        try
        {
            FileInputStream fileInputStream = new FileInputStream(new File(pathToOurFile) );

            URL url = new URL(urlServer);
            connection = (HttpURLConnection) url.openConnection();

            // Allow Inputs &amp; Outputs.
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            // Set HTTP method to POST.
            connection.setRequestMethod("POST");

            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);

            outputStream = new DataOutputStream( connection.getOutputStream() );
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
            outputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + pathToOurFile +"\"" + lineEnd);
            outputStream.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // Read file
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0)
            {
                outputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // Responses from the server (code and message)
            int serverResponseCode = connection.getResponseCode();
            String serverResponseMessage = connection.getResponseMessage();

            fileInputStream.close();
            outputStream.flush();
            outputStream.close();

            Log.d("PostUpload", "ResponseCode: "+serverResponseCode
                    +"ResponseMessage: "+serverResponseMessage);
        }
        catch (Exception ex)
        {
            //Exception handling
            Log.d("PostUpload", "Exception: ", ex);
        }
    }

    private class PostUploadTask extends AsyncTask <Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            testFileRequestParamUpload();
            return null;
        }
    }

    private void testUpload() {
        Toast.makeText(UploadActivity.this, "Saved to Device", Toast.LENGTH_SHORT).show();

        try {
            final String dir = Environment.getExternalStorageDirectory() + "/TestUpload/";
            File newdir = new File(dir);
            newdir.mkdirs();
            String fileName = dir+"testimg.jpg";
            File imgFile = new File(fileName);

            byte[] bFile = new byte[(int) imgFile.length()];

            FileInputStream fileInputStream = new FileInputStream(imgFile);
            fileInputStream.read(bFile);
            fileInputStream.close();

            String encodedBytes = Base64.encodeToString(bFile, Base64.NO_WRAP);
            //Log.d("UploadTest", "Base64: "+encodedBytes);

            //String mosUrl = "http://210.5.41.103/AppProfilePic/ProfilePic.aspx";
            String url = "http://210.5.41.103/AppProfilePic/ProfilePic.aspx?img="+encodedBytes+"&uid=391&action=null&name=testimg.jpg";
            //Log.d("UploadTest", "URL: \n"+mosUrl);
            saveStringToFile(url, "mosUrl.txt");

            /*RequestParams requestParams = new RequestParams();
            requestParams.put("img", encodedBytes);
            requestParams.put("uid", "391");
            requestParams.put("action", "");
            requestParams.put("name", "testimg.jpg");*/

            client.post(url, new TextHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    Log.d("UploadTest", "Started");
                    //status.setText("Started");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("UploadTest", "Failed: "+responseString);
                    Toast.makeText(UploadActivity.this, "Failed:"+responseString, Toast.LENGTH_SHORT).show();
                    status.setText("Failed: \n"+ Html.fromHtml(responseString)
                            +"\n\nStatusCode: "+statusCode);
                    saveStringToFile(responseString, "Response.txt");
                    /*try {
                        final String dir = Environment.getExternalStorageDirectory() + "/TestUpload/";
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter
                                (openFileOutput(dir+"error.txt", Context.MODE_PRIVATE));
                        outputStreamWriter.write(responseString);
                        outputStreamWriter.close();
                    }
                    catch (IOException e) {
                        Log.e("UploadTest", "File write failed: " + e.toString());
                    }*/
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Log.d("UploadTest", "Success:\n "+responseString+"\nStatusCode: "+statusCode);
                    Toast.makeText(UploadActivity.this, "Successful: "+responseString, Toast.LENGTH_SHORT).show();
                    status.setText("Success: \n"+responseString);
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    Log.d("UploadTest", "Finished");
                }
            });

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.d("UploadTest", "FileNotFoundException: "+e.toString());
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("UploadTest", "IOException: "+e.toString());
        }
    }

    /*private void saveStringToFile(String toWrite, String fileName) {
        try {
            final String dir = Environment.getExternalStorageDirectory() + "/TestUpload/";
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter
                    (ctx.openFileOutput(fileName, Context.MODE_PRIVATE));
            outputStreamWriter.write(toWrite);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("UploadTest", "File write failed: " + e.toString());
        }
    }*/

    private void saveStringToFile(String toWrite, String fileName) {
        WriteToDisk writeTask = new WriteToDisk();
        writeTask.fname = fileName;
        writeTask.data = toWrite;
        writeTask.execute();
    }

    private class WriteToDisk extends AsyncTask<Void, Void, Void> {
        private String fname;
        private String data;

        private void save(String toWrite, String fileName){
            Writer writer = null;
            try {
                final File configDir = new File(Environment.getExternalStorageDirectory()+"/TestUpload", "config");
                configDir.mkdir();
                writer = new OutputStreamWriter(new FileOutputStream(new File(configDir, fileName)));
                writer.write(toWrite);
            } catch (IOException e) {
                e.printStackTrace();
                Log.d("UploadTest", "IOException: "+e.toString());
            } finally {
                if (writer != null)
                    try {
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.d("UploadTest", "IOException: "+e.toString());
                    }
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("UploadTest", "Started: "+fname);
        }

        @Override
        protected Void doInBackground(Void... params) {
            save(data, fname);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d("UploadTest", "Finished: "+fname);
        }
    }

    public void doCameraToDevice() {
        final String dir = Environment.getExternalStorageDirectory() + "/TestUpload/";
        File newdir = new File(dir);
        newdir.mkdirs();
        String file = dir+"testimg.jpg";
        File newfile = new File(file);
        try {
            newfile.createNewFile();
        }
        catch (IOException e)
        {
            Log.d("BMP", "IOException"+e.toString());
        }

        Uri outputFileUri = Uri.fromFile(newfile);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

        startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
    }

    private void fettyUpload() {
//        String fettyUrl = "http://u.lawaweb.com/UploadToServer.php";

//        path=testimg.jpg&User=138&Action=Add

        /*File myFile = new File(filePath);
        RequestParams params = new RequestParams();
        try {
            params.put("uploaded_file", myFile);
        } catch(FileNotFoundException e) {
            Log.d(TAG, "File Not Found", e);
        }*/

        String fettyUrl = "http://210.5.41.103/Profilepicture/Profilepic.aspx?";

        RequestParams params = new RequestParams();
        try {
            String sourceFileUri = filePath;
            File sourceFile = new File(sourceFileUri);
            FileInputStream myInputStream = new FileInputStream(sourceFile);
            params.put("path", myInputStream, "testimg.jpg");
            params.put("User", 138);
            params.put("Action", "Add");
        } catch (Exception e) {
            Log.d(TAG, "Exception: ", e);
        }

        client.post(fettyUrl, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d(TAG, "Failed: "+statusCode+"\n"+responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.d(TAG, "Success: "+statusCode+"\n"+responseString);
            }
        });
    }

    private void fettyTutorialUpload() {
        /*FettyTask uploadTask = new FettyTask();
        uploadTask.execute();*/
        new FettyTask().execute();
    }

    private class FettyTask extends AsyncTask<Void, Void, Void> {
        private void doFettyUpload() {
            String upLoadServerUri = "http://u.lawaweb.com/UploadToServer.php";
            String sourceFileUri = filePath;
            String fileName = sourceFileUri;
            File sourceFile = new File(sourceFileUri);

            HttpURLConnection conn = null;
            DataOutputStream dos = null;
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;

            try {
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(upLoadServerUri);

                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", fileName);

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=uploaded_file;filename=\""
                        + fileName + "\"" + lineEnd);

                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {

                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                int serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();

                Log.i(TAG, "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);
            } catch (Exception e) {
                Log.d(TAG, "Error", e);
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            doFettyUpload();
            return null;
        }
    }

    private void byte64Upload() {
        try {
            final String dir = Environment.getExternalStorageDirectory() + "/TestUpload/";
            File newdir = new File(dir);
            newdir.mkdirs();
            String fileName = dir + "testimg.jpg";
            File imgFile = new File(fileName);

            byte[] bFile = new byte[(int) imgFile.length()];

            FileInputStream fileInputStream = new FileInputStream(imgFile);
            fileInputStream.read(bFile);
            fileInputStream.close();

            String encodedBytes = Base64.encodeToString(bFile, Base64.NO_WRAP);
            Log.d(TAG, "String Length: "+encodedBytes.length());
            String byteUrl = "http://210.5.41.103/AppProfilePic/ProfilePic.aspx?img="+encodedBytes+"&uid=391&action=null&name=testimg.jpg";
            Log.d("UploadTest", "URL: \n"+byteUrl);

            /*client.get(byteUrl, new TextHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    Log.d(TAG, "Started");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d(TAG, "Fail: "+statusCode+"\nResponse: "+responseString);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Log.d(TAG, "Success: "+statusCode+"\nResponse: "+responseString);
                }
            });*/
        } catch (IOException e) {
            Log.d(TAG, "Error Encoding", e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TAKE_PHOTO_CODE) {
            if (resultCode == RESULT_OK) {
                //testUpload();
                //byte64Upload();
                fettyUpload();
                //fettyTutorialUpload();
                Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(UploadActivity.this, "Save Failed", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
