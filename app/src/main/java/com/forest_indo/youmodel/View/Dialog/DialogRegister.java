package com.forest_indo.youmodel.View.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.forest_indo.youmodel.Presenter.RegisterPresenter;
import com.forest_indo.youmodel.R;

/**
 * Created by Koucluck on 8/31/2016.
 */
public class DialogRegister extends DialogFragment {

    RegisterPresenter.RegisterInterface registerInterface;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(STYLE_NO_TITLE, R.style.MyCustomTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyCustomTheme);

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.popup_register, null);

        view.findViewById(R.id.btn_reg_model).setOnClickListener(view1 -> {
            registerInterface.dialogChoosen(1);
            dismiss();
        });

        view.findViewById(R.id.btn_reg_agency).setOnClickListener(view12 -> {
            registerInterface.dialogChoosen(2);
            dismiss();
        });

        view.findViewById(R.id.btn_reg_guest).setOnClickListener(view13 -> {
            registerInterface.dialogChoosen(3);
            dismiss();
        });

        builder.setCancelable(false);
        builder.setView(view);

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        registerInterface = (RegisterPresenter.RegisterInterface) context;
    }
}
