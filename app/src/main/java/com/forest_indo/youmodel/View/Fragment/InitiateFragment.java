package com.forest_indo.youmodel.View.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.View.CustomEditText;
import com.forest_indo.youmodel.Utils.View.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link InitiateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InitiateFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String TYPE = "param1";
    private static final String EMAIL = "param2";
    private static final String PASSWD = "param3";

    @BindView(R.id.et_name_initiate)
    CustomEditText etName;
    @BindView(R.id.radio_gender_female_initiate)
    ImageView radioFemale;
    @BindView(R.id.radio_gender_male_initiate)
    ImageView radioMale;
    @BindView(R.id.rec_kategori_initiate)
    RecyclerView recKategori;
    @BindView(R.id.txt_radio_male_initiate)
    CustomTextView txtMaleRadio;
    @BindView(R.id.txt_radio_female_initiate)
    CustomTextView txtFemaleRadio;
    @BindView(R.id.txt_label_gender_initiate)
    CustomTextView labelGender;
    @BindView(R.id.txt_label_category_intiate)
    CustomTextView labelCategory;
    @BindView(R.id.holder_radio_gender_initiate)
    LinearLayout holderRadio;
    @BindView(R.id.txt_skip_initiate)
    CustomTextView txtInitiate;

    MainPresenter presenter;
    MainPresenter.MainInterface mainInterface;

    // TODO: Rename and change types of parameters
    boolean femaleAct = false;
    int UserType = -1;
    String Email = "";
    String Passwd = "";
    int Gender = -1;
    boolean maleAct = false;

    public InitiateFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param type   User Type.
     * @param name   Email
     * @param passwd Password .  @return A new instance of fragment InitiateFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InitiateFragment newInstance(int type, String name, String passwd) {
        InitiateFragment fragment = new InitiateFragment();
        Bundle args = new Bundle();
        args.putInt(TYPE, type);
        args.putString(EMAIL, name);
        args.putString(PASSWD, passwd);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            UserType = getArguments().getInt(TYPE);
            Email = getArguments().getString(EMAIL);
            Passwd = getArguments().getString(PASSWD);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_initiate, container, false);
        mainInterface.setTitle("newUser");

        ButterKnife.bind(this, view);

        CheckUser();
        setupRec();
        return view;
    }

    private void CheckUser() {
        switch (UserType) {
            case 2:
                labelGender.setVisibility(View.GONE);
                holderRadio.setVisibility(View.GONE);
                break;
            case 3:
                labelGender.setVisibility(View.GONE);
                holderRadio.setVisibility(View.GONE);

                labelCategory.setVisibility(View.GONE);
                recKategori.setVisibility(View.GONE);
                break;
        }

        presenter.setupInitiation(Email, Passwd);
    }

    @OnClick(R.id.btn_konfirm_initiate)
    public void saveInitiate() {
        presenter.submitData(UserType, etName.getText().toString(), Gender, recKategori);
    }

    @OnClick(R.id.radio_gender_male_initiate)
    public void toggleMale() {
        ChangeGenderState(radioMale, radioFemale, 1);
    }

    @OnClick(R.id.radio_gender_female_initiate)
    public void toggleFemale() {
        ChangeGenderState(radioFemale, radioMale, 2);
    }

    @OnClick(R.id.txt_radio_male_initiate)
    public void txtToggleMale() {
        ChangeGenderState(radioMale, radioFemale, 1);
    }

    @OnClick(R.id.txt_radio_female_initiate)
    public void txtToggleFeale() {
        ChangeGenderState(radioFemale, radioMale, 2);
    }

    private void ChangeGenderState(ImageView imgActive, ImageView imgInactive, int gender) {
        imgActive.setImageResource(R.drawable.asset_radio_button_selected);
        imgInactive.setImageResource(R.drawable.asset_radio_button_deselect);
        Gender = gender;
    }

    private void setupRec() {
        recKategori.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        presenter.setupKategori(recKategori);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainInterface = (MainPresenter.MainInterface) context;
        presenter = mainInterface.getPresenter();
    }

    @OnClick(R.id.txt_skip_initiate)
    public void skipInit() {
        presenter.skipInit();
    }
}
