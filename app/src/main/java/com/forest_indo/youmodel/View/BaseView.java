package com.forest_indo.youmodel.View;


import android.support.v7.app.AlertDialog;

/**
 * Created by Koucluck on 10/19/2016.
 */

public interface BaseView {
    void showProgress(boolean status);

    void hideKeyboard();

    void showOneButtonDialog(String message, String action);

    void showTwoButtonDialog(String message, String action1, String action2);

    AlertDialog getDialog();
}
