package com.forest_indo.youmodel.View.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.View.CustomEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GuestEditProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GuestEditProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @BindView(R.id.et_hp_edit_seeker)
    CustomEditText etNumber;
    @BindView(R.id.et_name_edit_seeker)
    CustomEditText etNama;

    MainPresenter.MainInterface mainInterface;

    MainPresenter presenter;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public GuestEditProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GuestEditProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GuestEditProfileFragment newInstance(String param1, String param2) {
        GuestEditProfileFragment fragment = new GuestEditProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frame_guest_edit_profile, container, false);
        mainInterface.setTitle("EDIT PROFILE");

        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.btn_konfirm_edit_model)
    public void submitData() {
        presenter.submitProfileData(etNama.getText().toString(), "", etNumber.getText().toString()
                , 0, "", 0, 0, 0, "", "", 0, "", "", null);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainInterface = (MainPresenter.MainInterface) context;
        presenter = mainInterface.getPresenter();
    }
}
