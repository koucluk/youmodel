package com.forest_indo.youmodel.View.Fragment;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.forest_indo.youmodel.Adapter.UserEntryAdapter;
import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;
import com.github.aakira.expandablelayout.ExpandableLayoutListener;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.github.aakira.expandablelayout.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link MainEntryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainEntryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String USER_TYPE = "param1";
    private static final String USE_TOOLBAR = "param2";


    MainPresenter.MainInterface mainInterface;
    MainPresenter presenter;

    @BindView(R.id.btn_filter)
    FrameLayout btnFilter;
    @BindView(R.id.btn_filter_rep)
    FrameLayout holderFilterRep;
    @BindView(R.id.rec_content_user)
    RecyclerView recContent;
    @BindView(R.id.holder_pause_sort)
    RelativeLayout relHolder;
    @BindView(R.id.holder_filter)
    LinearLayout holderFilter;
    @BindView(R.id.btn_sorting)
    RelativeLayout btnSorting;
    @BindView(R.id.expand_sorting)
    ExpandableLinearLayout expandSorting;
    @BindView(R.id.btn_toggle_expandable)
    ImageButton toggleButton;
    @BindView(R.id.empty_state)
    RelativeLayout holderError;

    Unbinder binder;
    private String mParam1;
    private String mParam2;
    private boolean useToolbar;

    public MainEntryFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static MainEntryFragment newInstance(String param1, boolean param2) {
        MainEntryFragment fragment = new MainEntryFragment();
        Bundle args = new Bundle();
        args.putString(USER_TYPE, param1);
        args.putBoolean(USE_TOOLBAR, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(USER_TYPE);
            useToolbar = getArguments().getBoolean(USE_TOOLBAR);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.frame_main_entry, container, false);

        binder = ButterKnife.bind(this, view);

        mainInterface.turnBehavior(true);

        relHolder.setVisibility(View.GONE);

        recContent.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        relHolder.setOnClickListener(view1 -> {
            expandSorting.collapse();
            relHolder.setVisibility(View.GONE);
        });


        if (mParam1.equals("agency")) {
            mainInterface.setTitle("AGENSI");
            if (useToolbar) {
                holderFilter.setVisibility(View.GONE);
                holderFilterRep.setVisibility(View.VISIBLE);

                holderFilterRep.setOnClickListener(view1 -> {
                    Fragment fragment = AgencyFilterFragment.newInstance(mParam1, "");
                    mainInterface.ChangeFragment(fragment, new String[]{"filter_agency"}, true);
                    mainInterface.setTitle("SARING");

                });
            } else {
                holderFilter.setVisibility(View.GONE);
            }

            Log.d("AGENCY SIZE", presenter.getCurrentAgencyResult().size() + "");
            if (presenter.getCurrentAgencyResult().size() > 0) {
                holderError.setVisibility(View.GONE);
                recContent.setVisibility(View.VISIBLE);
                recContent.setAdapter(new UserEntryAdapter(2, presenter.getCurrentAgencyResult()));
            } else {
                holderError.setVisibility(View.VISIBLE);
                recContent.setVisibility(View.GONE);
            }
        } else {
            mainInterface.setTitle("MODEL");
            if (useToolbar) {
                holderFilter.setVisibility(View.VISIBLE);
                holderFilterRep.setVisibility(View.GONE);

                btnFilter.setOnClickListener(view12 -> {
                    Fragment fragment = ModelFilterFragment.newInstance(mParam1, "");
                    mainInterface.ChangeFragment(fragment, new String[]{"filter_model"}, true);
                    mainInterface.setTitle("SARING");
                });
            } else {
                holderFilter.setVisibility(View.GONE);
            }

            if (presenter.getCurrentModelResult().size() > 0) {
                holderError.setVisibility(View.GONE);
                recContent.setVisibility(View.VISIBLE);
                recContent.setAdapter(new UserEntryAdapter(1, presenter.getCurrentModelResult()));
            } else {
                holderError.setVisibility(View.VISIBLE);
                recContent.setVisibility(View.GONE);
            }
        }

        btnSorting.setOnClickListener(view13 -> {
            if (expandSorting.isExpanded()) {
                relHolder.setVisibility(View.GONE);
                expandSorting.collapse();
            } else {
                relHolder.setVisibility(View.VISIBLE);
                expandSorting.expand();
            }
        });

        expandSorting.setListener(new ExpandableLayoutListener() {
            @Override
            public void onAnimationStart() {

            }

            @Override
            public void onAnimationEnd() {

            }

            @Override
            public void onPreOpen() {
                createRotateAnimator(toggleButton, 0f, 180f).start();
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(toggleButton, 180f, 0f).start();
            }

            @Override
            public void onOpened() {

            }

            @Override
            public void onClosed() {

            }
        });

        return view;
    }

    private ObjectAnimator createRotateAnimator(ImageButton target, float from, float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR));
        return animator;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainInterface = (MainPresenter.MainInterface) context;
        presenter = mainInterface.getPresenter();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (binder != null) {
            binder.unbind();
            binder = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (binder != null) {
            binder.unbind();
            binder = null;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (binder != null) {
            binder.unbind();
            binder = null;
        }
    }
}
