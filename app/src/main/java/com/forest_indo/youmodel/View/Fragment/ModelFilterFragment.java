package com.forest_indo.youmodel.View.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListPopupWindow;
import android.widget.RelativeLayout;

import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.View.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.apptik.widget.MultiSlider;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ModelFilterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ModelFilterFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    Unbinder unbinder;

    @BindView(R.id.slider_usia)
    MultiSlider sliderUsia;

    @BindView(R.id.slider_tinggi)
    MultiSlider sliderTinggi;

    @BindView(R.id.slider_berat)
    MultiSlider sliderBerat;

    @BindView(R.id.txt_usia_min)
    CustomTextView txtUsiaMin;

    @BindView(R.id.txt_usia_max)
    CustomTextView txtUsiaMax;

    @BindView(R.id.txt_min_berat)
    CustomTextView txtBeratMin;

    @BindView(R.id.txt_max_berat)
    CustomTextView txtBeratMax;

    @BindView(R.id.txt_min_tinggi)
    CustomTextView txtTinggiMin;

    @BindView(R.id.txt_max_tinggi)
    CustomTextView txtTinggiMax;

    @BindView(R.id.select_female_filter_model)
    RelativeLayout selectFemale;
    @BindView(R.id.select_male_filter_model)
    RelativeLayout selectMale;

    @BindView(R.id.img_male_filter_model)
    ImageButton imgMale;
    @BindView(R.id.img_female_filter_model)
    ImageButton imgFemale;

    @BindView(R.id.txt_male_filter_model)
    CustomTextView txtMale;
    @BindView(R.id.txt_female_filter_model)
    CustomTextView txtFemale;

    @BindView(R.id.rec_kategori)
    RecyclerView recKategori;

    @BindView(R.id.btn_lokasi_filter_model)
    RelativeLayout btnLokasi;
    @BindView(R.id.txt_lokasi_value)
    CustomTextView txtLokasi;

    MainPresenter.MainInterface mainInterface;
    MainPresenter presenter;
    boolean female = false;
    boolean male = false;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private String Gender;
    private String Lokasi = "S";
    private int Weight1 = 40;
    private int Weight2 = 100;
    private int Usia1 = 18;
    private int Usia2 = 40;
    private int Height1 = 150;
    private int Height2 = 200;

    public ModelFilterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ModelFilterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ModelFilterFragment newInstance(String param1, String param2) {
        ModelFilterFragment fragment = new ModelFilterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frame_model_filter, container, false);
        mainInterface.setTitle("SARING");
        unbinder = ButterKnife.bind(this, view);

        sliderUsia.setOnThumbValueChangeListener((multiSlider, thumb, thumbIndex, value) -> {
            if (thumbIndex == 0) {
                txtUsiaMin.setText(String.valueOf(value) + " TH");
                Usia1 = value;
            } else {
                txtUsiaMax.setText(String.valueOf(value) + " TH");
                Usia2 = value;
            }
        });

        sliderTinggi.setOnThumbValueChangeListener((multiSlider, thumb, thumbIndex, value) -> {
            if (thumbIndex == 0) {
                txtTinggiMin.setText(String.valueOf(value) + " CM");
                Height1 = value;
            } else {
                txtTinggiMax.setText(String.valueOf(value) + " CM");
                Height2 = value;
            }
        });

        sliderBerat.setOnThumbValueChangeListener((multiSlider, thumb, thumbIndex, value) -> {
            if (thumbIndex == 0) {
                txtBeratMin.setText(String.valueOf(value) + " KG");
                Weight1 = value;
            } else {
                txtBeratMax.setText(String.valueOf(value) + " KG");
                Weight2 = value;
            }
        });

        setupRec();

        return view;
    }

    private void setupRec() {
        recKategori.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        presenter.setupKategori(recKategori);
    }

    @OnClick(R.id.img_female_filter_model)
    public void changeFemaleState() {
        if (checkFemale()) {
            imgFemale.setImageResource(R.drawable.main_icon_sex_inactive);
            txtFemale.setTextColor(getActivity().getResources().getColor(R.color.base_pink));
        } else {
            imgFemale.setImageResource(R.drawable.main_icon_sex_active);
            txtFemale.setTextColor(getActivity().getResources().getColor(android.R.color.white));
        }

        female = !female;
    }

    private boolean checkFemale() {
        return female;
    }

    @OnClick(R.id.img_male_filter_model)
    public void changeMaleState() {
        if (checkMale()) {
            imgMale.setImageResource(R.drawable.main_icon_sex_inactive);
            txtMale.setTextColor(getActivity().getResources().getColor(R.color.base_pink));
        } else {
            txtMale.setTextColor(getActivity().getResources().getColor(android.R.color.white));
            imgMale.setImageResource(R.drawable.main_icon_sex_active);
        }

        male = !male;
    }

    @OnClick(R.id.btn_lokasi_filter_model)
    public void showPopupLokasi() {
        ListPopupWindow popupWindow = new ListPopupWindow(getActivity());

        ListAdapter adapter = new ArrayAdapter<>(getActivity(), R.layout.item_location, new String[]{"Surabaya", "Jakarta"});
        popupWindow.setAdapter(adapter);
        popupWindow.setAnchorView(btnLokasi);
        popupWindow.setOnItemClickListener((parent, view, position, id) -> {
            CustomTextView v = (CustomTextView) view;
            Lokasi = v.getText().toString();

            txtLokasi.setText(Lokasi);
            popupWindow.dismiss();
        });
        popupWindow.show();
    }

    @OnClick(R.id.btn_apply_filter_model)
    public void applyFilter() {
        if (checkFemale() && checkFemale()) {
            Gender = "(1,2)";
        } else if (checkFemale()) {
            Gender = "(2)";
        } else if (checkMale()) {
            Gender = "(1)";
        } else {
            Gender = "(1,2)";
        }

        presenter.applyModelFilter(Gender, Usia1, Usia2, Height1, Height2, Weight1, Weight2, Lokasi, recKategori);
    }

    private boolean checkMale() {
        return male;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainInterface = (MainPresenter.MainInterface) context;
        presenter = mainInterface.getPresenter();
    }
}
