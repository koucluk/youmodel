package com.forest_indo.youmodel.View.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.View.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.apptik.widget.MultiSlider;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MainFilterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFilterFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    MainPresenter presenter;
    MainPresenter.MainInterface mainInterface;
    Unbinder unbinder;

    @BindView(R.id.slider_usia)
    MultiSlider sliderUsia;

    @BindView(R.id.slider_tinggi)
    MultiSlider sliderTinggi;

    @BindView(R.id.slider_berat)
    MultiSlider sliderBerat;

    @BindView(R.id.txt_usia_min)
    CustomTextView txtUsiaMin;

    @BindView(R.id.txt_usia_max)
    CustomTextView txtUsiaMax;

    @BindView(R.id.txt_min_berat)
    CustomTextView txtBeratMin;

    @BindView(R.id.txt_max_berat)
    CustomTextView txtBeratMax;

    @BindView(R.id.txt_min_tinggi)
    CustomTextView txtTinggiMin;

    @BindView(R.id.txt_max_tinggi)
    CustomTextView txtTinggiMax;

    @BindView(R.id.rec_kategori)
    RecyclerView recKategori;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private String Gender;
    private String Lokasi;
    private int Weight1 = 40;
    private int Weight2 = 100;
    private int Usia1 = 18;
    private int Usia2 = 40;
    private int Height1 = 150;
    private int Height2 = 200;

    public MainFilterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MainFilterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MainFilterFragment newInstance(String param1, String param2) {
        MainFilterFragment fragment = new MainFilterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frame_main_filter, container, false);

        unbinder = ButterKnife.bind(this, view);

        sliderUsia.setOnThumbValueChangeListener((multiSlider, thumb, thumbIndex, value) -> {
            if (thumbIndex == 0) {
                txtUsiaMin.setText(String.valueOf(value) + " TH");
                Usia1 = value;
            } else {
                txtUsiaMax.setText(String.valueOf(value) + " TH");
                Usia2 = value;
            }
        });

        sliderTinggi.setOnThumbValueChangeListener((multiSlider, thumb, thumbIndex, value) -> {
            if (thumbIndex == 0) {
                txtTinggiMin.setText(String.valueOf(value) + " CM");
                Height1 = value;
            } else {
                txtTinggiMax.setText(String.valueOf(value) + " CM");
                Height2 = value;
            }
        });

        sliderBerat.setOnThumbValueChangeListener((multiSlider, thumb, thumbIndex, value) -> {
            if (thumbIndex == 0) {
                txtBeratMin.setText(String.valueOf(value) + " KG");
                Weight1 = value;
            } else {
                txtBeratMax.setText(String.valueOf(value) + " KG");
                Weight2 = value;
            }
        });

        recKategori.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        presenter.setupKategori(recKategori);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainInterface = (MainPresenter.MainInterface) context;
        presenter = mainInterface.getPresenter();
    }

    @OnClick(R.id.btn_apply_filter_model)
    public void applyFilter() {
        presenter.applyModelFilter(Gender, Usia1, Usia2, Height1, Height2, Weight1, Weight2, Lokasi, recKategori);
    }
}
