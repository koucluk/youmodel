package com.forest_indo.youmodel.View;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.View.CustomButton;
import com.forest_indo.youmodel.Utils.View.CustomEditText;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by FIN_MULTIMEDIA on 27/09/2016.
 */

public class InputNewPasswdActivity extends AppCompatActivity {

    @BindView(R.id.et_input_passwd)
    CustomEditText etInput;

    @BindView(R.id.btn_submit_input_passwd)
    CustomButton btnSubmit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_input_password);

        ButterKnife.bind(this);
    }
}
