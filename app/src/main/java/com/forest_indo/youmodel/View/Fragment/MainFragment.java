package com.forest_indo.youmodel.View.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    MainPresenter presenter;
    MainPresenter.MainInterface mainInterface;
    @BindView(R.id.holder_agency_main)
    ImageView imgAgency;
    @BindView(R.id.holder_model_main)
    ImageView imgModel;
    Unbinder binder;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FrameLayout mainFrame;

    public MainFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MainFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MainFragment newInstance(String param1, String param2) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frame_main_activity, container, false);

        binder = ButterKnife.bind(this, view);

        mainInterface.setTitle("home");
        mainInterface.turnBehavior(false);
        mainInterface.refreshUser();


        //presenter to get image from server

        imgAgency.setOnClickListener(view1 -> {
            presenter.applyAgencyFilterDefault();
        });

        imgModel.setOnClickListener(view1 -> {
            presenter.applyModelFilterDefault();
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mainInterface = (MainPresenter.MainInterface) context;
        presenter = mainInterface.getPresenter();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (binder != null) {
            binder.unbind();
            binder = null;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (binder != null) {
            binder.unbind();
            binder = null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (binder != null) {
            binder.unbind();
            binder = null;
        }
    }
}
