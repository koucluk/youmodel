package com.forest_indo.youmodel.View.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListPopupWindow;
import android.widget.RelativeLayout;

import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.View.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AgencyFilterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AgencyFilterFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @BindView(R.id.rec_kategori)
    RecyclerView recKategori;
    @BindView(R.id.btn_lokasi_agency)
    RelativeLayout btnLokasi;
    @BindView(R.id.txt_lokasi_filter_agency)
    CustomTextView txtLokasi;

    String Lokasi = "S";

    MainPresenter.MainInterface mainInterface;
    MainPresenter presenter;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public AgencyFilterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AgencyFilterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AgencyFilterFragment newInstance(String param1, String param2) {
        AgencyFilterFragment fragment = new AgencyFilterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frame_agency_filter, container, false);
        mainInterface.setTitle("SARING");
        mainInterface.turnBehavior(false);
        ButterKnife.bind(this, view);

        setupRec();
        return view;
    }

    private void setupRec() {
        recKategori.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        presenter.setupKategori(recKategori);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainInterface = (MainPresenter.MainInterface) context;
        presenter = mainInterface.getPresenter();
    }

    @OnClick(R.id.btn_lokasi_agency)
    public void showPopupLokasi() {
        ListPopupWindow popupWindow = new ListPopupWindow(getActivity());

        ListAdapter adapter = new ArrayAdapter<>(getActivity(), R.layout.item_location, new String[]{"Surabaya", "Jakarta"});
        popupWindow.setAdapter(adapter);
        popupWindow.setAnchorView(btnLokasi);
        popupWindow.setOnItemClickListener((parent, view, position, id) -> {
            CustomTextView v = (CustomTextView) view;
            Lokasi = v.getText().toString();

            txtLokasi.setText(Lokasi.toUpperCase());
            popupWindow.dismiss();
        });
        popupWindow.show();
    }

    @OnClick(R.id.btn_submit_filter_agency)
    public void submitFilter() {
        presenter.applyAgencyFilter(Lokasi, recKategori);
    }

}
