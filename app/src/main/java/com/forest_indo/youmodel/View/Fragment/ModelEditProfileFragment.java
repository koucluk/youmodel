package com.forest_indo.youmodel.View.Fragment;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListPopupWindow;
import android.widget.RelativeLayout;

import com.forest_indo.youmodel.Model.ProfileModel;
import com.forest_indo.youmodel.Model.User;
import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.Constant;
import com.forest_indo.youmodel.Utils.View.CircleTransformation;
import com.forest_indo.youmodel.Utils.View.CustomButton;
import com.forest_indo.youmodel.Utils.View.CustomEditText;
import com.forest_indo.youmodel.Utils.View.CustomTextView;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ModelEditProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ModelEditProfileFragment extends Fragment implements Constant {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    MainPresenter.MainInterface mainInterface;
    MainPresenter mainPresenter;

    @BindView(R.id.img_profil_edit_model)
    ImageView imgProfil;
    @BindView(R.id.btn_chg_edit_model)
    ImageButton imgEditPicture;
    @BindView(R.id.et_name_edit_model)
    CustomEditText etName;
    @BindView(R.id.holder_select_male_edit_model)
    LinearLayout holderSelectMale;
    @BindView(R.id.select_male_edit_model)
    ImageButton selectorMale;
    @BindView(R.id.txt_select_male_edit_model)
    CustomTextView txtSelectorMale;
    @BindView(R.id.holder_select_female_edit_model)
    LinearLayout holderSelectFemale;
    @BindView(R.id.select_female_edit_model)
    ImageButton selectorFemale;
    @BindView(R.id.txt_select_female_edit_model)
    CustomTextView txtSelectorFemale;
    @BindView(R.id.et_weight_edit_model)
    CustomEditText etBerat;
    @BindView(R.id.et_height_edit_model)
    CustomEditText etTinggi;
    @BindView(R.id.rec_kategori_edit_model)
    RecyclerView recKategori;
    @BindView(R.id.et_telepon_edit_model)
    CustomEditText etTelepon;

    @BindView(R.id.btn_lokasi_edit_model)
    RelativeLayout btnLokasi;

    @BindView(R.id.btn_konfirm_edit_model)
    CustomButton btnSubmit;
    @BindView(R.id.btn_dob_edit_model)
    RelativeLayout btnDobModel;
    @BindView(R.id.txt_value_dob_edit_model)
    CustomTextView txtDobModel;
    @BindView(R.id.txt_lokasi_value)
    CustomTextView txtLokasiValue;
    int AGE;
    private int GENDER = 0;
    private int Day = 0, Month = 0, Year = 0;
    private String DOB;
    private String Lokasi = "";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private List<Integer> listDate, listMonth, listYear;

    public ModelEditProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ModelEditProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ModelEditProfileFragment newInstance(String param1, String param2) {
        ModelEditProfileFragment fragment = new ModelEditProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frame_model_edit_profile, container, false);
        ButterKnife.bind(this, view);

        mainInterface.setTitle("EDIT PROFIL");
        mainInterface.turnBehavior(true);

        try {
            setViews(mainPresenter.getModel());
        } catch (NullPointerException e) {
            e.printStackTrace();
            setViews(mainPresenter.getUser());
        }

        recKategori.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        mainPresenter.setupKategoriUpdatable(recKategori);

        return view;
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 0x123);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), 0x122);
    }

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            try {
                CropImage.activity(data.getData())
                        .setCropShape(CropImageView.CropShape.OVAL)
                        .setFixAspectRatio(true)
                        .start(getContext(), this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @OnClick(R.id.btn_chg_edit_model)
    public void UpdatePicture() {
        final CharSequence[] items = {"Dari Kamera", "Ambil Gambar dari Galeri",
                "Batal"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, (dialog, item) -> {

            if (items[item].equals("Dari Kamera")) {
                cameraIntent();
            } else if (items[item].equals("Ambil Gambar dari Galeri")) {
                galleryIntent();
            } else if (items[item].equals("Batal")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void onCaptureImageResult(Intent data) {
        try {
            CropImage.activity(data.getData())
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .setFixAspectRatio(true)
                    .start(getContext(), this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 0x122)
                onSelectFromGalleryResult(data);
            else if (requestCode == 0x123)
                onCaptureImageResult(data);
            else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                processAndUpdate(result.getUri());
            }
        }
    }

    private void processAndUpdate(Uri uri) {
        File file = new File(uri.getPath());
        String ex = file.getName();
        Log.d("NAME FILE", ex);
        File newDir = new File(getActivity().getFilesDir() + "/profile/photos/");

        if (!newDir.exists()) {
            newDir.mkdirs();
        }

        File newFile = new File(getActivity().getFilesDir() + "/profile/photos/ym_profile_1_" + mainPresenter.getUser().getUSER_ID() + ex.substring(ex.lastIndexOf('.')));
        FileChannel inChannel = null;
        FileChannel outChannel = null;

        try {
            inChannel = new FileInputStream(file).getChannel();
            outChannel = new FileOutputStream(newFile).getChannel();

            inChannel.transferTo(0, inChannel.size(), outChannel);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inChannel != null)
                    inChannel.close();

                if (outChannel != null)
                    outChannel.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Log.d("FILE CROPPED", newFile.getPath());

        mainPresenter.ChangePicture(newFile, imgProfil);
    }

    private void setViews(User user) {
        etName.setText(user.getUsername());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mainInterface = (MainPresenter.MainInterface) context;
        mainPresenter = mainInterface.getPresenter();
    }

    @OnClick(R.id.btn_lokasi_edit_model)
    public void showLokasiPopup() {
        ListPopupWindow popupWindow = new ListPopupWindow(getActivity());

        ListAdapter adapter = new ArrayAdapter<>(getActivity(), R.layout.item_location, new String[]{"Surabaya", "Jakarta"});
        popupWindow.setAdapter(adapter);
        popupWindow.setAnchorView(btnLokasi);
        popupWindow.setOnItemClickListener((parent, view, position, id) -> {
            CustomTextView v = (CustomTextView) view;
            Lokasi = v.getText().toString();

            txtLokasiValue.setText(Lokasi.toUpperCase());
            popupWindow.dismiss();
        });
        popupWindow.show();
    }

    @OnClick(R.id.holder_select_male_edit_model)
    public void selectMale() {
        if (GENDER != 1) {
            selectorMale.setImageResource(R.drawable.asset_radio_button_selected);
            selectorFemale.setImageResource(R.drawable.asset_radio_button_deselect);
            GENDER = 1;
        }
    }

    @OnClick(R.id.holder_select_female_edit_model)
    public void selectFemale() {
        if (GENDER != 2) {
            selectorFemale.setImageResource(R.drawable.asset_radio_button_selected);
            selectorMale.setImageResource(R.drawable.asset_radio_button_deselect);
            GENDER = 2;
        }
    }

    @OnClick(R.id.btn_dob_edit_model)
    public void selectDob() {
        Calendar calendar = Calendar.getInstance();

        if (Day == 0) {
            Day = calendar.get(Calendar.DAY_OF_MONTH);
            Month = calendar.get(Calendar.MONTH);
            Year = calendar.get(Calendar.YEAR);
        }

        DatePickerDialog dialog = DatePickerDialog.newInstance((view, year, month, day) -> {
            Day = day;
            Month = month + 1;
            Year = year;

            DOB = Day + "/" + Month + "/" + Year;
            txtDobModel.setText(DOB);

        }, Year, Month, Day);

        dialog.setAccentColor(getResources().getColor(R.color.base_pink));
        dialog.show(getActivity().getFragmentManager(), "DOB_MODEL");
    }

    @OnClick(R.id.btn_konfirm_edit_model)
    public void submitEdit() {
        int Height = etTinggi.getText().toString().equals("") ? 150 : Integer.parseInt(etTinggi.getText().toString());
        int Weight = etBerat.getText().toString().equals("") ? 40 : Integer.parseInt(etBerat.getText().toString());
        AGE = DOB.equals("") || DOB == null ? 18 : Calendar.getInstance().get(Calendar.YEAR) - Year;

        if (Lokasi.equalsIgnoreCase("")) {
            Lokasi = txtLokasiValue.getText().toString();
        }

        Log.d("NAME", etName.getText().toString());
        Log.d("LOKASI", Lokasi);
        Log.d("NO TEL", etTelepon.getText().toString());
        Log.d("GENDER", GENDER + "");
        Log.d("DOB", DOB + "");
        Log.d("WEIGHT", Weight + "");
        Log.d("HEIGHT", Height + "");
        mainPresenter.submitProfileData(etName.getText().toString(), Lokasi, etTelepon.getText().toString(),
                GENDER, DOB, AGE, Height, Weight, "", "", 0, "", "", recKategori);
    }

    public void setViews(ProfileModel model) {
        if (model != null) {

            DOB = model.getDob() + "";

            String photoPath = getActivity().getFilesDir() + PROFILEPICTURE_LOCAL_PATH + model.getPhoto_Url();

            File photoProfile = new File(photoPath);

            if (photoProfile.exists()) {
                Picasso.with(getActivity())
                        .load(photoProfile)
                        .transform(new CircleTransformation())
                        .error(R.drawable.asset_image_avatar_dafault_model)
                        .placeholder(R.drawable.asset_image_avatar_dafault_model)
                        .into(imgProfil);
            } else {
                Picasso.with(getActivity())
                        .load(PROFILEPICTURE_SERVER_URL + model.getPhoto_Url())
                        .transform(new CircleTransformation())
                        .error(R.drawable.asset_image_avatar_dafault_model)
                        .placeholder(R.drawable.asset_image_avatar_dafault_model)
                        .into(imgProfil);
            }


            if (!DOB.equals("") && !DOB.equalsIgnoreCase("null")) {
                txtDobModel.setText(DOB);
                splitDob();
            }

            txtLokasiValue.setText(model.getLokasi().toUpperCase());

            etName.setText(model.getName().toUpperCase());

            selectorMale.setImageResource(model.getGender() == 1
                    ? R.drawable.asset_radio_button_selected
                    : R.drawable.asset_radio_button_deselect);

            selectorFemale.setImageResource(model.getGender() == 2
                    ? R.drawable.asset_radio_button_selected
                    : R.drawable.asset_radio_button_deselect);

            GENDER = model.getGender() == 2 ? 2 : 1;

            etTinggi.setText(model.getHeight() + "");
            etBerat.setText(model.getWeight() + "");

            etTelepon.setText(model.getPhone_Number() + "");
        }
    }


    private void splitDob() {
        Day = Integer.parseInt(DOB.split("/")[0]);
        Month = Integer.parseInt(DOB.split("/")[1]) - 1;
        Year = Integer.parseInt(DOB.split("/")[2]);
    }
}