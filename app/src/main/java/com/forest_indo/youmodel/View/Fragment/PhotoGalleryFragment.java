package com.forest_indo.youmodel.View.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.forest_indo.youmodel.Adapter.GalleryPhotoModelAdapter;
import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PhotoGalleryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PhotoGalleryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.rec_photo_model)
    RecyclerView recPhoto;
    MainPresenter.MainInterface mainInterface;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Unbinder binder;


    public PhotoGalleryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PhotoGalleryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PhotoGalleryFragment newInstance(String param1, String param2) {
        PhotoGalleryFragment fragment = new PhotoGalleryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frame_model_photo, container, false);
        mainInterface.setTitle("GALERI FOTO");

        binder = ButterKnife.bind(this, view);

        setupView();

        return view;
    }

    private void setupView() {
        recPhoto.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        recPhoto.setAdapter(new GalleryPhotoModelAdapter());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        binder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainInterface = (MainPresenter.MainInterface) context;
    }
}
