package com.forest_indo.youmodel.View;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;

import com.forest_indo.youmodel.Presenter.LoginPresenter;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.APIUtils.APIRequest;
import com.forest_indo.youmodel.Utils.View.CustomEditText;
import com.forest_indo.youmodel.Utils.View.CustomTextViewPhar;
import com.greenfrvr.rubberloader.RubberLoaderView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Koucluck on 8/31/2016.
 */
public class LoginActivity extends AppCompatActivity implements BaseView, LoginPresenter.LoginInterface {


//    ImageView btnShowPass;

    @BindView(R.id.txt_passwd)
    CustomEditText etPasswd;
    @BindView(R.id.txt_email)
    CustomEditText etEmail;

    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.btn_register)
    Button btnRegister;

    @BindView(R.id.holder_loader)
    FrameLayout loader;

    @BindView(R.id.loader)
    RubberLoaderView dotLoader;

    @BindView(R.id.txt_login_error)
    CustomTextViewPhar txtError;

    LoginPresenter presenter;

    Unbinder binder;

    boolean IS_PASSWD_SHOWED = false;

    @Inject
    APIRequest request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_login);

        binder = ButterKnife.bind(this);
        presenter = new LoginPresenter(this, this);

        presenter.isLoggedIn();
    }

    @OnClick(R.id.btn_login)
    public void doLogin() {
        presenter.doLogin(etEmail.getText().toString(), etPasswd.getText().toString());
    }

    @OnClick(R.id.btn_register)
    public void doRegister() {
        presenter.doRegister();
    }

    @OnClick(R.id.btn_show_pass)
    public void showPass() {
        presenter.showPass(etPasswd, IS_PASSWD_SHOWED);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
        binder.unbind();
        presenter = null;
    }

    @Override
    public void setStatusPassword(boolean status) {
        IS_PASSWD_SHOWED = status;
    }

    @Override
    public void showError(String msg) {
        Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.error_anim);
        Animation animDisolve = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.disolve_anim);

        Runnable runnable = () -> {
            txtError.startAnimation(animDisolve);
            txtError.setVisibility(View.GONE);
        };

        Handler handler = new Handler();
        handler.removeCallbacks(runnable);

        txtError.setVisibility(View.VISIBLE);
        txtError.setText(msg);
        txtError.startAnimation(anim);

        handler.postDelayed(runnable, 5000);
    }

    @Override
    public void showProgress(boolean status) {
        if (status) {
            loader.setVisibility(View.VISIBLE);
            dotLoader.startLoading();
        } else {
            loader.setVisibility(View.GONE);
            dotLoader.stopLoading();
        }
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getApplicationContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = this.getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @Override
    public void showOneButtonDialog(String message, String action) {

    }

    @Override
    public void showTwoButtonDialog(String message, String action1, String action2) {

    }

    @Override
    public AlertDialog getDialog() {
        return null;
    }
}
