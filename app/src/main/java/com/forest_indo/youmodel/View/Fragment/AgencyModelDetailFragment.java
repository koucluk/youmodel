package com.forest_indo.youmodel.View.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.forest_indo.youmodel.Presenter.MainPresenter;
import com.forest_indo.youmodel.R;
import com.forest_indo.youmodel.Utils.View.CustomTextViewPhar;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AgencyModelDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AgencyModelDetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @BindView(R.id.txt_name_model_agency)
    CustomTextViewPhar txtName;

    @BindView(R.id.txt_gender_model_agency)
    CustomTextViewPhar txtGender;

    @BindView(R.id.txt_lokasi_model_agency)
    CustomTextViewPhar txtLokasi;

    @BindView(R.id.txt_dob_model_agency)
    CustomTextViewPhar txtDob;

    @BindView(R.id.txt_appear_model_agency)
    CustomTextViewPhar txtAppears;

    @BindView(R.id.rec_kategori_model_agency)
    RecyclerView recKategori;

    MainPresenter.MainInterface mainInterface;


    public AgencyModelDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AgencyModelDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AgencyModelDetailFragment newInstance(String param1, String param2) {
        AgencyModelDetailFragment fragment = new AgencyModelDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frame_agency_model_detail, container, false);
        mainInterface.setTitle("RINCIAN MODEL");
        mainInterface.turnBehavior(true);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mainInterface = (MainPresenter.MainInterface) context;
    }
}
